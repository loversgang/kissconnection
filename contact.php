<?php
require_once("include/config/config.php");
require 'tmp/header.php';
if (isset($_POST['send_mail'])) {
    pr($_POST);
    extract($_POST);
    $to_email = 'balbinder@cwebconsultants.com';
    $subject = 'Contact Us';
    $FromName = 'KissConnection.Com';
    if ($username) {
        $FromName = $username;
    }
    SendEmail($subject, $to_email, $from_email, $FromName, $message);
    $admin_user->set_pass_msg('Your message has been sent!');
    Redirect(DIR_WS_SITE . 'contact.php');
}
?>
<div class="row-fluid">
    <div class="panel panel-default">
        <div class="panel-heading">
            <ol class="breadcrumb">
                <li><a href="<?php echo make_admin_url('home') ?>">Home</a></li>
                <li class="active">Contact Us</li>
            </ol>
        </div>
        <div class="panel-body">
            <?php display_message(1); ?>
            <div class="col-md-7">
                <p>You can contact us using the email form to the right.</p>
            </div>
            <div class="col-md-5">
                <form method="post" action="">
                    <?php
                    if ($admin_user->is_logged_in()) {
                        $user_id = $_SESSION['admin_session_secure']['user_id'];
                        $obj = new user;
                        $logged_user = $obj->getUser($user_id);
                        ?>
                        <div class="form-group">
                            <label class="control-label">Username</label>
                            <br/>
                            <?php echo $logged_user->username; ?>
                            <br/>
                            <input type="hidden" name="username" value="<?php echo $logged_user->username; ?>"/>
                        </div>
                    <?php } ?>
                    <div class="form-group">
                        <label class="control-label">Your Email Address</label>
                        <input type="email" name="from_email" class="form-control" required />
                    </div>
                    <div class="form-group">
                        <label class="control-label">Question and/or Comment</label>
                        <textarea class="form-control" name="message" required  rows="3"></textarea>
                    </div>
                    <input type="submit" name="send_mail" value="Send Message" class="btn btn-success"/>
                </form>
            </div>
        </div>
    </div>
</div>
<?php
require 'tmp/footer.php';
