<?php

session_start();

if (isset($_SESSION['last_activity_recorded']) && (time() - $_SESSION['last_activity_recorded'] > 1800)) {
    session_unset();
    session_destroy();
}
$_SESSION['last_activity_recorded'] = time();


require_once("include/config/config.php");
$Page = isset($_GET['Page']) ? $_GET['Page'] : "home";
if (!$admin_user->is_logged_in()) {
    if ($Page !== 'about') {
        $admin_user->set_error();
        $admin_user->set_pass_msg('Log in first');
        Redirect(DIR_WS_SITE);
    }
}
// Get Logged User Details
$user_id = $_SESSION['admin_session_secure']['user_id'];
$obj = new user;
$logged_user = $obj->getUser($user_id);

$PageAccess = '*';

if ($Page != '' && file_exists(DIR_FS_SITE . '/script/' . $Page . '.php')):
    if ($PageAccess == '*' || array_key_exists($Page, $PageAccess)):

        if ($PageAccess != '*'):
            $action = isset($_GET['action']) ? $_GET['action'] : "list";
            if (isset($PageAccess[$Page][$action])):
                include(DIR_FS_SITE . '/script/' . $Page . '.php');
            endif;
        else:
            include(DIR_FS_SITE . '/script/' . $Page . '.php');
        endif;
    endif;
endif;
require 'tmp/header.php';
if ($Page != "") {
    if (file_exists(DIR_FS_SITE . '/form/' . $Page . ".php")):
        if ($PageAccess == '*' || array_key_exists($Page, $PageAccess)):
            if ($PageAccess != '*'):
                if (isset($PageAccess[$Page][$action])):
                    require_once(DIR_FS_SITE . '/form/' . $Page . ".php");
                else:
                    echo"<p style='height:250px;'><br><br><font size='3'><b>Welcome " . $admin_user->get_username() . "!</b></font><br><br>";
                    echo "You do not have the permission to access this page.</p>";
                endif;
            else:
                require_once(DIR_FS_SITE . '/form/' . $Page . ".php");
            endif;
        else:
            echo"<p style='height:250px;'><br><br><font size='3'><b>Welcome " . $admin_user->get_username() . "!</b></font><br><br>";
            echo "You do not have the permission to access this page.</p>";
        endif;
    else:
        if ($PageAccess == '*' || array_key_exists($Page, $PageAccess)):
            if ($PageAccess != '*'):
                if (isset($PageAccess[$Page][$action])):
                    require_once(DIR_FS_SITE . '/form/default.php');
                else:
                    echo"<p style='height:250px;'><br><br><font size='3'><b>Welcome " . $admin_user->get_username() . "!</b></font><br><br>";
                    echo "You do not have the permission to access this page.</p>";
                endif;
            else:
                require_once(DIR_FS_SITE . '/form/default.php');
            endif;
        else:
            echo "<p style='height:250px;'><font size='3'><br><br><b>You do not have the permission to access this page.</b></font></p>";
        endif;
    endif;
}
require 'tmp/footer.php';
?>