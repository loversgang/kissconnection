<div class="row-fluid">
    <div class="panel panel-default">
        <div class="panel-heading">
            <?php echo ucfirst($gallery->title); ?>
        </div>
        <div class="panel-body">
            <?php display_message(1); ?>
            <div class="col-md-8">
                <?php if ($type == 'video') { ?>
                    <?php foreach ($videos as $video) { ?>
                        <div class="col-md-4">
                            <div class="wrapper">
                                <div class="videos_class">
                                    <?php echo html_entity_decode($video->video_code); ?>
                                </div>
                                <div class="desc_content" style=""><?php echo $video->caption; ?></div>
                            </div>
                        </div>
                    <?php } ?>
                <?php } else { ?>
                    <?php foreach ($photos as $photo) { ?>
                        <div class="col-md-4">
                            <div class="wrapper">
                                <div class="">
                                    <img src="<?php echo DIR_WS_SITE_FILE . $photo->file_name; ?>" class="img img-responsive" style="width: 100%;height: 100px"/>
                                </div>
                                <div class="desc_content" style=""><?php echo $photo->caption; ?></div>
                            </div>
                        </div>
                    <?php } ?>
                <?php } ?>
            </div>
            <div class="col-md-4">
                <ul class="list-group">
                    <li class="list-group-item active_c">Update <span class="pull-right">
                            <?php if ($type == 'video') { ?>
                                <a href="<?php echo make_admin_url('video') ?>">Upload Video</a>
                            <?php } else { ?>
                                <a href="<?php echo make_admin_url('photo') ?>">Upload Photo</a>
                            <?php } ?>
                        </span></li>
                    <li class="list-group-item radio_btns">
                        <form class="form" method="post" action="">
                            <div class="form-group">
                                <label class="control-label">Title</label>
                                <input type="hidden" name="id" value="<?php echo $gallery->id; ?>"/>
                                <input type="text" name="title" class="form-control" value="<?php echo $gallery->title; ?>" required/>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Description</label>
                                <textarea name="description" class="form-control" required><?php echo $gallery->description; ?></textarea>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="is_primary" value="1"<?php echo $gallery->is_primary == '1' ? 'checked' : ''; ?>> Is Primary?
                                </label>
                            </div>
                            <input type="submit" name="update_gallery" value="Update Gallery" class="btn btn-success"/>
                        </form>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>