<div class="row-fluid">
    <div class="panel panel-default">
        <div class="panel-heading">
            Gallery
        </div>
        <div class="panel-body">
            <?php display_message(1); ?>
            <div class="col-md-8">
                <?php if ($type == 'video') { ?>
                    <ul class="list-group">
                        <li class="list-group-item active_c">Video Gallery <span class="pull-right"><a href="<?php echo make_admin_url('gallery') ?>">Photo Gallery</a></span></li>
                        <li class="list-group-item radio_btns">
                            <table class="table table-bordered datatable" style="text-align: center">
                                <thead>
                                    <tr>
                                        <th style="text-align: center">Thumbnail</th>
                                        <th style="text-align: center">Title</th>
                                        <th style="text-align: center">Total Videos</th>
                                        <th style="text-align: center">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    foreach ($videos as $video) {
                                        $obj = new userVideo;
                                        $file = $obj->getLastUploadedVideo($user_id, $video->id);
                                        ?>
                                        <tr>
                                            <td style="width: 20%" class="videos_class">
                                                <?php if ($file) { ?>
                                                    <?php echo html_entity_decode($file->video_code); ?>
                                                <?php } else { ?>
                                                    <img src="<?php echo DIR_WS_SITE_IMAGE ?>noimage.jpg" class="img img-responsive" style="width: 100%"/>
                                                <?php } ?>
                                            </td>
                                            <td>
                                                <b><?php echo $video->title; ?></b>
                                            </td>
                                            <td>
                                                <?php
                                                $obj = new userVideo;
                                                $count = $obj->getVideoCount($user_id, $video->id);
                                                echo $count;
                                                ?>
                                            </td>
                                            <td>
                                                <a href="<?php echo make_admin_url('gallery', 'update', 'update', 'vid=' . $video->id . '&type=video') ?>" class="btn btn-success btn-xs"><i class="fa fa-pencil"></i></a>
                                                <a href="<?php echo make_admin_url('gallery', 'delete', 'delete', 'vid=' . $video->id . '&type=video') ?>" class="btn btn-danger btn-xs" onclick="return confirm('Are you sure? You are deleting this record.');"><i class="fa fa-trash"></i></a>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </li>
                    </ul>
                <?php } else { ?>
                    <ul class="list-group">
                        <li class="list-group-item active_c">Photo Gallery <span class="pull-right"><a href="<?php echo make_admin_url('gallery', 'list', 'list', 'type=video') ?>">Video Gallery</a></span></li>
                        <li class="list-group-item radio_btns">
                            <table class="table table-bordered datatable" style="text-align: center">
                                <thead>
                                    <tr>
                                        <th style="text-align: center">Thumbnail</th>
                                        <th style="text-align: center">Title</th>
                                        <th style="text-align: center">Total Photos</th>
                                        <th style="text-align: center">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    foreach ($photos as $photo) {
                                        $obj = new userPhoto;
                                        $file = $obj->getLastUploadedPhoto($user_id, $photo->id);
                                        ?>
                                        <tr>
                                            <td style="width: 20%">
                                                <?php if ($file) { ?>
                                                    <img src="<?php echo DIR_WS_SITE_FILE . $file->file_name; ?>" class="img img-responsive" style="width: 100%"/>
                                                <?php } else { ?>
                                                    <img src="<?php echo DIR_WS_SITE_IMAGE ?>noimage.jpg" class="img img-responsive" style="width: 100%"/>
                                                <?php } ?>
                                            </td>
                                            <td>
                                                <b><?php echo $photo->title; ?></b>
                                            </td>
                                            <td>
                                                <?php
                                                $obj = new userPhoto;
                                                $count = $obj->getPhotoCount($user_id, $photo->id);
                                                echo $count;
                                                ?>
                                            </td>
                                            <td>
                                                <a href="<?php echo make_admin_url('gallery', 'update', 'update', 'pid=' . $photo->id) ?>" class="btn btn-success btn-xs"><i class="fa fa-pencil"></i></a>
                                                <a href="<?php echo make_admin_url('gallery', 'delete', 'delete', 'pid=' . $photo->id) ?>" class="btn btn-danger btn-xs" onclick="return confirm('Are you sure? You are deleting this record.');"><i class="fa fa-trash"></i></a>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </li>
                    </ul>
                <?php } ?>
            </div>
            <div class="col-md-4">
                <ul class="list-group">
                    <li class="list-group-item active_c">Create Gallery</li>
                    <li class="list-group-item radio_btns">
                        <form class="form" method="post" action="">
                            <div class="form-group">
                                <label class="control-label">Title</label>
                                <input type="text" name="title" class="form-control" required/>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Gallery Type</label>
                                <select class="form-control" name="type">
                                    <option value="photo">Photo</option>
                                    <option value="video">Video</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Description</label>
                                <textarea name="description" class="form-control" required></textarea>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="is_primary" value="1"> Is Primary?
                                </label>
                            </div>
                            <input type="submit" name="create_gallery" value="Create Gallery" class="btn btn-success"/>
                        </form>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>