<div class="row-fluid">
    <div class="panel panel-default">
        <div class="panel-heading">
            <ol class="breadcrumb">
                <li><a href="<?php echo make_admin_url('home') ?>">Home</a></li>
                <li class="active"><a href="<?php echo make_admin_url('account') ?>">Edit Profile</a></li>
                <li class="active">My Videos</li>
            </ol>
        </div>
        <div class="panel-body">
            <?php display_message(1); ?>
            <div class="col-md-8">
                <div class="panel panel-default">
                    <div class="panel panel-heading">
                        Video Gallery <span class="pull-right"><a href="<?php echo make_admin_url('photo') ?>">Photo Gallery</a></span>
                    </div>
                    <div class="panel panel-body">
                        <?php if ($videos) { ?>
                            <?php foreach ($videos as $video) { ?>
                                <div class="col-md-4">
                                    <div class="wrapper">
                                        <div class="videos_class">
                                            <img src="https://i.ytimg.com/vi/<?php echo $video->video_code ?>/hqdefault.jpg"  class="img img-responsive youtube_id tool" data-key="<?php echo $video->video_code ?>" style="cursor:pointer" title="Click Here To View"/>
                                        </div>
                                        <div class="desc_content" style=""><?php echo $video->caption; ?></div>
                                        <a href="<?php echo make_admin_url('video', 'delete', 'delete', 'vid=' . $video->id . '&type=video') ?>" class="btn btn-danger btn-xs tool" title="Click Here To Delete" onclick="return confirm('Are you sure? You are deleting this record.');"><i class="fa fa-trash"></i></a>
                                        <a href="<?php echo make_admin_url('video', 'change', 'change', 'vid=' . $video->id) ?>" class="btn btn-success btn-xs tool" title="Make this Your Primary Video" ><i class="fa fa-picture-o"></i></a>
                                    </div>
                                </div>
                            <?php } ?>
                        <?php } else { ?>
                            <div class="alert alert-danger">No Video Uploaded!</div>
                        <?php } ?>
                    </div>
                </div>
                <a href="<?php echo make_admin_url('account') ?>" class="btn btn-success" style="margin-bottom: 20px">Go back to profile</a>
            </div>
            <div class="col-md-4">
                <ul class="list-group">
                    <li class="list-group-item active_c">Upload Video</li>
                    <li class="list-group-item radio_btns">
                        <form class="form" enctype="multipart/form-data" method="post" action="">
                            <div class="form-group">
                                <label class="control-label">Paste Youtube URL Or Share Code</label>
                                <input type="text" name="video_code" class="form-control" required />
                            </div>
                            <div class="form-group">
                                <label class="control-label">Caption</label>
                                <input type="text" name="caption" class="form-control"/>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Position</label>
                                <input type="number" name="sequence" class="form-control"/>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="is_primary" value="1"> Is Primary?
                                </label>
                            </div>
                            <input type="submit" name="upload_video" value="Upload" class="btn btn-success"/>
                        </form>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="youtube_model">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Video</h4>
            </div>
            <div class="modal-body"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary stop_player" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).on('click', '.youtube_id', function () {
        var iframe = $(this).attr('data-key');
        $('.modal-body').html('<iframe width="854" height="580" src="https://www.youtube.com/embed/' + iframe + '" frameborder="0" allowfullscreen class="img img-responsive" style="height: 373px;"></iframe>');
        $('#youtube_model').modal({backdrop: 'static', keyboard: false});
    });

    $(document).on('click', '.stop_player', function () {
        $('iframe').remove();
    });
</script>