<div class="row-fluid">
    <div class="panel panel-default">
        <div class="panel-heading">
            <ol class="breadcrumb">
                <li><a href="<?php echo make_admin_url('home') ?>">Home</a></li>
                <li class="active">My Cupid</li>
            </ol>
        </div>
        <div class="panel-body">
            <?php display_message(1); ?>
            <ul class="list-group">
                <li class="list-group-item radio_btns">
                    <p>To change your My Cupid setting, select the appropriate option below and click "Update":</p>
                    <form method="post" class="from form-horizontal">
                        <div class="radio">
                            <label>
                                <input type="radio" name="is_cupid" value="1" <?php echo $logged_user->is_cupid == 1 ? 'checked' : ''; ?>> On
                            </label>
                        </div>
                        <div class="radio">
                            <label>
                                <input type="radio" name="is_cupid" value="0" <?php echo $logged_user->is_cupid == 0 ? 'checked' : ''; ?>> Off
                            </label>
                        </div>
                        <div style="margin-top: 10px">
                            <input type="submit" name="update_cupid" value="Update" class="btn btn-success btn-xs"/>
                        </div>
                    </form>
                </li>
            </ul>
        </div>
    </div>
</div>