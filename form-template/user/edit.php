<div class="page-content">
    <div class="content-block">
        <h2 class="page_title">Edit User <a href="<?php echo make_load_url('user') ?>" class="flot-right back link" alt="Back" title="Back"><i class="fa fa-arrow-circle-left fa-2x"></i></a></h2>
        <div class="contactform">
            <form method="post" action="<?php echo 'admin.php#!/' . make_load_url('user') ?>">
                <input type="text" name="first_name" value="<?php echo $user->first_name ?>" placeholder="First Name" class="form_input" />
                <input type="text" name="last_name" value="<?php echo $user->last_name ?>" placeholder="Last Name" class="form_input" />
                <input type="text" name="username" value="<?php echo $user->username ?>" placeholder="Username" class="form_input" />
                <input type="password" name="password" value="" placeholder="Password" class="form_input" id="password"/>
                <input type="password" name="password2" value="" placeholder="Confirm Password" class="form_input" />
                <input type="text" name="email" value="<?php echo $user->email ?>" placeholder="Email" class="form_input" />
                <input type="text" name="phone" value="<?php echo $user->phone ?>" placeholder="Phone" class="form_input" />
                <input type="hidden" name="id" value="<?php echo $user->id ?>" />
                <input type="submit" name="update_user" class="button button-big button-fill" id="update_user_btn" value="SAVE" />
            </form>
            <div>
                <a class="button button-big button-fill back link" href="<?php echo make_load_url('user') ?>">CANCEL</a>
            </div>
        </div>
    </div>
</div>