<div class="row-fluid">
    <div class="panel panel-default">
        <div class="panel-heading">
            <ol class="breadcrumb">
                <li><a href="<?php echo make_admin_url('home') ?>">Home</a></li>
                <li class="active"><a href="<?php echo make_admin_url('account') ?>">Edit Profile</a></li>
                <li class="active">My Photos</li>
            </ol>
        </div>
        <div class="panel-body">
            <?php display_message(1); ?>
            <div class="col-md-8">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Photo Gallery
                        <span class="pull-right"><a href="<?php echo make_admin_url('video') ?>">Video Gallery</a></span>
                    </div>
                    <div class="panel-body">
                        <?php if ($photos) { ?>
                            <?php foreach ($photos as $photo) { ?>
                                <div class="col-md-4">
                                    <div class="wrapper">
                                        <div class="">
                                            <img src="<?php echo DIR_WS_SITE_FILE . $photo->file_name; ?>" class="img img-responsive" style="width: 100%;height: 100px"/>
                                        </div>
                                        <div class="desc_content" style=""><?php echo $photo->caption; ?></div>
                                        <a href="<?php echo make_admin_url('photo', 'delete', 'delete', 'pid=' . $photo->id) ?>" class="btn btn-danger btn-xs tool" title="Click Here To Delete" onclick="return confirm('Are you sure? You are deleting this record.');"><i class="fa fa-trash"></i></a> |
                                        <a href="<?php echo make_admin_url('photo', 'change', 'change', 'pid=' . $photo->id) ?>" class="btn btn-success btn-xs tool" title="Make this Your Primary Image" ><i class="fa fa-picture-o"></i></a>

                                    </div>
                                </div>
                            <?php } ?>
                        <?php } else { ?>
                            <div class="alert alert-danger">No Photo Uploaded!</div>
                        <?php } ?>

                    </div>
                </div>
                <a href="<?php echo make_admin_url('account') ?>" class="btn btn-success" style="margin-bottom: 20px">Go back to profile</a>
            </div>
            <div class="col-md-4">
                <ul class="list-group">
                    <li class="list-group-item active_c">Upload Photo</li>
                    <li class="list-group-item radio_btns">
                        <form class="form" enctype="multipart/form-data" method="post" action="">
                            <div class="form-group">
                                <label class="control-label">Photo to Upload</label>
                                <input type="file" name="photo" class="" required/>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Caption</label>
                                <input type="text" name="caption" class="form-control"/>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Position</label>
                                <input type="number" name="sequence" class="form-control"/>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="is_primary" value="1"> Is Primary?
                                </label>
                            </div>
                            <input type="submit" name="upload_photo" value="Upload" class="btn btn-success"/>
                        </form>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>