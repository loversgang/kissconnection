<div class="row-fluid">
    <div class="panel panel-default">
        <div class="panel-heading">
            Quick Search
        </div>
        <div class="panel-body">
            <?php display_message(1); ?>
            <div class="col-md-7" style="border-right: 1px dashed #999">
                <form method="post" action="<?php echo make_admin_url('search', 'quick', 'quick') ?>">
                    <div class="quick_search">
                        <div class="col-md-12">
                            <span class="col-md-4">I am a</span>
                            <select class="col-md-5 s_input">
                                <option value="male">Male</option>
                                <option value="female">Female</option>        
                            </select>
                        </div>
                        <div class="col-md-12">
                            <span class="col-md-4">Seeking a</span>
                            <select name="gender" class="col-md-5 s_input">
                                <option value="female">Female</option>
                                <option value="male">Male</option>        
                            </select>
                        </div>
                        <div class="col-md-12">
                            <span class="col-md-4">Between</span>
                            <select name="from_age" class="col-md-2 s_input">
                                <?php for ($i = 18; $i <= 120; $i++) { ?>
                                    <option value="<?php echo $i; ?>"><?php echo $i; ?></option> 
                                <?php } ?>
                            </select>
                        </div>
                        <div class="col-md-12">
                            <span class="col-md-4">And</span>
                            <select name="to_age" class="col-md-2 s_input">
                                <?php for ($i = 18; $i <= 120; $i++) { ?>
                                    <option value="<?php echo $i; ?>" <?php echo $i == 35 ? 'selected' : ''; ?>><?php echo $i; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="col-md-12">
                            <span class="col-md-4">Located Within</span>
                            <select name="distance" class="col-md-7s_input">
                                <option value="0">Any</option>
                                <option value="5">5</option>
                                <option value="10">10</option>
                                <option value="25" selected >25</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                                <option value="300">300</option>
                                <option value="500">500</option>
                                <option value="1000">1000</option>
                            </select>
                        </div>
                        <div class="col-md-12">
                            <span class="col-md-4">Zip Code</span>
                            <input name="zip" class="col-md-5 s_input" type="number" required />
                        </div>
                        <div class="col-md-12">
                            <span class="col-md-4">Photos Only</span>
                            <input name="photos_only" value="1" class="col-md-2 s_input" type="checkbox"/>
                        </div>
                        <div class="col-md-12">
                            <span class="col-md-4 hidden-xs">&nbsp;</span>
                            <input name="quick_search" class="col-md-3 btn btn-success" type="submit" value="Submit">
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-5">
                <div style="text-align: center">
                    Do you already know who you are looking for?
                    <br/>
                    <a href="<?php echo make_admin_url('search', 'username', 'username') ?>">
                        <b>
                            Username<br/>
                            Search
                        </b>
                    </a>
                </div>
                <br/>
                <br/>
                <div style="text-align: center">
                    Would you like to narrow your search?
                    <br/>
                    <a href="<?php echo make_admin_url('search', 'full', 'full') ?>">
                        <b>
                            Full<br/>
                            Search
                        </b>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
echo display_toastr_message();
?>