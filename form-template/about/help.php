<div class="row-fluid">
    <div class="panel panel-default">
        <div class="panel-heading">
            <ol class="breadcrumb">
                <li><a href="<?php echo make_admin_url('home') ?>">Home</a></li>
                <li class="active">Frequently Asked Questions</li>
            </ol>
        </div>
        <div class="panel-body">
            <div class="body-reg">
                <table width="100%" border="0" cellspacing="0" cellpadding="2">
                    <tbody><tr><td valign="top" class="body-qa">Q:</td><td colspan="2" class="body-reg-bld">I forgot my username/password. Can you tell me what it is?</td></tr> 
                        <tr><td class="body-qa"><br></td><td valign="top" class="body-qa">A:</td><td class="body-reg">No problem! Just click on <a href="<?php echo DIR_WS_SITE . 'contact.php' ?>">Contact Us</a>, enter either your email address or username, and your date of birth, and we'll send you your password.</td></tr>
                        <tr><td colspan="3" class="body-qa"><br></td></tr>
                        <tr><td valign="top" class="body-qa">Q:</td><td colspan="2" class="body-reg-bld">How do I change my username/password?</td></tr>
                        <tr><td class="body-qa"><br></td><td valign="top" class="body-qa">A:</td><td class="body-reg">Just go to <a href="<?php echo make_admin_url('membership') ?>">My Membership</a> and follow the easy steps!</td></tr>
                        <tr><td colspan="3" class="body-qa"><br></td></tr>
                        <tr><td valign="top" class="body-qa">Q:</td><td colspan="2" class="body-reg-bld">Why can't my username be the same as my email address?</td></tr> 
                        <tr><td class="body-qa"><br></td><td valign="top" class="body-qa">A:</td><td class="body-reg">Your kissconnection.com username is a unique word or phrase that you select; maybe it describes your personality or what you're looking for in a match. Unlike your email address, your kissconnection.com username does not correspond in any way to your identity outside the kissconnection.com community-ensuring that your anonymity is protected until you decide to reveal it.</td></tr>
                        <tr><td colspan="3" class="body-qa"><br></td></tr>
                        <tr><td valign="top" class="body-qa">Q:</td><td colspan="2" class="body-reg-bld">How do I Create a Profile?</td></tr>
                        <tr><td class="body-qa"><br></td><td valign="top" class="body-qa">A:</td><td class="body-reg">Creating and posting a profile is free and takes only a few minutes to go through the easy steps. Think of it as a quick sketch of who you are, your lifestyle, and what counts most in a relationship. Our questionnaire will highlight your personality traits and interests and lets you describe your personality even further in a brief (or lengthy!) narrative. Why wait? <a href="<?php echo DIR_WS_SITE . 'register.php' ?>">Create your profile today!</a> Once your profile's complete, our two-way technology will compare your profile responses with other members, helping you to meet your match!</td></tr>
                        <tr><td colspan="3" class="body-qa"><br></td></tr>
                        <tr><td valign="top" class="body-qa">Q:</td><td colspan="2" class="body-reg-bld">How do I Update My Profile?</td></tr>
                        <tr><td class="body-qa"><br></td><td valign="top" class="body-qa">A:</td><td class="body-reg">It's quick and easy to do! To update your profile, just click <a href="<?php echo make_admin_url('account') ?>">here</a> and follow the simple instructions.</td></tr>
                        <tr><td colspan="3" class="body-qa"><br></td></tr>
                        <tr><td valign="top" class="body-qa">Q:</td><td colspan="2" class="body-reg-bld">What are kissconnection.com's profile guidelines?</td></tr>
                        <tr><td class="body-qa"><br></td><td valign="top" class="body-qa">A:</td><td class="body-reg">
                                <p class="body-reg">kissconnection.com welcomes all single adults seeking one-to-one relationships ranging from companionship to friendship, romance to marriage.</p>
                                <p class="body-reg">Our Quality Assurance team reviews each and every profile to provide our members with a comfortable environment, also ensuring them the best possible chances of making quality matches. We reserve the right to revise our guidelines from time to time, so please check back for updates.</p>
                                <p class="body-reg">We may reject profiles if the username, headline, or narrative text contain any of the following:</p>
                                <ul class="body-reg">
                                    <li>Any direct contact information (i.e., email addresses, URLs, ICQ/instant messenger IDs, phone numbers, full names, addresses)</li>
                                    <li>Any location or descriptive information that threatens a member's anonymity </li>
                                    <li>Abusive language of any kind (i.e., vulgarity, racism) </li>
                                    <li>Discussion or descriptions of illegal acts or behavior (i.e., drug use, violence)</li>
                                    <li>Business or political advertisements or solicitations </li>
                                    <li>Material that exploits or solicits personal information from individuals under the age of 18 </li>
                                    <li>Foreign languages (to effectively monitor profiles, they must be in English) </li>
                                    <li>Solicitation of multiple or additional partners </li>
                                    <li>Unauthorized use of copyrighted or trademarked material </li>
                                    <li>Overt sexual innuendo or discussion</li>
                                </ul>
                                <p class="body-reg">kissconnection.com does not accept content from:</p>
                                <ul class="body-reg">
                                    <li>Incarcerated individuals </li>
                                    <li>Individuals under the age of 18</li>

                                </ul>
                                <p class="body-reg">Thanks for your help in preserving the welcome environment that continues to be so important to our members and a driving factor of kissconnection.com's continuing success.</p>
                            </td></tr>
                        <tr><td colspan="3" class="body-qa"><br></td></tr>
                        <tr><td valign="top" class="body-qa">Q:</td><td colspan="2" class="body-reg-bld">Why should I post my photo on kissconnection.com?</td></tr>
                        <tr><td class="body-qa"><br></td><td valign="top" class="body-qa">A:</td><td class="body-reg">kissconnection.com is committed to helping members find good quality matches, and looking at photos is an important part of most people's selection process. A nice picture of yourself adds personality and presence to your profile. What's more, profiles with photos get browsed eight times more often than those without photos.</td></tr>
                        <tr><td colspan="3" class="body-qa"><br></td></tr> 
                        <tr><td valign="top" class="body-qa">Q:</td><td colspan="2" class="body-reg-bld">What are the guidelines for posting a photo?</td></tr>
                        <tr><td class="body-qa"><br></td><td valign="top" class="body-qa">A:</td><td class="body-reg">
                                <p class="body-reg">kissconnection.com is committed to providing our members with a secure, comfortable environment. We reserve the right to crop or reject any photo at our discretion. Photos will not appear on the site until they are approved by Customer Care. Please adhere to the following guidelines:</p>
                                <p class="body-reg">Photo Content</p>
                                <ul class="body-reg">
                                    <li>Nude, obscene, sexual or otherwise offensive photos will not be posted.</li>
                                    <li>Cartoon characters and photos of celebrities are copyrighted and will not be posted.</li>
                                    <li>You must be in the photo and clearly visible.</li>
                                </ul>
                                <p class="body-reg">Photo Tips</p>
                                <ul class="body-reg">
                                    <li>Choose a recent photo that reflects your current appearance.</li>
                                    <li>Use a photo of yourself alone for your primary image.</li>
                                    <li>Try group photos for other non-primary images.</li>
                                </ul>
                            </td></tr>
                        <tr><td valign="top" class="body-qa">Q:</td><td colspan="2" class="body-reg-bld">How do I post a photo?</td></tr>
                        <tr><td class="body-qa"><br></td><td valign="top" class="body-qa">A:</td><td class="body-reg">
                                <p class="body-reg">kissconnection.com makes it easy for you to post a photo.</p>
                                <p class="body-reg">To upload a photo directly from your computer, simply go to <a href="<?php echo make_admin_url('photo') ?>">Manage My Photos</a>, select "Upload Photo" and follow the instructions.</p>
                                <p class="body-reg">Once approved, uploaded photos will be posted exactly as they are submitted. Be sure to crop your photo if necessary before uploading it. Due to the volume of photos that we receive, it may take 24 to 72 hours for your photo to appear on your profile.</p>
                            </td></tr>
                        <tr><td colspan="3" class="body-qa"><br></td></tr>
                        <tr><td valign="top" class="body-qa">Q:</td><td colspan="2" class="body-reg-bld">I already have a photo, but how do I add others?</td></tr>
                        <tr><td class="body-qa"><br></td><td valign="top" class="body-qa">A:</td><td class="body-reg">Posting additional photos is quick and easy! Simply go to the <a href="<?php echo make_admin_url('photo') ?>">Manage My Photos</a> section, click "Add a Photo" to begin the process to add more photos.</td></tr>
                        <tr><td colspan="3" class="body-qa"><br></td></tr>
                        <tr><td valign="top" class="body-qa">Q:</td><td colspan="2" class="body-reg-bld">How long will it take for my photo to appear?</td></tr>
                        <tr><td class="body-qa"><br></td><td valign="top" class="body-qa">A:</td><td class="body-reg">Our Customer Care team usually approves and posts your photos within 24 to 72 hours.</td></tr>
                        <tr><td colspan="3" class="body-qa"><br></td></tr>
                        <tr><td valign="top" class="body-qa">Q:</td><td colspan="2" class="body-reg-bld">I'm following all the guidelines and still having trouble posting photos. Any suggestions?</td></tr>
                        <tr><td class="body-qa"><br></td><td valign="top" class="body-qa">A:</td><td class="body-reg">If you've followed our <a href="<?php echo make_admin_url('about','photo','photo') ?>">Help With Photos</a> and still have trouble posting your photo, please <a href="<?php echo DIR_WS_SITE . 'contact.php' ?>">contact us</a>.</td></tr>
                        <tr><td colspan="3" class="body-qa"><br></td></tr>
                        <tr><td valign="top" class="body-qa">Q:</td><td colspan="2" class="body-reg-bld">How do I Block/Remove users?</td></tr>
                        <tr><td class="body-qa"><br></td><td valign="top" class="body-qa">A:</td><td class="body-reg">"Search blocking" prevents a member's profile from reappearing in your match results. "Email blocking" stops a member from sending you anonymous email. To do either or both, simply visit  <a href="<?php echo make_admin_url('message', 'blocked', 'blocked') ?>">Manage blocked members</a>.</td></tr>
                        <tr><td colspan="3" class="body-qa"><br></td></tr>
                        <tr><td valign="top" class="body-qa">Q:</td><td colspan="2" class="body-reg-bld">What is Cupid?</td></tr>
                        <tr><td class="body-qa"><br></td><td valign="top" class="body-qa">A:</td><td class="body-reg">Cupid searches our massive database to seek out your matches and delivers the newest profiles directly to your inbox. When there aren't any new matches, Cupid tempts you with some exciting new search results! Follow the links within Cupids' emails, browse the profiles he's found for you, and then connect with your perfect match!</td></tr>
                        <tr><td colspan="3" class="body-qa"><br></td></tr>
                        <tr><td valign="top" class="body-qa">Q:</td><td colspan="2" class="body-reg-bld">Why am I not receiving Cupid messages?</td></tr>
                        <tr><td class="body-qa"><br></td><td valign="top" class="body-qa">A:</td><td class="body-reg">Not getting Cupid? Go to <a href="<?php echo make_admin_url('cupid') ?>">My Cupid</a> and ensure you've checked the Cupid On box.</td></tr>
                        <tr><td colspan="3" class="body-qa"><br></td></tr>
                        <tr><td valign="top" class="body-qa">Q:</td><td colspan="2" class="body-reg-bld">How do I set my Cupid criteria?</td></tr>
                        <tr><td class="body-qa"><br></td><td valign="top" class="body-qa">A:</td><td class="body-reg">To ensure Cupid is sending you the best matches possible, simply <a href="<?php echo make_admin_url('account','update','update') ?>&type=match">Change my matching criteria</a> in your profile. Once you've made changes to the About My Match section, be sure you press Submit to save your new matching criteria.</td></tr>
                        <tr><td colspan="3" class="body-qa"><br></td></tr>
                        <tr><td valign="top" class="body-qa">Q:</td><td colspan="2" class="body-reg-bld">Why is Cupid sending me the "wrong" matches?</td></tr>
                        <tr><td class="body-qa"><br></td><td valign="top" class="body-qa">A:</td><td class="body-reg">
                                <p class="body-reg">To ensure Cupid is sending you the best matches possible, you may need to <a href="<?php echo make_admin_url('account','update','update') ?>&type=match">Change my matching criteria</a> you may find that one or more of your selections (height, gender, age) are not quite right or could be expanded.</p>
                                <p class="body-reg">Remember that when you don't have any new matches, Cupid still helps you by sending exciting new search results!</p> 
                                <p class="body-reg"><strong>NOTE:</strong> It's a good idea to update your profile regularly, as our system will consider you "new" and sends your info out to other members in their Cupid deliveries!</p>
                            </td></tr>
                        <tr><td colspan="3" class="body-qa"><br></td></tr>
                        <tr><td valign="top" class="body-qa">Q:</td><td colspan="2" class="body-reg-bld">How do I turn off Cupid?</td></tr>
                        <tr><td class="body-qa"><br></td><td valign="top" class="body-qa">A:</td><td class="body-reg">
                                <p class="body-reg">Some members find that reducing or increasing the frequency of Cupid emails works better than turning Cupid off completely. Maybe it's the format of the Cupid mail that concerns you. In either case, go to <a href="<?php echo make_admin_url('cupid') ?>">My Cupid</a> and to change the frequency or format. Press Submit, and Cupid will continue to help you meet your match!</p>
                                <p class="body-reg">If you still want to turn off Cupid completely, go to <a href="<?php echo make_admin_url('cupid') ?>">My Cupid</a> and check the Venus Off box. Then press Submit.</p>
                                <p class="body-reg">When you're ready to rekindle your relationship with Cupid, just return to <a href="<?php echo make_admin_url('cupid') ?>">My Cupid</a> and check the Cupid On box.</p>
                            </td></tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div><div class="row-fluid">
  <!--  <div class="panel panel-default">
        <div class="panel-heading">
            <ol class="breadcrumb">
                <li><a href="<?php echo make_admin_url('home') ?>">Home</a></li>
                <li class="active">Frequently Asked Questions</li>
            </ol>
        </div>
        <div class="panel-body">
            <div class="body-reg">
                <table width="100%" border="0" cellspacing="0" cellpadding="2">
                    <tbody><tr><td valign="top" class="body-qa">Q:</td><td colspan="2" class="body-reg-bld">I forgot my username/password. Can you tell me what it is?</td></tr> 
                        <tr><td class="body-qa"><br></td><td valign="top" class="body-qa">A:</td><td class="body-reg">No problem! Just click on <a href="<?php echo DIR_WS_SITE . 'contact.php' ?>">Contact Us</a>, enter either your email address or username, and your date of birth, and we'll send you your password.</td></tr>
                        <tr><td colspan="3" class="body-qa"><br></td></tr>
                        <tr><td valign="top" class="body-qa">Q:</td><td colspan="2" class="body-reg-bld">How do I change my username/password?</td></tr>
                        <tr><td class="body-qa"><br></td><td valign="top" class="body-qa">A:</td><td class="body-reg">Just go to <a href="<?php echo make_admin_url('membership') ?>">My Membership</a> and follow the easy steps!</td></tr>
                        <tr><td colspan="3" class="body-qa"><br></td></tr>
                        <tr><td valign="top" class="body-qa">Q:</td><td colspan="2" class="body-reg-bld">Why can't my username be the same as my email address?</td></tr> 
                        <tr><td class="body-qa"><br></td><td valign="top" class="body-qa">A:</td><td class="body-reg">Your kissconnection.com username is a unique word or phrase that you select; maybe it describes your personality or what you're looking for in a match. Unlike your email address, your kissconnection.com username does not correspond in any way to your identity outside the kissconnection.com community-ensuring that your anonymity is protected until you decide to reveal it.</td></tr>
                        <tr><td colspan="3" class="body-qa"><br></td></tr>
                        <tr><td valign="top" class="body-qa">Q:</td><td colspan="2" class="body-reg-bld">How do I Create a Profile?</td></tr>
                        <tr><td class="body-qa"><br></td><td valign="top" class="body-qa">A:</td><td class="body-reg">Creating and posting a profile is free and takes only a few minutes to go through the easy steps. Think of it as a quick sketch of who you are, your lifestyle, and what counts most in a relationship. Our questionnaire will highlight your personality traits and interests and lets you describe your personality even further in a brief (or lengthy!) narrative. Why wait? <a href="<?php echo DIR_WS_SITE . 'register.php' ?>">Create your profile today!</a> Once your profile's complete, our two-way technology will compare your profile responses with other members, helping you to meet your match!</td></tr>
                        <tr><td colspan="3" class="body-qa"><br></td></tr>
                        <tr><td valign="top" class="body-qa">Q:</td><td colspan="2" class="body-reg-bld">How do I Update My Profile?</td></tr>
                        <tr><td class="body-qa"><br></td><td valign="top" class="body-qa">A:</td><td class="body-reg">It's quick and easy to do! To update your profile, just click <a href="<?php echo make_admin_url('account') ?>">here</a> and follow the simple instructions.</td></tr>
                        <tr><td colspan="3" class="body-qa"><br></td></tr>
                        <tr><td valign="top" class="body-qa">Q:</td><td colspan="2" class="body-reg-bld">What are kissconnection.com's profile guidelines?</td></tr>
                        <tr><td class="body-qa"><br></td><td valign="top" class="body-qa">A:</td><td class="body-reg">
                                <p class="body-reg">kissconnection.com welcomes all single adults seeking one-to-one relationships ranging from companionship to friendship, romance to marriage.</p>
                                <p class="body-reg">Our Quality Assurance team reviews each and every profile to provide our members with a comfortable environment, also ensuring them the best possible chances of making quality matches. We reserve the right to revise our guidelines from time to time, so please check back for updates.</p>
                                <p class="body-reg">We may reject profiles if the username, headline, or narrative text contain any of the following:</p>
                                <ul class="body-reg">
                                    <li>Any direct contact information (i.e., email addresses, URLs, ICQ/instant messenger IDs, phone numbers, full names, addresses)</li>
                                    <li>Any location or descriptive information that threatens a member's anonymity </li>
                                    <li>Abusive language of any kind (i.e., vulgarity, racism) </li>
                                    <li>Discussion or descriptions of illegal acts or behavior (i.e., drug use, violence)</li>
                                    <li>Business or political advertisements or solicitations </li>
                                    <li>Material that exploits or solicits personal information from individuals under the age of 18 </li>
                                    <li>Foreign languages (to effectively monitor profiles, they must be in English) </li>
                                    <li>Solicitation of multiple or additional partners </li>
                                    <li>Unauthorized use of copyrighted or trademarked material </li>
                                    <li>Overt sexual innuendo or discussion</li>
                                </ul>
                                <p class="body-reg">kissconnection.com does not accept content from:</p>
                                <ul class="body-reg">
                                    <li>Incarcerated individuals </li>
                                    <li>Individuals under the age of 18</li>

                                </ul>
                                <p class="body-reg">Thanks for your help in preserving the welcome environment that continues to be so important to our members and a driving factor of kissconnection.com's continuing success.</p>
                            </td></tr>
                        <tr><td colspan="3" class="body-qa"><br></td></tr>
                        <tr><td valign="top" class="body-qa">Q:</td><td colspan="2" class="body-reg-bld">Why should I post my photo on kissconnection.com?</td></tr>
                        <tr><td class="body-qa"><br></td><td valign="top" class="body-qa">A:</td><td class="body-reg">kissconnection.com is committed to helping members find good quality matches, and looking at photos is an important part of most people's selection process. A nice picture of yourself adds personality and presence to your profile. What's more, profiles with photos get browsed eight times more often than those without photos.</td></tr>
                        <tr><td colspan="3" class="body-qa"><br></td></tr> 
                        <tr><td valign="top" class="body-qa">Q:</td><td colspan="2" class="body-reg-bld">What are the guidelines for posting a photo?</td></tr>
                        <tr><td class="body-qa"><br></td><td valign="top" class="body-qa">A:</td><td class="body-reg">
                                <p class="body-reg">kissconnection.com is committed to providing our members with a secure, comfortable environment. We reserve the right to crop or reject any photo at our discretion. Photos will not appear on the site until they are approved by Customer Care. Please adhere to the following guidelines:</p>
                                <p class="body-reg">Photo Content</p>
                                <ul class="body-reg">
                                    <li>Nude, obscene, sexual or otherwise offensive photos will not be posted.</li>
                                    <li>Cartoon characters and photos of celebrities are copyrighted and will not be posted.</li>
                                    <li>You must be in the photo and clearly visible.</li>
                                </ul>
                                <p class="body-reg">Photo Tips</p>
                                <ul class="body-reg">
                                    <li>Choose a recent photo that reflects your current appearance.</li>
                                    <li>Use a photo of yourself alone for your primary image.</li>
                                    <li>Try group photos for other non-primary images.</li>
                                </ul>
                            </td></tr>
                        <tr><td valign="top" class="body-qa">Q:</td><td colspan="2" class="body-reg-bld">How do I post a photo?</td></tr>
                        <tr><td class="body-qa"><br></td><td valign="top" class="body-qa">A:</td><td class="body-reg">
                                <p class="body-reg">kissconnection.com makes it easy for you to post a photo.</p>
                                <p class="body-reg">To upload a photo directly from your computer, simply go to <a href="<?php echo make_admin_url('photo') ?>">Manage My Photos</a>, select "Upload Photo" and follow the instructions.</p>
                                <p class="body-reg">Once approved, uploaded photos will be posted exactly as they are submitted. Be sure to crop your photo if necessary before uploading it. Due to the volume of photos that we receive, it may take 24 to 72 hours for your photo to appear on your profile.</p>
                            </td></tr>
                        <tr><td colspan="3" class="body-qa"><br></td></tr>
                        <tr><td valign="top" class="body-qa">Q:</td><td colspan="2" class="body-reg-bld">I already have a photo, but how do I add others?</td></tr>
                        <tr><td class="body-qa"><br></td><td valign="top" class="body-qa">A:</td><td class="body-reg">Posting additional photos is quick and easy! Simply go to the <a href="<?php echo make_admin_url('photo') ?>">Manage My Photos</a> section, click "Add a Photo" to begin the process to add more photos.</td></tr>
                        <tr><td colspan="3" class="body-qa"><br></td></tr>
                        <tr><td valign="top" class="body-qa">Q:</td><td colspan="2" class="body-reg-bld">How long will it take for my photo to appear?</td></tr>
                        <tr><td class="body-qa"><br></td><td valign="top" class="body-qa">A:</td><td class="body-reg">Our Customer Care team usually approves and posts your photos within 24 to 72 hours.</td></tr>
                        <tr><td colspan="3" class="body-qa"><br></td></tr>
                        <tr><td valign="top" class="body-qa">Q:</td><td colspan="2" class="body-reg-bld">I'm following all the guidelines and still having trouble posting photos. Any suggestions?</td></tr>
                        <tr><td class="body-qa"><br></td><td valign="top" class="body-qa">A:</td><td class="body-reg">If you've followed our <a href="<?php echo make_admin_url('about','photo','photo') ?>">Help With Photos</a> and still have trouble posting your photo, please <a href="<?php echo DIR_WS_SITE . 'contact.php' ?>">contact us</a>.</td></tr>
                        <tr><td colspan="3" class="body-qa"><br></td></tr>
                        <tr><td valign="top" class="body-qa">Q:</td><td colspan="2" class="body-reg-bld">How do I Block/Remove users?</td></tr>
                        <tr><td class="body-qa"><br></td><td valign="top" class="body-qa">A:</td><td class="body-reg">"Search blocking" prevents a member's profile from reappearing in your match results. "Email blocking" stops a member from sending you anonymous email. To do either or both, simply visit  <a href="<?php echo make_admin_url('message', 'blocked', 'blocked') ?>">Manage blocked members</a>.</td></tr>
                        <tr><td colspan="3" class="body-qa"><br></td></tr>
                        <tr><td valign="top" class="body-qa">Q:</td><td colspan="2" class="body-reg-bld">What is Cupid?</td></tr>
                        <tr><td class="body-qa"><br></td><td valign="top" class="body-qa">A:</td><td class="body-reg">Cupid searches our massive database to seek out your matches and delivers the newest profiles directly to your inbox. When there aren't any new matches, Cupid tempts you with some exciting new search results! Follow the links within Cupids' emails, browse the profiles he's found for you, and then connect with your perfect match!</td></tr>
                        <tr><td colspan="3" class="body-qa"><br></td></tr>
                        <tr><td valign="top" class="body-qa">Q:</td><td colspan="2" class="body-reg-bld">Why am I not receiving Cupid messages?</td></tr>
                        <tr><td class="body-qa"><br></td><td valign="top" class="body-qa">A:</td><td class="body-reg">Not getting Cupid? Go to <a href="<?php echo make_admin_url('cupid') ?>">My Cupid</a> and ensure you've checked the Cupid On box.</td></tr>
                        <tr><td colspan="3" class="body-qa"><br></td></tr>
                        <tr><td valign="top" class="body-qa">Q:</td><td colspan="2" class="body-reg-bld">How do I set my Cupid criteria?</td></tr>
                        <tr><td class="body-qa"><br></td><td valign="top" class="body-qa">A:</td><td class="body-reg">To ensure Cupid is sending you the best matches possible, simply <a href="<?php echo make_admin_url('matches') ?>">Change my matching criteria</a> in your profile. Once you've made changes to the About My Match section, be sure you press Submit to save your new matching criteria.</td></tr>
                        <tr><td colspan="3" class="body-qa"><br></td></tr>
                        <tr><td valign="top" class="body-qa">Q:</td><td colspan="2" class="body-reg-bld">Why is Cupid sending me the "wrong" matches?</td></tr>
                        <tr><td class="body-qa"><br></td><td valign="top" class="body-qa">A:</td><td class="body-reg">
                                <p class="body-reg">To ensure Cupid is sending you the best matches possible, you may need to <a href="<?php echo make_admin_url('matches') ?>">Change my matching criteria</a> you may find that one or more of your selections (height, gender, age) are not quite right or could be expanded.</p>
                                <p class="body-reg">Remember that when you don't have any new matches, Cupid still helps you by sending exciting new search results!</p> 
                                <p class="body-reg"><strong>NOTE:</strong> It's a good idea to update your profile regularly, as our system will consider you "new" and sends your info out to other members in their Cupid deliveries!</p>
                            </td></tr>
                        <tr><td colspan="3" class="body-qa"><br></td></tr>
                        <tr><td valign="top" class="body-qa">Q:</td><td colspan="2" class="body-reg-bld">How do I turn off Cupid?</td></tr>
                        <tr><td class="body-qa"><br></td><td valign="top" class="body-qa">A:</td><td class="body-reg">
                                <p class="body-reg">Some members find that reducing or increasing the frequency of Cupid emails works better than turning Cupid off completely. Maybe it's the format of the Cupid mail that concerns you. In either case, go to <a href="<?php echo make_admin_url('cupid', 'list', 'list') ?>">My Cupid</a> and to change the frequency or format. Press Submit, and Cupid will continue to help you meet your match!</p>
                                <p class="body-reg">If you still want to turn off Cupid completely, go to <a href="<?php echo make_admin_url('cupid', 'list', 'list') ?>">My Cupid</a> and check the Venus Off box. Then press Submit.</p>
                                <p class="body-reg">When you're ready to rekindle your relationship with Cupid, just return to <a href="<?php echo make_admin_url('cupid', 'list', 'list') ?>">My Cupid</a> and check the Cupid On box.</p>
                            </td></tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>-->
