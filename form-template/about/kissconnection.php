<div class="row-fluid">
    <div class="panel panel-default">
        <div class="panel-heading">
            <ol class="breadcrumb">
                <li><a href="<?php echo make_admin_url('home') ?>">Home</a></li>
                <li class="active">About KissConnection.Com</li>
            </ol>
        </div>
        <div class="panel-body">
            <div class="body-reg">
                <p class="body-reg">Welcome to kissconnection.com, where you can date, relate and find your life long partner among the web's largest community of quality, eligible singles. Still haven't <a href="<?php echo DIR_WS_SITE; ?>register.php"">registered</a>?</p> 
                <p class="body-reg-bld">Purpose</p>
                <p>kissconnection.com brings people together; we welcome all single adults seeking personal relationships ranging from companionship to friendship, romance to marriage. Our sole purpose is to use a fun, secure online environment to provide our members with quick, meaningful and lasting results. We make it easier and more rewarding for today's single men and women who are searching for convenient and effective ways to meet other quality singles.</p> 
                <p class="body-reg-bld">Team</p>
                <p class="body-reg">kissconnection.com's core team comprises some of the brightest, most active professionals in the world. Beginning with the leadership team, everyone at kissconnection.com is passionate about improving the online dating experience and helping kissconnection.com members meet their ideal partners.</p> 
                <p class="body-reg-bld">Media Inquiries</p>
                <p class="body-reg">For media inquiries, please visit our <a href="<?php echo DIR_WS_SITE; ?>contact.php">Contact Us</a> page.</p> 
            </div>
        </div>
    </div>
</div>