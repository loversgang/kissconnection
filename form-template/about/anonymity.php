<div class="row-fluid">
    <div class="panel panel-default">
        <div class="panel-heading">
            <ol class="breadcrumb">
                <li><a href="<?php echo make_admin_url('home') ?>">Home</a></li>
                <li class="active">Anonymity on KissConnection.com</li>
            </ol>
        </div>
        <div class="panel-body">
            <p class="body-reg-bld">How is the email I send to other members anonymous?</p>
            <p class="body-reg">We respect your privacy and will keep your actual email address confidential. You'll never see it or your real name displayed at kissconnection.com, and all written communication from other members will be directed to username@ kissconnection.com. Then our double-blind e-mail system will forward the messages to your registered email address.</p>
            <p class="body-reg"><strong>To send anonymous email:</strong> From your own email program, be sure to address your messages to: Username@ kissconnection.com. Remember, you can only send anonymous email from your registered email address. Read below to learn more about protecting your anonymity when replying to a message you've received.</p>
            <p class="body-reg"><strong>To reply to email from other kissconnection.com users:</strong> Make sure not to simply hit "reply"! By doing this, you may be sending your actual email address! HELP US PROTECT YOUR PRIVACY! Turn off your auto-signatures and auto-responders. Make certain not to copy (cc:) or forward email to yourself when you send a message from your own email program. When you reply to a Username@ kissconnection.com email address, your real email address will be removed from the "From" line, so the recipient will see only: YourUserName@ kissconnection.com.</p>
            <p class="body-reg"><strong>Receiving Anonymous Email:</strong> You will receive anonymous email at your registered email address, exactly like a regular email message.</p>
            <p class="body-reg"><strong>Saving Correspondence:</strong> We do not save email sent through kissconnection.com. If you want to save email messages that you have sent and received, save copies on your own computer as you would save any other email, or copy and paste the body of your email to a document that you can save.</p>
        </div>
    </div>
</div>