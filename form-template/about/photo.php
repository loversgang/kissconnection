<div class="row-fluid">
    <div class="panel panel-default">
        <div class="panel-heading">
            <ol class="breadcrumb">
                <li><a href="<?php echo make_admin_url('home') ?>">Home</a></li>
                <li><a href="<?php echo make_admin_url('photo') ?>">Manage my photos</a></li>
                <li class="active">Photo Help</li>
            </ol>
        </div>
        <div class="panel-body">
            <table>
                <tbody><tr><td valign="top" class="body-qa">Q:</td><td colspan="2" class="body-reg-bld">How do I post a photo?</td></tr>
                    <tr><td class="body-qa"><br></td><td valign="top" class="body-qa">A:</td><td class="body-reg">
                            <p class="body-reg">kissconnection.com makes it easy for you to post a photo.</p>
                            <p class="body-reg">To upload a photo directly from your computer, simply go to <a href="<?php echo make_admin_url('photo') ?>">Manage My Photos</a>, select "Upload Photo" and follow the instructions.</p>
                            <p class="body-reg">Once approved, uploaded photos will be posted exactly as they are submitted. Be sure to crop your photo if necessary before uploading it. Due to the volume of photos that we receive, it may take 24 to 72 hours for your photo to appear on your profile.</p>
                        </td></tr>
                    <tr><td colspan="3" class="body-qa"><br></td></tr>
                    <tr><td valign="top" class="body-qa">Q:</td><td colspan="2" class="body-reg-bld">What types of images can I upload?</td></tr>
                    <tr><td class="body-qa"><br></td><td valign="top" class="body-qa">A:</td><td class="body-reg">
                            <p class="body-reg">Here are the basic guidelines:</p>
                            <ul class="body-reg">
                                <li>Photo must be in .jpg (.jpeg) or .gif format</li>
                                <li>The file size must be no larger than 200 Kb</li>
                                <li>The file dimensions should not exceed 170 W x 270 H (pixels)</li>
                            </ul>	
                        </td></tr>
                    <tr><td colspan="3" class="body-qa"><br></td></tr>
                    <tr><td valign="top" class="body-qa">Q:</td><td colspan="2" class="body-reg-bld">I already have a photo, but how do I add others?</td></tr>
                    <tr><td class="body-qa"><br></td><td valign="top" class="body-qa">A:</td><td class="body-reg">Posting additional photos is quick and easy! Simply go to the <a href="<?php echo make_admin_url('photo') ?>">Manage My Photos</a> section, click "Add a Photo" to begin the process to add more photos.</td></tr>
                    <tr><td colspan="3" class="body-qa"><br></td></tr>
                    <tr><td valign="top" class="body-qa">Q:</td><td colspan="2" class="body-reg-bld">How long will it take for my photo to appear?</td></tr>
                    <tr><td class="body-qa"><br></td><td valign="top" class="body-qa">A:</td><td class="body-reg">Our Customer Care team usually approves and posts your photos within 24 to 72 hours.</td></tr>
                    <tr><td colspan="3" class="body-qa"><br></td></tr>
                    <tr><td valign="top" class="body-qa">Q:</td><td colspan="2" class="body-reg-bld">I'm following all the guidelines and still having trouble posting photos. Any suggestions?</td></tr>
                    <tr><td class="body-qa"><br></td><td valign="top" class="body-qa">A:</td><td class="body-reg">If you're having trouble posting your photo, please <a href="<?php echo make_admin_url('contact') ?>">contact us</a>.</td></tr>
                </tbody>
            </table>
        </div>
    </div>
</div>