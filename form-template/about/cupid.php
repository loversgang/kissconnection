<div class="row-fluid">
    <div class="panel panel-default">
        <div class="panel-heading">
            <ol class="breadcrumb">
                <li><a href="<?php echo make_admin_url('home') ?>">Home</a></li>
                <li class="active">About My Cupid</li>
            </ol>
        </div>
        <div class="panel-body">
            <table>
                <tr><td valign="top" class="body-qa">Q:</td><td colspan="2" class="body-reg-bld">What is Cupid?</td></tr>
                <tr><td class="body-qa"><br></td><td valign="top" class="body-qa">A:</td><td class="body-reg">Cupid searches our massive database to seek out your matches and delivers the newest profiles directly to your inbox. When there aren't any new matches, Cupid tempts you with some exciting new search results! Follow the links within Cupids' emails, browse the profiles he's found for you, and then connect with your perfect match!</td></tr>
                <tr><td colspan="3" class="body-qa"><br></td></tr>
                <tr><td valign="top" class="body-qa">Q:</td><td colspan="2" class="body-reg-bld">Why am I not receiving Cupid messages?</td></tr>
                <tr><td class="body-qa"><br></td><td valign="top" class="body-qa">A:</td><td class="body-reg">Not getting Cupid? Go to <a href="<?php echo make_admin_url('cupid') ?>">My Cupid</a> and ensure you've checked the Cupid On box.</td></tr>
                <tr><td colspan="3" class="body-qa"><br></td></tr>
                <tr><td valign="top" class="body-qa">Q:</td><td colspan="2" class="body-reg-bld">How do I set my Cupid criteria?</td></tr>
                <tr><td class="body-qa"><br></td><td valign="top" class="body-qa">A:</td><td class="body-reg">To ensure Cupid is sending you the best matches possible, simply <a href="<?php echo make_admin_url('account', 'update', 'update', '&type=match') ?>">Change my matching criteria</a> in your profile. Once you've made changes to the About My Match section, be sure you press Submit to save your new matching criteria.</td></tr><tr><td colspan="3" class="body-qa"><br></td></tr>
                <tr><td valign="top" class="body-qa">Q:</td><td colspan="2" class="body-reg-bld">Why is Cupid sending me the "wrong" matches?</td></tr>
                <tr><td class="body-qa"><br></td><td valign="top" class="body-qa">A:</td><td class="body-reg">
                        <p class="body-reg">To ensure Cupid is sending you the best matches possible, you may need to <a href="<?php echo make_admin_url('account', 'update', 'update', '&type=match') ?>">Change my matching criteria</a> you may find that one or more of your selections (height, gender, age) are not quite right or could be expanded.</p>
                        <p class="body-reg">Remember that when you don't have any new matches, Cupid still helps you by sending exciting new search results!</p> 
                        <p class="body-reg"><strong>NOTE:</strong> It's a good idea to update your profile regularly, as our system will consider you "new" and sends your info out to other members in their Cupid deliveries!</p>
                    </td></tr>
                <tr><td colspan="3" class="body-qa"><br></td></tr>
                <tr><td valign="top" class="body-qa">Q:</td><td colspan="2" class="body-reg-bld">How do I turn off Cupid?</td></tr>
                <tr><td class="body-qa"><br></td><td valign="top" class="body-qa">A:</td><td class="body-reg">
                        <p class="body-reg">Some members find that reducing or increasing the frequency of Cupid emails works better than turning Cupid off completely. Maybe it's the format of the Cupid mail that concerns you. In either case, go to <a href="<?php echo make_admin_url('cupid') ?>">My Cupid</a> and to change the frequency or format. Press Submit, and Cupid will continue to help you meet your match!</p>
                        <p class="body-reg">If you still want to turn off Cupid completely, go to <a href="<?php echo make_admin_url('cupid') ?>">My Cupid</a> and check the Venus Off box. Then press Submit.</p>
                        <p class="body-reg">When you're ready to rekindle your relationship with Cupid, just return to <a href="<?php echo make_admin_url('cupid') ?>">My Cupid</a> and check the Cupid On box.</p>
                    </td></tr>
                </tbody>
            </table>
        </div>
    </div>
</div>