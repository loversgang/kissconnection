<div class="row-fluid">
    <div class="panel panel-default">
        <div class="panel-heading">
            <ol class="breadcrumb">
                <li><a href="<?php echo make_admin_url('home') ?>">Home</a></li>
                <li class="active">Membership Sign Up Complete</li>
            </ol>
        </div>
        <div class="panel-body">
            <p class="body-reg-bld">Thank you for signing up with KissConnection.com!</p>		
            <p class="body-reg">You will be receiving an email shortly confirming your registration.</p>
            <p class="body-reg">Click <a href="<?php echo DIR_WS_SITE; ?>">here</a> to log into KissConnection.com and get started!</p>
            <p class="body-reg">Note: It sometimes takes a few moments for your membership to be finalized in our system, so your member login may not be immediately active.</p>
        </div>
    </div>
</div>