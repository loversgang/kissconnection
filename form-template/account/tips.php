<div class="row-fluid">
    <div class="panel panel-default">
        <div class="panel-heading">
            <ol class="breadcrumb">
                <li><a href="<?php echo make_admin_url('home') ?>">Home</a></li>
                <li><a href="<?php echo make_admin_url('account') ?>">Profile</a></li>
                <li class="active">Profile Tips and Examples</li>
            </ol>
        </div>
        <div class="panel-body">
            <p class="body-reg-bld">Your Headline</p>
            <p class="body-reg">Browsing through Search Results is like being in a room full of potential mates; with so many to choose from, you have to depend on first impressions. Your headline is one of the first impressions you make, so it had better be good!</p> 
            <ul class="body-reg">
                <li><strong>Make it different!</strong><br>"Looking For Love" or "Seeking My Match" is too generic and won't set you apart from the crowd. Remember that your headline is one of the first things other members will see; set aside a few minutes to make it special.</li>
                <li><strong>Play up your best attributes!</strong><br>Pick the most appealing or distinctive aspect of your personality. "Warm, Affectionate Man Seeks Hand To Hold In His," "Gourmet Wants To Cook Romantic Dinners For Two," "Yes! There IS A Woman Who'd Love To Watch Football With You Every Sunday!" It's important to send a message that your match will receive loud and clear.</li>
                <li><strong>Be clever but clear.</strong><br>Don't assume strangers will understand your sense of humor. "Clever Headline TBD" doesn't give anyone a reason to read your profile. "Fat, Ugly And Stupid Seeks Thin, Gorgeous And Brilliant" doesn't work either (would that make you want to click or move on?).</li>
                <li><strong>Be realistic.</strong><br>"Prince Seeks Princess" and "Looking To Live The Fairy Tale Life" suggests that you need to get your feet back on the ground. Try not to set yourself up as an object of pity by using the words "lonely" or "desperate," as in "Lonely Lady Seeks LTR" or "Desperately Seeking Soulmate." Are you looking for someone who is lonely or desperate? Neither is anyone else. And surely there are more enticing ways to describe yourself!</li>
            </ul>
            <p class="body-reg-bld">Your Profile</p>
            <p class="body-reg">Once your headline makes someone want to learn more about you, the trick is to keep his or her attention. Don't feel like you have to sell yourself; just be open and honest. Ask a friend to help you write your profile, and have another friend read it afterwards. Keep the following advice in mind to help you create a winning profile.</p>
            <ul class="body-reg">
                <li><strong>Get to the point!</strong><br>Avoid beginning by complaining about how hard it is to write a profile or find a quality mate; everyone here has to do just that. Dive right into describing yourself and what makes you tick.</li>
                <li><strong>Focus on your strengths!</strong><br>Write about your hobbies, involvement in your community, interesting work or travels-whatever it is that makes you special. Think about your ideal match, and write as though you're talking specifically to that person.</li>
                <li><strong>Be honest!</strong><br>An "avid polo player" is not someone who started taking lessons last weekend. The walk from your desk to the parking lot-no matter how briskly you do it-does not count as "exercises daily". Remember that your match will assume everything you write to be true; once you decide to meet offline, you don't want any uncomfortable surprises.</li>
                <li><strong>Be realistic!</strong><br>The words you choose can alienate potential matches, so go easy on phrases like "drop-dead gorgeous" and "looking for the perfect mate." Set your expectations high, but keep them real too.</li>
                <li><strong>Be open and conversational!</strong><br>How many times have you read a profile that tries to impress by using vocabulary-busting words-words that inevitably are misspelled? It ruins the effect. Or maybe you've come across profiles that say too little, leaving you wondering why anyone would want to contact this person. Write enough to get your message across, but use words that would come out of your mouth normally. In short, just be yourself on a particularly good day.</li>
                <li><strong>Describe what's important to you!</strong><br>Don't be afraid to mention qualities that are important to you in a relationship; loyalty, the ability to communicate and listen, intelligence and humor are good examples. Put those qualities front and center, and avoid emphasizing characteristics that are less important to you. Give some thought to why your best relationships have worked well and why the worst worked so badly; maybe you'll discover a pattern there.</li>
                <li><strong>Check your spelling and grammar!</strong><br>Your profile tells your potential matches what to expect from you in an offline conversation; it's all anyone really has to determine your personality and your ability to communicate. Although it might be completely unfair to assume, misspelled words can make people judge you as being uneducated or illiterate. Take a few extra minutes to check your spelling. Try writing your profile in a word processing application first; run spell check, make corrections and copy the text into your profile. </li>
            </ul>
            <p class="body-reg-bld">Kissconnection.com's Profile Guidelines</p>
            <p class="body-reg">kissconnection.com welcomes all single adults seeking one-to-one relationships ranging from companionship to friendship, romance to marriage.</p>
            <p class="body-reg">Our Quality Assurance team reviews each and every profile to provide our members with a comfortable environment, also ensuring them the best possible chances of making quality matches. We reserve the right to revise our guidelines from time to time, so please check back for updates.</p>
            <p class="body-reg">We may reject profiles if the username, headline, or narrative text contain any of the following:</p>
            <ul class="body-reg">
                <li>Any direct contact information (i.e., email addresses, URLs, ICQ/instant messenger IDs, phone numbers, full names, addresses) </li>
                <li>Any location or descriptive information that threatens a member's anonymity </li>
                <li>Abusive language of any kind (i.e., vulgarity, racism) </li>
                <li>Discussion or descriptions of illegal acts or behavior (i.e., drug use, violence) </li>
                <li>Business or political advertisements or solicitations </li>
                <li>Material that exploits or solicits personal information from individuals under the age of 18 </li>
                <li>Foreign languages (to effectively monitor profiles, they must be in English) </li>
                <li>Solicitation of multiple or additional partners </li>
                <li>Unauthorized use of copyrighted or trademarked material </li>
                <li>Overt sexual innuendo or discussion </li>
            </ul>
            <p class="body-reg">kissconnection.com does not accept content from:</p> 
            <ul class="body-reg">
                <li>Incarcerated individuals </li>
                <li>Individuals under the age of 18 </li>
            </ul>
            <p class="body-reg">Thanks for your help in preserving the welcome environment that continues to be so important to our members and a driving factor of kissconnection.com's continuing success.</p> 

        </div>
    </div>
</div>