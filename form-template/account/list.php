<div class="row-fluid">
    <div class="panel panel-default">
        <div class="panel-heading">
            <ol class="breadcrumb">
                <li><a href="<?php echo make_admin_url('home') ?>">Home</a></li>
                <li class="active">My Profile</li>
                <li class="right_breadcrumb"><a href="<?php echo make_admin_url('account', 'view', 'view') . '&id=' . $user_id ?>">View Your Published Profile</a></li>
            </ol>
        </div>
        <div>
            <ol class="breadcrumb">
            </ol>
        </div>
        <div class="panel-body">
            <?php display_message(1); ?>
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Photos <span class="pull-right"><a href="<?php echo make_admin_url('photo') ?>">Manage Photos</a></span>
                    </div>
                    <div class="panel-body">
                        <?php if ($photo) { ?>
                            <div class="wrapper account">
                                <div class="">
                                    <img src="<?php echo DIR_WS_SITE_FILE . $photo->file_name; ?>" class="img img-responsive img-thumbnail" style="width: 100%;height: 260px !important;"/>
                                </div>
                                <?php if ($photo->caption) { ?>
                                    <div class="desc_content" style=""><?php echo $photo->caption; ?></div>
                                <?php } ?>
                                <span class="carousel-caption"><i class="fa fa-camera icon-class">  <?php echo $photos_count - 1 ?></i></span>
                            </div>
                        <?php } else { ?>
                            <img src="<?php echo DIR_WS_SITE_IMAGE . 'default.png'; ?>" class="img img-responsive img-thumbnail" style="width: 100%;"/>
                        <?php } ?>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Videos <span class="pull-right"><a href="<?php echo make_admin_url('video') ?>">Manage Videos</a></span>
                    </div>
                    <div class="panel-body">
                        <?php if ($video) { ?>
                            <div class="wrapper">
                                <div class="videos_class account">
                                    <iframe  src="https://www.youtube.com/embed/<?php echo $video->video_code ?>" frameborder="0" allowfullscreen class="img img-responsive img-thumbnail"></iframe>

                                </div>
                                <?php if ($video->caption) { ?>
                                    <div class="desc_content" style=""><?php echo $video->caption; ?></div>
                                <?php } ?>
                            </div>
                        <?php } else { ?>
                            <div class="alert alert-info">No Video Uploaded!</div>
                        <?php } ?>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Profile Visibility
                    </div>
                    <div class="panel-body">
                        <div class="alert alert-info">
                            Your profile is currently :
                            <?php
                            echo $user->is_visible == 1 ? 'Visible' : 'Hidden';
                            ?>
                        </div>
                        <form method="post" class="from form-horizontal">
                            <div class="radio">
                                <label>
                                    <input type="radio" name="is_visible" value="1" <?php echo ($user->is_visible == 1) ? 'checked' : '' ?> /> Yes
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="is_visible" value="0" <?php echo ($user->is_visible == 0) ? 'checked' : '' ?> /> No
                                </label>
                            </div>
                            <div class="">
                                <input type="submit" name="profile_visible" value="Update" class="btn btn-success btn-xs"/>
                            </div>
                        </form>
                        <br/>
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#About">About</a></li>
                    <li><a data-toggle="tab" href="#photos">Photos (<?php echo $photos_count ?>)</a></li>
                    <li><a data-toggle="tab" href="#videos">Videos (<?php echo count($videos) ?>)</a></li>
                    <li><a data-toggle="tab" href="#Narratives">Narratives</a></li>
                    <li><a data-toggle="tab" href="#Personality">Personality</a></li>
                    <li><a data-toggle="tab" href="#Match">About Match</a></li>
                </ul>
                <div class="tab-content">
                    <div id="About" class="tab-pane fade in active">
                        <ul class="list-group">
                            <li class="list-group-item" style="border-top: 0px;text-align: right"><a href="<?php echo make_admin_url('account', 'update', 'update') ?>">Edit</a></li>
                            <li class="list-group-item radio_btns">
                                <table class="table table-bordered">
                                    <?php foreach ($metaDataAbout as $meta) { ?>
                                        <tr>
                                            <td>
                                                <b><?php echo $meta->title; ?></b>
                                            </td>
                                            <td>
                                                <?php echo str_replace('||', ',', metaData::getMetaValue($meta->id)); ?>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </table>
                            </li>
                        </ul>
                    </div>
                    <div id="photos" class="tab-pane fade">
                        <li class="list-group-item" style="border-top: 0px;text-align: right"><a href="<?php echo make_admin_url('photo', 'list', 'list') ?>">Upload Photo</a></li>
                        <br />
                        <?php if ($photos) { ?>
                            <?php foreach ($photos as $photo_get) { ?>
                                <div class="col-md-4" style="margin-bottom: 10px">
                                    <img src="<?php echo DIR_WS_SITE . 'assets/files/' . $photo_get->file_name ?>" class="img img-responsive img-thumbnail" style="width: 100%;height: 100px" /><br />
                                    <a href="<?php echo make_admin_url('photo', 'delete', 'delete', 'pid=' . $photo_get->id . '&type=account') ?>" class="btn btn-danger btn-xs tool" title="Click Here To Delete" onclick="return confirm('Are you sure? You are deleting this record.');"><i class="fa fa-trash"></i></a> |

                                    <a href="<?php echo make_admin_url('photo', 'change', 'change', 'pid=' . $photo_get->id . '&type=account') ?>" class="btn btn-success btn-xs tool" title="Make this Your Primary Image" ><i class="fa fa-picture-o"></i></a>
                                </div>
                            <?php } ?> 
                        <?php } else { ?> 
                            <div class="alert alert-info">No Photo Uploaded</div>
                        <?php } ?> 
                    </div>
                    <div id="videos" class="tab-pane fade">
                        <li class="list-group-item" style="border-top: 0px;text-align: right"><a href="<?php echo make_admin_url('video', 'list', 'list') ?>">Upload Video</a></li>
                        <br />
                        <?php if (count($videos) > 0) { ?>
                            <?php foreach ($videos as $videos_get) { ?>
                                <div class="col-md-4" style="margin-bottom: 10px">
                                    <img src="https://i.ytimg.com/vi/<?php echo $videos_get->video_code ?>/hqdefault.jpg" class="img img-responsive tool youtube_id img-thumbnail" title="Click Here To View" data-key="<?php echo $videos_get->video_code ?>" /><br />
                                    <a href="<?php echo make_admin_url('video', 'delete', 'delete', 'vid=' . $videos_get->id . '&type=video&redirect=account') ?>" class="btn btn-danger btn-xs tool" title="Click Here To Delete" onclick="return confirm('Are you sure? You are deleting this record.');"><i class="fa fa-trash"></i></a>
                                    <a href="<?php echo make_admin_url('video', 'change', 'change', 'vid=' . $videos_get->id . '&redirect=account') ?>" class="btn btn-success btn-xs tool" title="Make this Your Primary Image" ><i class="fa fa-picture-o"></i></a>
                                </div>
                            <?php } ?> 
                        <?php } else { ?>
                            <div class="alert alert-info">No Video Uploaded</div>
                        <?php } ?> 
                    </div>
                    <div id="Narratives" class="tab-pane fade">
                        <ul class="list-group">
                            <li class="list-group-item" style="border-top: 1px;text-align: right"><a href="<?php echo make_admin_url('account', 'update', 'update', 'type=narratives') ?>">Edit</a></li>
                            <li class="list-group-item radio_btns">
                                <?php foreach ($metaDataNarratives as $meta) { ?>
                                    <ul class="list-group">
                                        <li class="list-group-item active_c"><?php echo $meta->title; ?></li>
                                        <li class="list-group-item radio_btns">
                                            <?php echo str_replace('||', ',', metaData::getMetaValue($meta->id)); ?>
                                        </li>
                                    </ul>
                                <?php } ?>
                            </li>
                        </ul>
                    </div>
                    <div id="Personality" class="tab-pane fade">
                        <ul class="list-group">
                            <li class="list-group-item" style="border-top: 0px;text-align: right"><a href="<?php echo make_admin_url('account', 'update', 'update', 'type=personality') ?>">Edit</a></li>
                            <li class="list-group-item radio_btns">
                                <table class="table table-bordered">
                                    <?php foreach ($metaDataPersonality as $meta) { ?>
                                        <tr>
                                            <td>
                                                <b><?php echo $meta->title; ?></b>
                                            </td>
                                            <td>
                                                <?php echo str_replace('||', ',', metaData::getMetaValue($meta->id)); ?>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </table>
                            </li>
                        </ul>
                    </div>
                    <div id="Match" class="tab-pane fade">
                        <ul class="list-group">
                            <li class="list-group-item" style="border-top: 0px;text-align: right"><a href="<?php echo make_admin_url('account', 'update', 'update', 'type=match') ?>">Edit</a></li>
                            <li class="list-group-item radio_btns">
                                <table class="table table-bordered">
                                    <?php foreach ($metaDataMatch as $meta) { ?>
                                        <tr>
                                            <td>
                                                <b><?php echo $meta->title; ?></b>
                                            </td>
                                            <td>
                                                <?php echo str_replace('||', ',', metaData::getMetaValue($meta->id)); ?>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </table>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="youtube_model">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Video</h4>
            </div>
            <div class="modal-body">


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary stop_player" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).on('click', '.youtube_id', function () {
        var iframe = $(this).attr('data-key');
        $('.modal-body').html('<iframe width="854" height="580" src="https://www.youtube.com/embed/' + iframe + '" frameborder="0" allowfullscreen class="img img-responsive pop_video" style="height: 373px;"></iframe>');
        $('#youtube_model').modal({backdrop: 'static', keyboard: false});
    });

    $(document).on('click', '.stop_player', function () {
        $('.pop_video').remove();
    });
</script>