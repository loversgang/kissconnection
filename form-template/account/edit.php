 <div class="row-fluid <?php echo $type; ?>">
    <div class="panel panel-default">
        <div class="panel-heading">
            <ol class="breadcrumb">
                <li><a href="<?php echo make_admin_url('home') ?>">Home</a></li>
                <li><a href="<?php echo make_admin_url('account') ?>">Profile</a></li>
                <li class="active">Edit My Basic Profile </li>
            </ol>
        </div>
        <div class="panel-body">
            <?php display_message(1); ?>
            <form action="" method="POST">
                <?php if ($type === 'about') { ?>
                    <ul class="list-group">
                        <li class="list-group-item active_c">General Information</li>
                        <li class="list-group-item radio_btns">
                            <div class="col-md-4" style="padding: 0">
                                <label class="control-label">Birthday</label>
                                <div class="clearfix"></div>
                                <div class="col-md-4" style="padding-left: 0">
                                    <select class="form-control" name="birth_month" style="padding: 6px 10px;">
                                        <?php for ($month = 1; $month <= 12; $month++) { ?>
                                            <option value="<?php echo $month; ?>" <?php echo $month == $birthmonth ? 'selected' : ''; ?>><?php echo $month; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="col-md-4" style="padding-left: 0">
                                    <select class="form-control" name="birth_day" style="padding: 6px 10px;">
                                        <?php for ($day = 1; $day <= 31; $day++) { ?>
                                            <option value="<?php echo $day; ?>" <?php echo $day == $birthday ? 'selected' : ''; ?>><?php echo $day; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="col-md-4" style="padding-left: 0">
                                    <select class="form-control" name="birth_year" style="padding: 6px 2px;">
                                        <?php for ($year = 1900; $year <= date('Y'); $year++) { ?>
                                            <option value="<?php echo $year; ?>" <?php echo $year == $birthyear ? 'selected' : ''; ?>><?php echo $year; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label class="control-label">Gender</label>
                                    <select class="form-control" name="gender">
                                        <option value="male">Male</option>
                                        <option value="female" <?php echo $myself->gender == 'female' ? 'selected' : '' ?>>Female</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <label class="control-label">Height</label>
                                <div class="clearfix"></div>
                                <div class="col-md-6" style="padding-left: 0">
                                    <div class="input-group">
                                        <select name="foot" class="form-control" style="padding: 6px 6px;">
                                            <?php for ($foot = 3; $foot <= 8; $foot++) { ?>
                                                <option value="<?php echo $foot; ?>" <?php echo $foot == $myself->foot ? 'selected' : ''; ?>><?php echo $foot; ?></option>
                                            <?php } ?>
                                        </select>
                                        <div class="input-group-addon">ft.</div>
                                    </div>
                                </div>
                                <div class="col-md-6" style="padding-left: 0">
                                    <div class="input-group">
                                        <select name="inch" class="form-control" style="padding: 6px 6px;">
                                            <?php for ($inch = 0; $inch <= 11; $inch++) { ?>
                                                <option value="<?php echo $inch; ?>" <?php echo $inch == $myself->inch ? 'selected' : ''; ?>><?php echo $inch; ?></option>
                                            <?php } ?>
                                        </select>
                                        <div class="input-group-addon">in.</div>
                                </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label class="control-label">Profile Zip Code</label>
                                    <input value="<?php echo $myself->zip; ?>" type="number" name="zip" class="form-control" required/>
                                </div>
                            </div>
                        </li>
                    </ul>
                <?php } ?>
                <hr/>

                <?php
                foreach ($metaData as $meta) {
                    $type = $meta->type;
                    $obj = new metaOptions;
                    $values = $obj->getMetaOptionsByMetaId($meta->id);
                    switch ($type) {
                        case 'radio':
                            $existing_value = array_key_exists($meta->id, $user_meta_array) ? $user_meta_array[$meta->id] : $meta->default_value;
                            ?>
                            <div class="col-md-6 <?php echo strtolower(ucfirst($meta->title)); ?>">
                                <ul class="list-group">
                                    <li class="list-group-item active_c"><?php echo ucfirst($meta->title); ?></li>
                                    <li class="list-group-item radio_btns">
                                        <?php
                                        foreach ($values as $value) {
                                            ?>
                                            <div class="col-md-4">
                                                <?php generateHTML($value->value, $meta->id, 'radio', $meta->default_value, $existing_value); ?>
                                            </div>
                                        <?php } ?>
                                    </li>
                                </ul>
                            </div>
                            <?php
                            break;
                        case 'checkbox':
                            $existing_value = array_key_exists($meta->id, $user_meta_array) ? explode('||', $user_meta_array[$meta->id]) : array($meta->default_value);
                            ?>
                            <div class="col-md-6 <?php echo strtolower(ucfirst($meta->title)); ?>" >
                                <ul class="list-group">
                                    <li class="list-group-item active_c"><?php echo ucfirst($meta->title); ?></li>
                                    <li class="list-group-item radio_btns">
                                        <?php
                                        foreach ($values as $value) {
                                            ?>
                                            <div class="col-md-4">
                                                <?php generateHTML($value->value, $meta->id, 'checkbox', $meta->default_value, $existing_value); ?>
                                            </div>
                                        <?php } ?>
                                    </li>
                                </ul>
                            </div>
                            <?php
                            break;
                        case'select':
                            ?>
                <div class="col-md-6 <?php echo strtolower(ucfirst($meta->title)); ?>">
                                <ul class="list-group">
                                    <li class="list-group-item active_c question_heading"><?php echo ucfirst($meta->title); ?></li>
                                    <li class="list-group-item radio_btns">
                                        <select class="form-control" name="<?php echo $meta->id; ?>">
                                            <?php
                                            foreach ($values as $value) {
                                                ?>
                                                <?php generateHTML($value->value, $meta->id, 'select', $meta->default_value, $existing_value); ?>
                                            <?php } ?>
                                        </select>
                                    </li>
                                </ul>
                            </div>
                            <?php
                            break;
                        case'textarea':
                            $existing_value = array_key_exists($meta->id, $user_meta_array) ? explode('||', $user_meta_array[$meta->id]) : $existing_value[$meta->default_value];  $meta_val=str_replace('||', ',', metaData::getMetaValue($meta->id));
                            ?>
                            <ul class="list-group">
                                <li class="list-group-item active_c"><?php echo ucfirst($meta->title); ?></li>
                                <li class="list-group-item radio_btns">
                                    <?php generateHTML($meta_val, $meta->id, 'textarea'); ?>
                                </li>
                            </ul>
                            <?php
                            break;
                        default:
                            break;
                    }
                    ?>
                <?php } ?>

                <?php if ($type == 'personality') { ?>
                    <div class="col-md-12">
                        <table class="table table-bordered" style="text-align: center">
                            <thead>
                                <tr>
                                    <th style="text-align: center">Pets</th>
                                    <th style="text-align: center">I Have</th>
                                    <th style="text-align: center">Don't Have, But Like</th>
                                    <th style="text-align: center">Don't Have, Don't Like</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                foreach ($metaDataPets as $meta) {
                                    $type = $meta->type;
                                    $obj = new metaOptions;
                                    $values = $obj->getMetaOptionsByMetaId($meta->id);
                                    ?>
                                    <tr>
                                        <td>
                                            <b><?php echo $meta->title ?></b>
                                        </td>
                                        <?php foreach ($values as $value) { ?>
                                            <td>
                                                <input type="radio" name="<?php echo $meta->id ?>" value="<?php echo $value->value ?>" <?php echo $value->value == "Don't Have, Don't Like" ? 'checked' : '' ?>/>
                                            </td>
                                        <?php } ?>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-12">
                        <table class="table table-bordered" style="text-align: center">
                            <thead>
                                <tr>
                                    <th style="text-align: center">Turn Ons / Turn Offs</th>
                                    <th style="text-align: center">Turn On</th>
                                    <th style="text-align: center">Turn Off</th>
                                    <th style="text-align: center">Take It Or Leave It</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                foreach ($metaDataTurn as $meta) {
                                    $type = $meta->type;
                                    $obj = new metaOptions;
                                    $values = $obj->getMetaOptionsByMetaId($meta->id);
                                    ?>
                                    <tr>
                                        <td>
                                            <b><?php echo $meta->title ?></b>
                                        </td>
                                        <?php foreach ($values as $value) { ?>
                                            <td>
                                                <input type="radio" name="<?php echo $meta->id ?>" value="<?php echo $value->value ?>" <?php echo $value->value == 'Take It Or Leave It' ? 'checked' : '' ?>/>
                                            </td>
                                        <?php } ?>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                <?php } ?>
                <div class="clearfix"></div>
                <div class="col-md-12" style="text-align: center;">
                    <input type="submit" name="submit" value="Save" class="btn btn-success"/>
                    <a href="<?php echo DIR_WS_SITE; ?>" class="btn btn-danger">Cancel</a>
                </div>
            </form>
        </div>
    </div>
</div>