<div class="row-fluid">
    <div class="panel panel-default">
        <div class="panel-heading">
            <ol class="breadcrumb">
                <li><a href="<?php echo make_admin_url('home') ?>">Home</a></li>
                <li><a href="<?php echo make_admin_url('account') ?>">Profile</a></li>
                <li class="active">Member Options Overview</li>
            </ol>
        </div>
        <div class="panel-body">
            <div class="col-md-6">
                <div class="list-group">
                    <a href="#" class="list-group-item active_c">Profile</a>
                    <a href="<?php echo make_admin_url('account'); ?>" class="list-group-item">Edit my profile</a>
                    <a href="<?php echo make_admin_url('account', 'tips', 'tips'); ?>" class="list-group-item">Profile tips and examples</a>
                </div>
                <div class="list-group">
                    <a href="#" class="list-group-item active_c">Email/Messaging Options</a>
                    <a href="<?php echo make_admin_url('membership'); ?>" class="list-group-item">Change my email address</a>
                    <a href="<?php echo make_admin_url('message', 'how', 'how'); ?>" class="list-group-item">How to send a message to another member</a>
                    <a href="<?php echo make_admin_url('message', 'blocked', 'blocked'); ?>" class="list-group-item">Manage blocked members</a>
                    <a href="<?php echo DIR_WS_SITE; ?>contact.php" class="list-group-item">Email KissConnection.com</a>
                </div>
                <div class="list-group">
                    <a href="#" class="list-group-item active_c">Help Desk</a>
                    <a href="<?php echo make_admin_url('account', 'options', 'options'); ?>" class="list-group-item">Advice and Tips</a>
                    <a href="<?php echo make_admin_url('about', 'help', 'help'); ?>" class="list-group-item">Help/Troubleshooting</a>
                    <a href="<?php echo DIR_WS_SITE; ?>terms_and_conditions.php" class="list-group-item">Terms of Use</a>
                    <a href="<?php echo DIR_WS_SITE; ?>privacy_policy.php" class="list-group-item">Privacy Policy</a>
                </div>
                <div class="list-group">
                    <a href="#" class="list-group-item active_c">Auto-login</a>
                    <a href="<?php echo make_admin_url('membership'); ?>" class="list-group-item">Turn Auto-login On/Off</a>
                    <a href="<?php echo make_admin_url('membership'); ?>" class="list-group-item">About Auto-login</a>
                </div>
            </div>
            <div class="col-md-6">
                <div class="list-group">
                    <a href="#" class="list-group-item active_c">Photos</a>
                    <a href="<?php echo make_admin_url('photo') ?>" class="list-group-item">Manage my photos</a>
                    <a href="<?php echo make_admin_url('video') ?>" class="list-group-item">Manage my videos</a>
                    <a href="<?php echo make_admin_url('about', 'photo', 'photo') ?>" class="list-group-item">Help with photos</a>
                </div>
                <div class="list-group">
                    <a href="#" class="list-group-item active_c">Membership</a>
                    <a href="<?php echo make_admin_url('membership'); ?>" class="list-group-item">Change my membership information</a>
                </div>
                <div class="list-group">
                    <a href="#" class="list-group-item active_c">Cupid</a>
                    <a href="<?php echo make_admin_url('cupid') ?>" class="list-group-item">Turn Cupid on/off</a>
                    <a href="<?php echo make_admin_url('account', 'update', 'update', '&type=match') ?>" class="list-group-item">Change my match criteria</a>
                    <a href="<?php echo make_admin_url('about', 'cupid', 'cupid'); ?>" class="list-group-item">About Cupid</a>
                </div>
                <div class="list-group">
                    <a href="#" class="list-group-item active_c">Privacy and Security</a>
                    <a href="<?php echo DIR_WS_SITE; ?>privacy_policy.php" class="list-group-item">Your privacy</a>
                    <a href="<?php echo make_admin_url('about', 'anonymity', 'anonymity') ?>" class="list-group-item">Anonymity on KissConnection.com</a>
                </div>
            </div>
        </div>
    </div>
</div>