<div class="row-fluid">
    <div class="panel panel-default">
        <div class="panel-heading">
            <ol class="breadcrumb">
                <li><a href="<?php echo make_admin_url('home') ?>">Home</a></li>
                <li class="active">Profile</li>
            </ol>
        </div>
        <div class="panel-body">
            <?php display_message(1); ?>
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel panel-heading">
                        Photos
                    </div>
                    <div class="panel panel-body">
                        <?php if ($photo) { ?>
                            <div class="wrapper account">
                                <div class="">
                                    <img src="<?php echo DIR_WS_SITE_FILE . $photo->file_name; ?>" class="img img-responsive img-thumbnail" style="width: 100%;height: 260px !important;"/>
                                </div>
                                <?php if ($photo->caption) { ?>
                                    <div class="desc_content" style=""><?php echo $photo->caption; ?></div>
                                <?php } ?>
                                <span class="carousel-caption"><i class="fa fa-camera icon-class">  <?php echo $photos_count - 1 ?></i></span>
                            </div>
                        <?php } else { ?>
                            <img src="<?php echo DIR_WS_SITE_IMAGE . 'default.png'; ?>" class="img img-responsive img-thumbnail" style="width: 100%;margin-bottom:10px"/>
                        <?php } ?>
                        <?php if ($is_block) { ?>
                            <a href="<?php echo make_admin_url('block', 'list', 'list&type=unblock&block=' . $id) ?>" onclick="return confirm('Are You Sure')" class="btn btn-primary form-control"><i class="fa fa-male"></i>   UNBLOCK <?php echo $user->gender == 'male' ? 'HIM' : 'HER' ?></a><br /><br />

                        <?php } else { ?>
                            <a href="<?php echo make_admin_url('block', 'list', 'list&block=' . $id) ?>" onclick="return confirm('Are You Sure')" class="btn btn-primary form-control"><i class="fa fa-male"></i>   BLOCK <?php echo $user->gender == 'male' ? 'HIM' : 'HER' ?></a><br /><br />

                        <?php } ?>
                        <a href="<?php echo make_admin_url('message', 'compose', 'compose&reply_username=' . $user->username) ?>" class="btn btn-primary form-control"><i class="fa fa-envelope"></i> EMAIL <?php echo $user->gender == 'male' ? 'HIM' : 'HER' ?></a>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel panel-heading">
                        Videos
                    </div>
                    <div class="panel panel-body">
                        <?php if ($video) { ?>
                            <div class="wrapper">
                                <div class="videos_class account">
                                    <iframe  src="https://www.youtube.com/embed/<?php echo $video->video_code ?>" frameborder="0" allowfullscreen class="img img-responsive"></iframe>
                                </div>
                                <?php if ($video->caption) { ?>
                                    <div class="desc_content" style=""><?php echo $video->caption; ?></div>
                                <?php } ?>
                            </div>
                        <?php } else { ?>
                            <div class="alert alert-info">No Video Uploaded!</div>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#About">About</a></li>
                    <li><a data-toggle="tab" href="#photos">Photos (<?php echo $photos_count ?>)</a></li>
                    <li><a data-toggle="tab" href="#videos">Videos (<?php echo count($videos) ?>)</a></li>
                    <li class="active"><a data-toggle="tab" href="#Narratives">Narratives</a></li>
                    <li><a data-toggle="tab" href="#Personality">Personality</a></li>
                    <li><a data-toggle="tab" href="#Match">About Match</a></li>
                </ul>
                <div class="tab-content">
                    <div id="About" class="tab-pane fade in active">
                        <br />
                        <table class="table table-bordered">
                            <?php foreach ($metaDataAbout as $meta) { ?>
                                <tr>
                                    <td>
                                        <b><?php echo $meta->title; ?></b>
                                    </td>
                                    <td>
                                        <?php echo str_replace('||', ',', metaData::getMetaValue($meta->id, $id)); ?>
                                    </td>
                                </tr>
                            <?php } ?>
                        </table>
                    </div>
                    <div id="photos" class="tab-pane fade">
                        <br />
                        <?php if ($photos) { ?>
                            <?php foreach ($photos as $photo_get) { ?>
                                <div class="col-md-4" style="margin-bottom: 10px">
                                    <img src="<?php echo DIR_WS_SITE . 'assets/files/' . $photo_get->file_name ?>" class="img img-responsive img-thumbnail" style="width: 100%;height: 100px">
                                </div>
                            <?php } ?> 
                        <?php } else { ?> 
                            <div class="alert alert-info">No Photo Uploaded</div>
                        <?php } ?> 
                    </div>
                    <div id="videos" class="tab-pane fade">
                        <br />
                        <?php if (count($videos) > 0) { ?>
                            <?php foreach ($videos as $videos_get) { ?>
                                <div class="col-md-4" style="margin-bottom: 10px">
                                    <img src="https://i.ytimg.com/vi/<?php echo $videos_get->video_code ?>/hqdefault.jpg" class="img img-responsive tool youtube_id img-thumbnail" title="Click Here To View" data-key="<?php echo $videos_get->video_code ?>" />
                                </div>
                            <?php } ?> 
                        <?php } else { ?>
                            <div class="alert alert-info">No Video Uploaded</div>
                        <?php } ?> 
                    </div>
                    <div id="Narratives" class="tab-pane fade">
                        <br />
                        <?php foreach ($metaDataNarratives as $meta) { ?>
                            <ul class="list-group">
                                <li class="list-group-item active_c"><?php echo $meta->title; ?></li>
                                <li class="list-group-item radio_btns">
                                    <?php echo str_replace('||', ',', metaData::getMetaValue($meta->id, $id)); ?>
                                </li>
                            </ul>
                        <?php } ?>
                    </div>
                    <div id="Personality" class="tab-pane fade">
                        <br />
                        <table class="table table-bordered">
                            <?php foreach ($metaDataPersonality as $meta) { ?>
                                <tr>
                                    <td>
                                        <b><?php echo $meta->title; ?></b>
                                    </td>
                                    <td>
                                        <?php echo str_replace('||', ',', metaData::getMetaValue($meta->id, $id)); ?>
                                    </td>
                                </tr>
                            <?php } ?>
                        </table>
                    </div>
                    <div id="Match" class="tab-pane fade">
                        <br />
                        <table class="table table-bordered">
                            <?php foreach ($metaDataMatch as $meta) { ?>
                                <tr>
                                    <td>
                                        <b><?php echo $meta->title; ?></b>
                                    </td>
                                    <td>
                                        <?php echo str_replace('||', ',', metaData::getMetaValue($meta->id, $id)); ?>
                                    </td>
                                </tr>
                            <?php } ?>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="youtube_model">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Video</h4>
            </div>
            <div class="modal-body">


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary stop_player" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).on('click', '.youtube_id', function () {
        var iframe = $(this).attr('data-key');
        $('.modal-body').html('<iframe width="854" height="580" src="https://www.youtube.com/embed/' + iframe + '" frameborder="0" allowfullscreen class="img img-responsive pop_video" style="height: 373px;"></iframe>');
        $('#youtube_model').modal({backdrop: 'static', keyboard: false});
    });

    $(document).on('click', '.stop_player', function () {
        $('.pop_video').remove();
    });
</script>