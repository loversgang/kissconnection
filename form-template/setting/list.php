<div class="page-content">
    <div class="content-block">
        <h2 class="page_title">Settings</h2>
        <div id="show_notification_message"></div>
        <div class="contactform">
            <form method="post" action="<?php echo 'admin.php#!/' . make_load_url('setting') ?>">
                <input type="text" name="email" value="<?php echo $admin->email ?>" placeholder="Email" class="form_input" />
                <input type="password" name="password" value="" placeholder="Password" class="form_input"/>
                <input type="password" name="password2" value="" placeholder="Confirm Password" class="form_input" />
                <input type="hidden" name="id" value="<?php echo $admin->id ?>" />
                <input type="submit" name="save_setting" class="button button-big button-fill" id="submit_setting_btn" value="SAVE" />
            </form>
            <div>
                <a class="button button-big button-fill back link" href="<?php echo make_load_url('home') ?>">CANCEL</a>
            </div>
        </div>
    </div>
</div>