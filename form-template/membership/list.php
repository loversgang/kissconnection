<div class="row-fluid">
    <div class="panel panel-default">
        <div class="panel-heading">
            My Membership
        </div>
        <div class="panel-body">
            <?php display_message(1); ?>
            <div class="col-md-6">
                <ul class="list-group">
                    <li class="list-group-item active_c">Change My Username</li>
                    <li class="list-group-item radio_btns">
                        <p>To change your username, please provide your new username, a confirmation of your new username, and, for security, your password.</p>
                        <form action="" method="post" class="form-horizontal">
                            <div class="form-group">
                                <label class="control-label col-md-4" style="padding-left: 0">Current Username</label>
                                <div class="col-md-8">
                                    <input type="text" name="username" value="<?php echo $user->username; ?>" class="form-control" readonly/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-4" style="padding-left: 0">New Username</label>
                                <div class="col-md-8">
                                    <input type="text" name="new_username" value="" class="form-control" required/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-4" style="padding-left: 0">Confirm New Username</label>
                                <div class="col-md-8">
                                    <input type="text" name="confirm_new_username" value="" class="form-control" required/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-4" style="padding-left: 0">Confirm Password</label>
                                <div class="col-md-8">
                                    <input type="password" name="password" value="" class="form-control" required/>
                                </div>
                            </div>
                            <div class="col-md-12" style="padding-right: 0">
                                <input type="submit" name="change_username" class="btn btn-success btn-xs pull-right" value="Save"/>
                            </div>
                        </form>
                    </li>
                </ul>
            </div>
            <div class="col-md-6">
                <ul class="list-group">
                    <li class="list-group-item active_c">Change My Password</li>
                    <li class="list-group-item radio_btns">
                        <p>To change your password, please provide your current password, your new password, and a confirmation of your new password.<br/><span class="body-reg-bld">Passwords are case-sensitive.</span></p>
                        <form action="" method="post" class="form-horizontal">
                            <div class="form-group">
                                <label class="control-label col-md-4" style="padding-left: 0">Current Password</label>
                                <div class="col-md-8">
                                    <input type="password" name="password" value="" class="form-control" required/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-4" style="padding-left: 0">New Password</label>
                                <div class="col-md-8">
                                    <input type="password" name="new_password" value="" class="form-control" required/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-4" style="padding-left: 0">Confirm New Password</label>
                                <div class="col-md-8">
                                    <input type="password" name="confirm_new_password" value="" class="form-control" required/>
                                </div>
                            </div>
                            <div class="col-md-12" style="padding-right: 0">
                                <input type="submit" name="change_password" class="btn btn-success btn-xs pull-right" value="Save"/>
                            </div>
                        </form>
                    </li>
                </ul>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-6">
                <ul class="list-group">
                    <li class="list-group-item active_c">Change My Email Address</li>
                    <li class="list-group-item radio_btns">
                        <p>	
                            The email address provided below will be used by KissConnection.com to communicate with you. Your email address will not be visible to other members.<br/>To change your email address, please provide your new address, a confirmation of your new address, and, for security, your password.</p>
                        <form action="" method="post" class="form-horizontal">
                            <div class="form-group">
                                <label class="control-label col-md-4" style="padding-left: 0">Current Email Address</label>
                                <div class="col-md-8">
                                    <input type="email" name="email" value="<?php echo $user->email; ?>" class="form-control" readonly/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-4" style="padding-left: 0">New Email Address</label>
                                <div class="col-md-8">
                                    <input type="email" name="new_email" value="" class="form-control" required/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-4" style="padding-left: 0">Confirm New Email Address</label>
                                <div class="col-md-8">
                                    <input type="email" name="confirm_new_email" value="" class="form-control" required/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-4" style="padding-left: 0">Confirm Password</label>
                                <div class="col-md-8">
                                    <input type="password" name="password" value="" class="form-control" required/>
                                </div>
                            </div>
                            <div class="col-md-12" style="padding-right: 0">
                                <input type="submit" name="change_email" class="btn btn-success btn-xs pull-right" value="Save"/>
                            </div>
                        </form>
                    </li>
                </ul>
            </div>
            <div class="col-md-6">
                <ul class="list-group">
                    <li class="list-group-item active_c">Auto-login Setting</li>
                    <li class="list-group-item radio_btns">
                        <p>To change your auto-login setting, select the appropriate option below and click "Update":</p>
                        <form method="post" class="from form-horizontal">
                            <div class="radio">
                                <label>
                                    <input type="radio" name="is_auto_login" value="1" <?php echo $user->is_auto_login == 1 ? 'checked' : ''; ?>> Yes
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="is_auto_login" value="0" <?php echo $user->is_auto_login == 0 ? 'checked' : ''; ?>> No
                                </label>
                            </div>
                            <div class="">
                                <input type="submit" name="update_auto_login" value="Update" class="btn btn-success btn-xs"/>
                            </div>
                        </form>
                        <br/>
                        <p class="body-reg-bld">What is auto-login?</p>
                        <p class="body-reg">Auto-login allows you to access your KissConnection.com account without having to provide your username and password each time you visit the site.</p>
                        <p class="body-reg">If you access KissConnection.com from a shared/public computer, you should <strong>not</strong> have auto-login enabled.</p>
                    </li>
                </ul>
            </div>
            
        </div>
    </div>
</div>