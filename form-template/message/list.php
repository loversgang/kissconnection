<div class="row-fluid">
    <div class="panel panel-default">
        <div class="panel-heading">
            <ol class="breadcrumb">
                <li><a href="<?php echo make_admin_url('home') ?>">Home</a></li>
                <li class="active">Inbox</li>
            </ol>
        </div>
        <div class="panel-body">
            <?php display_message(1); ?>
            <div class="col-md-4">
                <?php include 'left.php'; ?>
            </div>
            <div class="col-md-8">
                <ul class="list-group">
                    <li class="list-group-item active_c">Inbox</li>
                    <li class="list-group-item radio_btns">
                        <form method="POST">
                            <table class="table datatable">
                                <thead>
                                    <tr>
                                        <th><input type="checkbox" id="check_one" /></th>
                                        <th>From</th>
                                        <th>Subject</th>
                                        <th>Date</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($inbox_messages as $message) { ?>
                                        <tr>
                                            <td><input type="checkbox" name="d[]" value="<?php echo $message->id ?>" class="check_all" /></td>
                                            <td>
                                                <?php $user_photos = userPhoto :: inbox_user_photo($message->from_id); ?>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <?php if ($user_photos) { ?>
                                                            <img src="<?php echo DIR_WS_SITE . 'assets/files/' . $user_photos->file_name; ?>" class="img img-responsive photo_thumbnail img-thumbnail" />
                                                        <?php } else { ?>
                                                            <img src="<?php echo DIR_WS_SITE_IMAGE . 'default.png'; ?>" class="img img-responsive photo_thumbnail img-thumbnail" />
                                                        <?php } ?>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <a title="View this message" href="<?php echo make_admin_url('message', 'view_inbox', 'view_inbox', 'id=' . $message->id) ?>">
                                                            <?php
                                                            $obj = new user;
                                                            $user = $obj->getUser($message->from_id);
                                                            echo $message->seen == 1 ? $user->username : '<strong>' . $user->username . '</strong>';
                                                            ?>
                                                        </a>
                                                    </div>
                                                    <div class="col-md-6"></div>
                                                </div>

                                            </td>
                                            <td>
                                                <a title="View this message" href="<?php echo make_admin_url('message', 'view_inbox', 'view_inbox', 'id=' . $message->id) ?>">
                                                    <?php echo $message->seen == 1 ? $message->subject : '<strong>' . $message->subject . '</strong>' ?>
                                                </a>
                                            </td>
                                            <td>
                                                <a title="View this message" href="<?php echo make_admin_url('message', 'view_inbox', 'view_inbox', 'id=' . $message->id) ?>">
                                                    <?php
                                                    echo $message->seen ? display_time($message->add_date) : '<strong>' . display_time($message->add_date) . '</strong>'
                                                    ?>
                                                </a>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                            <button type="submit" name="submit" class="btn btn-success btn-sm">Delete Selected</button>
                        </form>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).on('change', '#check_one', function () {
        var check_one_checked = $('#check_one').is(':checked');
        if (check_one_checked) {
            $('.check_all').prop('checked', true);
        } else {
            $('.check_all').prop('checked', false);
        }
    });

    $(document).on('change', '.check_all', function () {
        var check_all_length_checked = $('.check_all:checked').length;
        var check_all_checked = $('.check_all').length;

        if (check_all_length_checked === check_all_checked) {
            $('#check_one').prop('checked', true);
        } else {
            $('#check_one').prop('checked', false);
        }
    });
</script>