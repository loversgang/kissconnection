<div class="row-fluid">
    <div class="panel panel-default">
        <div class="panel-heading">
            <ol class="breadcrumb">
                <li><a href="<?php echo make_admin_url('home') ?>">Home</a></li>
                <li><a href="<?php echo make_admin_url('message') ?>">Messages</a></li>
                <li class="active">New Message</li>
            </ol>
        </div>
        <div class="panel-body">
            <?php display_message(1); ?>
            <div class="col-md-4">
                <?php include 'left.php'; ?>
            </div>
            <div class="col-md-8">
                <?php if (isset($_GET['reply_username'])) { ?>
                    <div class="row">
                        <div class="col-md-6">
                            <?php if ($primary_photo) { ?>
                                <a href="<?php echo make_admin_url('account', 'view', 'view&id=' . $user_info->id) ?>"><img src="<?php echo DIR_WS_SITE . 'assets/files/' . $primary_photo->file_name ?>" class="img img-responsive img-medium img-thumbnail" /></a>
                            <?php } else { ?>
                                <a href="<?php echo make_admin_url('account', 'view', 'view&id=' . $user_info->id) ?>"><img src="<?php echo DIR_WS_SITE_IMAGE . 'default.png' ?>" class="img img-responsive img-medium img-thumbnail" /></a>
                            <?php } ?>
                        </div>
                        <div class="col-md-6">
                            <a href="<?php echo make_admin_url('account', 'view', 'view&id=' . $user_info->id) ?>"><?php echo $user->username; ?></a><br />
                            Gender : <?php echo $user_info->gender ?><br />
                            Age : <?php echo $user_info->age ?><br />
                        </div>
                    </div>
                    <br />
                <?php } ?>
                <ul class="list-group">
                    <li class="list-group-item active_c">Compose Message</li>
                    <li class="list-group-item radio_btns">
                        <form class="form" method="POST" action="">
                            <div class="form-group">
                                <label class="control-label">To <small>(username)</small></label>                           
                                <input type="hidden" class="ajaxurl" value="<?php echo DIR_WS_SITE . 'ajaxuername.php'; ?>">
                                <input type="text" name="to_username" id="search-box" class="form-control" value="<?php echo isset($_GET['reply_username']) ? $_GET['reply_username'] : '' ?>" required />      <div id="suggesstion-box"></div>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Subject</label>
                                <input type="text" name="subject" class="form-control" required value="<?php echo (isset($_GET['id'])) ? $message->subject : '' ?>" />
                            </div>
                            <div class="form-group">
                                <label class="control-label">Message</label>
                                <textarea name="message" rows="8" class="form-control" required ><?php echo (isset($_GET['id'])) ? $message->message : '' ?></textarea>
                            </div>
                            <input type="submit" name="send_message" value="Send Now" class="btn btn-success"/>
                        </form>
                    </li>
                </ul>
                <?php if (isset($_GET['reply_username'])) { ?>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            You and <?php echo $reply_username ?>
                        </div>
                        <div class="panel-body">
                            <?php
                            if (count($me_reply_user_messages) > 0) {
                                foreach ($me_reply_user_messages as $user_messages) {
                                    ?>
                                    <?php if (($user_messages['from_id'] == $user_id && $user_messages['sender_delete'] == 0) || ($user_messages['to_id'] == $user_id && $user_messages['receiver_delete'] == 0)) { ?>
                                        <div class="row">
                                            <div class="col-xs-6"><a href="<?php echo make_admin_url('account', 'view', 'view&id=' . $user_messages['from_id']) ?>">
                                                    <?php
                                                    $user_table = user :: get_messages_using_from_id($user_messages['from_id']);
                                                    if ($user_table->id == $user_id) {
                                                        echo 'You Said';
                                                    } else {
                                                        echo $user_table->gender == 'male' ? 'He Said' : 'You Said';
                                                    }
                                                    ?>
                                                </a>
                                            </div>
                                            <div class="col-xs-6"><?php echo date('d M', $user_messages['add_date']) ?></div><br /><br />
                                            <div class="col-md-12">
                                                <?php echo $user_messages['message'] ?>
                                            </div>
                                        </div>
                                        <br /><hr />
                                        <?php
                                    }
                                }
                            } else {
                                ?>
                                <div class="alert alert-info">No Message Found..!</div>
                                <?php
                            }
                            ?>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
