<ul class="list-group">
    <li class="list-group-item" style="background-color: #337ab7;">
        <a href="<?php echo make_admin_url('message', 'compose', 'compose'); ?>"  style="color: #fff;">New Message</a>
    </li>
    <li class="list-group-item">
        <a href="<?php echo make_admin_url('message'); ?>">Inbox  (<?php echo count($unseen_message_count) ?>)</a>
    </li>
    <li class="list-group-item">
        <a href="<?php echo make_admin_url('message', 'sent', 'sent'); ?>">Sent Messages (<?php echo count($sent_messages) ?>) </a>
    </li>
    <li class="list-group-item">
        <a href="<?php echo make_admin_url('message', 'blocked', 'blocked'); ?>">Manage Blocked Members</a>
    </li>
    <li class="list-group-item">
        <a href="<?php echo make_admin_url('message', 'how', 'how'); ?>">How To Send A Message</a>
    </li>
</ul>