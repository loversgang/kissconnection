<div class="row-fluid">
    <div class="panel panel-default">
        <div class="panel-heading">
            <ol class="breadcrumb">
                <li><a href="<?php echo make_admin_url('home') ?>">Home</a></li>
                <li><a href="<?php echo make_admin_url('message') ?>">Messages</a></li>
                <li class="active">Blocked Members</li>
            </ol>
        </div>
        <div class="panel-body">
            <?php display_message(1); ?>
            <div class="col-md-4">
                <?php include 'left.php'; ?>
            </div>
            <div class="col-md-8">
                <ul class="list-group">
                    <li class="list-group-item active_c">Manage Blocked Members</li>
                    <li class="list-group-item radio_btns">
                        <p>Members you choose to block will not be able to send you messages but will still be able to view your profile.</p>
                        <div class="col-md-6" style="padding-left: 0">
                            <ul class="list-group">
                                <li class="list-group-item active_c">Blocked Members</li>
                                <li class="list-group-item radio_btns">
                                    <?php
                                    if ($blockList->blocked) {
                                        $blockedUsers = explode(',', $blockList->blocked);
                                        ?>
                                        <form method="post" action="" class="form">
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th>Username</th>
                                                        <th style="text-align: right">Remove?</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    foreach ($blockedUsers as $user_id) {
                                                        $obj = new user;
                                                        $user = $obj->getUser($user_id);
                                                        ?>
                                                        <tr>
                                                            <td><?php echo $user->username; ?></td>
                                                            <td style="text-align: right">
                                                                <input type="checkbox" name="unblock[]" value="<?php echo $user->id; ?>"/>
                                                            </td>
                                                        </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                            <input type="submit" name="unblock_users" class="btn btn-danger btn-xs pull-right" value="Unblock"/>
                                        </form>
                                    <?php } else { ?>
                                        <div class="alert alert-danger">You are not currently blocking any members.</div>                
                                    <?php } ?>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-6" style="padding-right:  0">
                            <ul class="list-group">
                                <li class="list-group-item active_c">Add a Block</li>
                                <li class="list-group-item radio_btns">
                                    <form class="form" method="post">
                                        <div class="form-group">
                                            <label class="control-label">Username</label>
                                            <input required type="text" name="username" class="form-control"/>
                                        </div>
                                        <input type="submit" class="btn btn-success btn-sm" name="block_user" value="Add"/>
                                    </form>
                                </li>
                            </ul>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>