<div class="row-fluid">
    <div class="panel panel-default">
        <div class="panel-heading">
            <ol class="breadcrumb">
                <li><a href="<?php echo make_admin_url('home') ?>">Home</a></li>
                <li><a href="<?php echo make_admin_url('message') ?>">Messages</a></li>
                <li class="active">Sent Messages</li>
            </ol>
        </div>
        <div class="panel-body">
            <?php display_message(1); ?>
            <div class="col-md-4">
                <?php include 'left.php'; ?>
            </div>
            <div class="col-md-8">
                <div class="row">
                    <div class="col-xs-6">
                        <?php if ($primary_photo) { ?>
                            <a href="<?php echo make_admin_url('account', 'view', 'view&id=' . $user_info->id) ?>"><img src="<?php echo DIR_WS_SITE . 'assets/files/' . $primary_photo->file_name ?>" class="img img-responsive img-medium img-thumbnail" /></a>
                        <?php } else { ?>
                            <a href="<?php echo make_admin_url('account', 'view', 'view&id=' . $user_info->id) ?>"><img src="<?php echo DIR_WS_SITE_IMAGE . 'default.png' ?>" class="img img-responsive img-medium img-thumbnail" /></a>
                        <?php } ?>
                    </div>
                    <div class="col-xs-6">
                        <a href="<?php echo make_admin_url('account', 'view', 'view&id=' . $user_info->id) ?>"><?php echo $user->username; ?></a><br />
                        Gender : <?php echo $user_info->gender ?><br />
                        Age : <?php echo $user_info->age ?><br />
                    </div>
                </div>
                <br />
                <ul class="list-group">
                    <li class="list-group-item active_c">Message</li>
                    <li class="list-group-item radio_btns">
                        <table class="table">
                            <tbody>
                                <tr>
                                    <th>To:</th>
                                    <td><?php echo $user->username; ?></td>
                                </tr>
                                <tr>
                                    <th>Sent:</th>
                                    <td><?php
                                        echo date('l F d,Y h:i A', $message->add_date);
                                        ?></td>
                                </tr>
                                <tr>
                                    <th>Subject:</th>
                                    <td><?php echo $message->subject; ?></td>
                                </tr>
                                <tr>
                                    <th>Message:</th>
                                    <td><?php echo $message->message; ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>