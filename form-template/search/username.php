<div class="row-fluid">
    <div class="panel panel-default">
        <div class="panel-heading">
            <ol class="breadcrumb">
                <li><a href="<?php echo make_admin_url('home') ?>">Home</a></li>
                <li class="active">Username Search</li>
            </ol>
        </div>
        <div class="panel-body">
            <?php display_message(1); ?>
            <div class="col-md-8">
                <?php if (isset($_POST['username_search'])) { ?>
                    <?php if ($user) { ?>
                        <table class="table-bordered">
                            <tbody>
                                <?php
                                $obj = new userMatch;
                                $match = $obj->getUserMatch($user->id);
                                $obj = new userPhoto;
                                $photo = $obj->getLastUploadedPhoto($user->id);
                                $img_url = $photo ? '<img src="' . DIR_WS_SITE_FILE . $photo->file_name . '" class="img img-responsive" style="width: 100%;height: 100px"/>' : '<img src="' . DIR_WS_SITE_IMAGE . 'noimage.jpg" class="img img-responsive" style="width: 100%;height: 100px"/>';
                                ?>
                                <tr>
                                    <td valign="top" style="width: 25%">
                                        <div style="padding: 10px">
                                            <a href="<?php echo make_admin_url('account', 'view', 'view','id=').$user->id?>"><?php echo $img_url; ?></a>
                                        </div>
                                    </td>
                                    <td valign="top">
                                        <div class="tbl_username">
                                            <?php echo $user->username; ?>
                                        </div>
                                        <div class="user_info">
                                            <b>Age: </b> <?php echo $user->age; ?><br/>
                                            <b>Location: </b> <?php echo getLocation($user->zip); ?><br/>
                                            <i><?php echo ucfirst($result->gender); ?> seeking a <?php echo ucfirst($match->gender); ?>, aged <?php echo $match->from_age; ?> to <?php echo $match->to_age; ?>, living within <?php echo $match->distance; ?> miles of his location.</i>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    <?php } else { ?>
                        <div class="alert alert-danger">
                            Your search did not return any matches. Please try again!
                        </div>
                    <?php } ?>
                <?php } else { ?>
                    <img class="img" style="width: 100%;height: 300px;" src="<?php echo DIR_WS_SITE_IMAGE ?>splash-screen.png"/>
                <?php } ?>
            </div>
            <div class="col-md-4">
                <form method="post" action="<?php echo make_admin_url('search', 'username', 'username') ?>">
                    <div class="username_search">
                        <p>
                            If you know exactly who you are looking for, enter their username below and press submit.
                        </p>
                        <div class="form-group">
                            <label class="control-label">Username</label>
                            <input name="username" class="form-control" type="text" required />
                        </div>
                        <input name="username_search" class="btn btn-success" type="submit" value="Submit">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>