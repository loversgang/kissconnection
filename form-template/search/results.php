<div class="row-fluid">
    <div class="panel panel-default">
        <div class="panel-heading">
            <ol class="breadcrumb">
                <li><a href="<?php echo make_admin_url('home') ?>">Home</a></li>
                <li class="active">Search Results</li>
            </ol>
        </div>
        <div class="panel-body">
            <?php display_message(1); ?>
            <div class="col-md-8">
                <?php if (isset($_POST['full_search'])) { ?>
                    <?php if ($results) { ?>
                        <table class="table-bordered">
                            <tbody>
                                <?php
                                foreach ($results as $result) {
                                    $obj = new userMatch;
                                    $match = $obj->getUserMatch($result->id);
                                    $obj = new userPhoto;
                                    $photo = $obj->getLastUploadedPhoto($result->id);
                                    $img_url = $photo ? '<img src="' . DIR_WS_SITE_FILE . $photo->file_name . '" class="img img-responsive" style="width: 100%;height: 100px"/>' : '<img src="' . DIR_WS_SITE_IMAGE . 'noimage.jpg" class="img img-responsive" style="width: 100%;height: 100px"/>';
                                    ?>
                                    <tr>
                                        <td valign="top" style="width: 25%">
                                            <div style="padding: 10px">
                                                <?php echo $img_url; ?>
                                            </div>
                                        </td>
                                        <td valign="top">
                                            <div class="tbl_username">
                                                <a href="<?php echo make_admin_url('account', 'view', 'view', 'id=' . $result->id) ?>">
                                                    <?php echo $result->username; ?>
                                                </a>
                                                 <div class="profilematchp"> Is a <?php echo round($profilematch[$result->user_id]['percentage']); ?>% Match</div>
                                            </div>
                                            <div class="user_info">
                                                <b>Age: </b> <?php echo $result->age; ?><br/>
                                                <b>Location: </b> <?php echo getLocation($result->zip); ?><br/>
                                                <i><?php echo ucfirst($result->gender); ?> seeking a <?php echo ucfirst($match->gender); ?>, aged <?php echo $match->from_age; ?> to <?php echo $match->to_age; ?>, living within <?php echo $match->distance; ?> miles of his location.</i>
                                            </div>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    <?php } else { ?>
                        <div class="alert alert-danger">
                            Your search did not return any matches. Please try again!
                        </div>
                    <?php } ?>
                <?php } else { ?>
                    <img class="img" style="width: 100%;height: 300px;" src="<?php echo DIR_WS_SITE_IMAGE ?>splash-screen.png"/>
                <?php } ?>
            </div>
            <div class="col-md-4">
                <form method="post" action="<?php echo make_admin_url('search', 'quick', 'quick') ?>">
                    <div class="quick_search">
                        <div class="col-md-12">
                            <span class="col-md-4">I am a</span>
                            <select class="col-md-5 s_input">
                                <option value="male">Male</option>
                                <option value="female">Female</option>        
                            </select>
                        </div>
                        <div class="col-md-12">
                            <span class="col-md-4">Seeking a</span>
                            <select name="gender" class="col-md-5 s_input">
                                <option value="female">Female</option>
                                <option value="male">Male</option>        
                            </select>
                        </div>
                        <div class="col-md-12">
                            <span class="col-md-4">Between</span>
                            <select name="from_age" class="col-md-2 s_input">
                                <?php for ($i = 18; $i <= 120; $i++) { ?>
                                    <option value="<?php echo $i; ?>"><?php echo $i; ?></option> 
                                <?php } ?>
                            </select>
                        </div>
                        <div class="col-md-12">
                            <span class="col-md-4">And</span>
                            <select name="to_age" class="col-md-2 s_input">
                                <?php for ($i = 18; $i <= 120; $i++) { ?>
                                    <option value="<?php echo $i; ?>" <?php echo $i == 35 ? 'selected' : ''; ?>><?php echo $i; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="col-md-12">
                            <span class="col-md-4">Located Within</span>
                            <select name="distance" class="col-md-7s_input">
                                <option value="0">Any</option>
                                <option value="5">5</option>
                                <option value="10">10</option>
                                <option value="25" selected >25</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                                <option value="300">300</option>
                                <option value="500">500</option>
                                <option value="1000">1000</option>
                            </select>
                        </div>
                        <div class="col-md-12">
                            <span class="col-md-4">Zip Code</span>
                            <input name="zip" class="col-md-5 s_input" type="number" required />
                        </div>
                        <div class="col-md-12">
                            <span class="col-md-4">Photos Only</span>
                            <input name="photos_only" value="1" class="col-md-2 s_input" type="checkbox"/>
                        </div>
                        <div class="col-md-12">
                            <span class="col-md-4 hidden-xs">&nbsp;</span>
                            <input name="quick_search" class="col-md-3 btn btn-success" type="submit" value="Submit">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>