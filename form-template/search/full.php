<div class="row-fluid">
    <div class="panel panel-default">
        <div class="panel-heading">
            <ol class="breadcrumb">
                <li><a href="<?php echo make_admin_url('home') ?>">Home</a></li>
                <li class="active">Full Search</li>
            </ol>
        </div>
        <div class="panel-body">
            <?php display_message(1); ?>
            <?php if ($searches) { ?>
                <ul class="list-group">
                    <li class="list-group-item active_c">Saved Searches <span class="pull-right"><a href="<?php echo make_admin_url('search', 'saved', 'saved') ?>">Manage Saved Searches</a></span></li>
                    <li class="list-group-item radio_btns">
                        <div class="col-md-3"></div>
                        <div class="col-md-6">
                            <form method="post" class="form-inline" action="<?php echo make_admin_url('search', 'full', 'full') ?>">
                                <div class="form-group">
                                    <select class="form-control" name="saved_search">
                                        <?php foreach ($searches as $search) { ?>
                                            <option value="<?php echo $search->id ?>"><?php echo $search->title ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <input type="submit" class="btn btn-success" name="get_saved_search" value="Submit Saved Search"/>
                            </form>
                        </div>
                        <div class="col-md-3"></div>
                    </li>
                </ul>
            <?php } ?>
            <form method="post" action="<?php echo make_admin_url('search', 'results', 'results') ?>">
                <ul class="list-group">
                    <li class="list-group-item active_c">General Match Criteria</li>
                    <li class="list-group-item radio_btns">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label class="control-label">I am a</label>
                                <select class="form-control">
                                    <option value="male">Male</option>
                                    <option value="female">Female</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label class="control-label">Seeking a</label>
                                <select class="form-control" name="gender">
                                    <option value="male">Male</option>
                                    <option value="female" <?php echo is_array($preValues['gender']) ? in_array('female', $preValues['gender']) ? 'selected' : '' : '' ?>>Female</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <label class="control-label">Aged</label>
                            <div class="clearfix"></div>
                            <div class="col-md-6" style="padding-left: 0">
                                <div class="form-group">
                                    <select name="from_age" class="form-control">
                                        <?php for ($from_age = 18; $from_age <= 120; $from_age++) { ?>
                                            <option value="<?php echo $from_age; ?>" <?php echo is_array($preValues['from_age']) ? in_array($from_age, $preValues['from_age']) ? 'selected' : '' : '' ?>><?php echo $from_age; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6" style="padding-left: 0">
                                <div class="form-group">
                                    <select name="to_age" class="form-control">
                                        <?php for ($to_age = 18; $to_age <= 120; $to_age++) { ?>
                                            <option value="<?php echo $to_age; ?>" <?php echo is_array($preValues['to_age']) ? in_array($to_age, $preValues['to_age']) ? 'selected' : '' : $to_age == 35 ? 'selected' : '' ?>><?php echo $to_age; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3" style="padding: 0px">
                            <div class="form-group">
                                <label class="control-label">Whose height is between</label>
                                <div class="clearfix"></div>
                                <div class="col-md-6" style="padding-left: 0">
                                    <div class="input-group">
                                        <select name="foot_from" class="form-control" style="padding: 6px 6px;">
                                            <?php for ($foot = 3; $foot <= 8; $foot++) { ?>
                                                <option value="<?php echo $foot; ?>" <?php echo is_array($preValues['foot_from']) ? in_array($foot, $preValues['foot_from']) ? 'selected' : '' : $foot == 5 ? 'selected' : '' ?>><?php echo $foot; ?></option>
                                            <?php } ?>
                                        </select>
                                        <div class="input-group-addon">ft.</div>
                                    </div>
                                </div>
                                <div class="col-md-6" style="padding-left: 0">
                                    <div class="input-group">
                                        <select name="inch_from" class="form-control" style="padding: 6px 6px;">
                                            <?php for ($inch = 0; $inch <= 11; $inch++) { ?>
                                                <option value="<?php echo $inch; ?>" <?php echo is_array($preValues['inch_from']) ? in_array($inch, $preValues['inch_from']) ? 'selected' : '' : $inch == 1 ? 'selected' : '' ?>><?php echo $inch; ?></option>
                                            <?php } ?>
                                        </select>
                                        <div class="input-group-addon">in.</div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label">and</label>
                                <div class="clearfix"></div>
                                <div class="col-md-6" style="padding-left: 0">
                                    <div class="input-group">
                                        <select name="foot_to" class="form-control" style="padding: 6px 6px;">
                                            <?php for ($foot = 3; $foot <= 8; $foot++) { ?>
                                                <option value="<?php echo $foot; ?>" <?php echo is_array($preValues['foot_to']) ? in_array($foot, $preValues['foot_to']) ? 'selected' : '' : $foot == 5 ? 'selected' : '' ?>><?php echo $foot; ?></option>
                                            <?php } ?>
                                        </select>
                                        <div class="input-group-addon">ft.</div>
                                    </div>
                                </div>
                                <div class="col-md-6" style="padding-left: 0">
                                    <div class="input-group">
                                        <select name="inch_to" class="form-control" style="padding: 6px 6px;">
                                            <?php for ($inch = 0; $inch <= 11; $inch++) { ?>
                                                <option value="<?php echo $inch; ?>" <?php echo is_array($preValues['inch_to']) ? in_array($inch, $preValues['inch_to']) ? 'selected' : '' : $inch == 11 ? 'selected' : '' ?>><?php echo $inch; ?></option>
                                            <?php } ?>
                                        </select>
                                        <div class="input-group-addon">in.</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label class="control-label">And lives within</label>
                                <select name="distance" class="form-control">
                                    <option value="0" <?php echo is_array($preValues['distance']) ? in_array('0', $preValues['distance']) ? 'selected' : '' : '' ?>>Any</option>
                                    <option value="5" <?php echo is_array($preValues['distance']) ? in_array('5', $preValues['distance']) ? 'selected' : '' : '' ?>>5 Miles</option>
                                    <option value="10" <?php echo is_array($preValues['distance']) ? in_array('10', $preValues['distance']) ? 'selected' : '' : '' ?>>10 Miles</option>
                                    <option value="25" <?php echo is_array($preValues['distance']) ? in_array('25', $preValues['distance']) ? 'selected' : '' : '' ?>>25 Miles</option>
                                    <option value="50" <?php echo is_array($preValues['distance']) ? in_array('50', $preValues['distance']) ? 'selected' : '' : '' ?>>50 Miles</option>
                                    <option value="100" <?php echo is_array($preValues['distance']) ? in_array('100', $preValues['distance']) ? 'selected' : '' : '' ?>>100 Miles</option>
                                    <option value="300" <?php echo is_array($preValues['distance']) ? in_array('300', $preValues['distance']) ? 'selected' : '' : '' ?>>300 Miles</option>
                                    <option value="500" <?php echo is_array($preValues['distance']) ? in_array('500', $preValues['distance']) ? 'selected' : '' : '' ?>>500 Miles</option>
                                    <option value="1000" <?php echo is_array($preValues['distance']) ? in_array('1000', $preValues['distance']) ? 'selected' : '' : '' ?>>1000 Miles</option>
                                </select>
                            </div>
                        </div>
                    </li>
                </ul>
                <?php
                foreach ($metaDataMatch as $meta) {
                    $obj = new metaOptions;
                    $values = $obj->getMetaOptionsByMetaId($meta->id);
                    ?>
                    <ul class="list-group">
                        <li class="list-group-item active_c"><?php echo $meta->title; ?></li>
                        <li class="list-group-item radio_btns">
                            <?php
                            foreach ($values as $value) {
                                ?>
                                <div class="col-md-4">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="user[<?php echo $meta->id; ?>][]" value="<?php echo $value->value; ?>" <?php echo is_array($preValues[$meta->id]) ? in_array($value->value, $preValues[$meta->id]) ? 'checked' : '' : '' ?>> <?php echo $value->value; ?>
                                        </label>
                                    </div>
                                </div>
                            <?php } ?>
                        </li>
                    </ul>
                <?php } ?>
                <ul class="list-group">
                    <li class="list-group-item active_c">Additional Match Criteria</li>
                    <li class="list-group-item radio_btns">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="control-label">Last Activity</label>
                                <select class="form-control" name="activity">
                                    <option value="0" <?php echo is_array($preValues['activity']) ? in_array('0', $preValues['activity']) ? 'selected' : '' : '' ?>>anytime</option>
                                    <option value="d2" <?php echo is_array($preValues['activity']) ? in_array('d2', $preValues['activity']) ? 'selected' : '' : '' ?>>within 2 days</option>
                                    <option value="w1" <?php echo is_array($preValues['activity']) ? in_array('w1', $preValues['activity']) ? 'selected' : '' : '' ?>>within 1 week</option>
                                    <option value="w2" <?php echo is_array($preValues['activity']) ? in_array('w2', $preValues['activity']) ? 'selected' : '' : '' ?>>within 2 weeks</option>
                                    <option value="m1" <?php echo is_array($preValues['activity']) ? in_array('m1', $preValues['activity']) ? 'selected' : '' : '' ?>>within 1 month</option>
                                    <option value="m2" <?php echo is_array($preValues['activity']) ? in_array('m2', $preValues['activity']) ? 'selected' : '' : '' ?>>within 2 months</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="control-label">Photos only?</label>
                                <div class="clearfix"></div>
                                <label class="radio-inline">
                                    <input type="radio" name="photos_only" value="yes" checked /> Yes
                                </label>
                                <label class="radio-inline">
                                    <input type="radio" name="photos_only" value="no" <?php echo is_array($preValues['photos_only']) ? in_array('no', $preValues['photos_only']) ? 'checked' : '' : '' ?>> No
                                </label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Save this search as</label>
                                <input type="text" name="save_search" class="form-control" />
                            </div>
                        </div>
                    </li>
                </ul>
                <div class="col-lg-12" style="text-align: center">
                    <input type="hidden" name="zip" value="<?php echo $logged_user->zip; ?>"/>
                    <input type="submit" id='full_search_btn' name="full_search" class="btn btn-success" value="Submit"/>
                </div>
            </form>
        </div>
    </div>
</div>

<?php  if(isset($_GET['sid'])):?>
<script>
    $( document ).ready(function() {
    $('#full_search_btn').trigger('click');
});
    
 </script>   
<?php endif; ?> 
