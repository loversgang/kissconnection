<div class="row-fluid">
    <div class="panel panel-default">
        <div class="panel-heading">
            <ol class="breadcrumb">
                <li><a href="<?php echo make_admin_url('home') ?>">Home</a></li>
                <li class="active">My Videos</li>
            </ol>
        </div>
        <div class="panel-body">
            <?php display_message(1); ?>
        </div>
    </div>
</div>