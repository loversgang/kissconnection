<div class="row-fluid">
    <div class="panel panel-default">
        <div class="panel-heading">
            <ol class="breadcrumb">
                <li><a href="<?php echo make_admin_url('home') ?>">Home</a></li>
                <li class="active">Manage Saved Searches</li>
            </ol>
        </div>
        <div class="panel-body">
            <?php display_message(1); ?>
            <div class="col-md-8">
                <p class="body-reg">To remove a search, select the corresponding checkbox in the "Remove?" column and press "Save".</p>
                <p class="body-reg">To add a search, visit the <a href="<?php echo make_admin_url('search', 'full', 'full') ?>">Full Search</a> page and provide a descriptive search name in the "Save this search as" form field before submitting your new search.</p>
                <ul class="list-group">
                    <li class="list-group-item active_c">Currently Saved Searches</li>
                    <li class="list-group-item radio_btns">
                        <?php
                        if ($searches) {
                            ?>
                            <form method="post" action="" class="form">
                                <table class="table" >
                                    <thead>
                                        <tr>
                                            <th>Search</th>
                                            <th style="text-align: center">Use this Save Search</th>
                                            <th style="text-align: right">Remove?</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($searches as $search) { ?>
                                            <tr>
                                                <td><?php echo $search->title; ?></td>
                                                <td style="text-align: center">
                                                    <a href="<?php echo make_admin_url('search', 'full', 'full').'&sid='.$search->id; ?>">User Search</a>
                                                </td>
                                                <td style="text-align: right">
                                                    <input type="checkbox" name="delete[]" value="<?php echo $search->id; ?>"/>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                                <input type="submit" name="delete_searches" class="btn btn-default btn-xs pull-right" value="Save"/>
                            </form>
                        <?php } else { ?>
                            <div class="alert alert-danger">You do not currently have any saved searches.</div>                
                        <?php } ?>
                    </li>
                </ul>
            </div>
            <div class="col-md-4">
                <form method="post" action="<?php echo make_admin_url('search', 'quick', 'quick') ?>">
                    <div class="quick_search">
                        <div class="col-md-12">
                            <span class="col-md-4">I am a</span>
                            <select class="col-md-5 s_input">
                                <option value="male">Male</option>
                                <option value="female">Female</option>        
                            </select>
                        </div>
                        <div class="col-md-12">
                            <span class="col-md-4">Seeking a</span>
                            <select name="gender" class="col-md-5 s_input">
                                <option value="female">Female</option>
                                <option value="male">Male</option>        
                            </select>
                        </div>
                        <div class="col-md-12">
                            <span class="col-md-4">Between</span>
                            <select name="from_age" class="col-md-2 s_input">
                                <?php for ($i = 18; $i <= 120; $i++) { ?>
                                    <option value="<?php echo $i; ?>"><?php echo $i; ?></option> 
                                <?php } ?>
                            </select>
                        </div>
                        <div class="col-md-12">
                            <span class="col-md-4">And</span>
                            <select name="to_age" class="col-md-2 s_input">
                                <?php for ($i = 18; $i <= 120; $i++) { ?>
                                    <option value="<?php echo $i; ?>" <?php echo $i == 35 ? 'selected' : ''; ?>><?php echo $i; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="col-md-12">
                            <span class="col-md-4">Located Within</span>
                            <select name="distance" class="col-md-7s_input">
                                <option value="0">Any</option>
                                <option value="5">5</option>
                                <option value="10">10</option>
                                <option value="25" selected >25</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                                <option value="300">300</option>
                                <option value="500">500</option>
                                <option value="1000">1000</option>
                            </select>
                        </div>
                        <div class="col-md-12">
                            <span class="col-md-4">Zip Code</span>
                            <input name="zip" class="col-md-5 s_input" type="number" required />
                        </div>
                        <div class="col-md-12">
                            <span class="col-md-4">Photos Only</span>
                            <input name="photos_only" value="1" class="col-md-2 s_input" type="checkbox"/>
                        </div>
                        <div class="col-md-12">
                            <span class="col-md-4 hidden-xs">&nbsp;</span>
                            <input name="quick_search" class="col-md-3 btn btn-success" type="submit" value="Submit">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>