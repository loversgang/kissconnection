<?php
require_once("include/config/config.php");
require_once("include/functionClass/metaDataClass.php");
require_once("include/functionClass/userMediaClass.php");
include 'tmp/header.php';
// Get All Users
$obj = new user;
$users = $obj->listUsers();
foreach ($users as $user) {
    if ($user->is_cupid == 1) {
        $obj = new userMatch;
        $matches = $obj->getUserMatch($user->id);
        $obj = new user;
        $results = $obj->getMatchResultsLatest($matches);
        if ($results) {
            ob_start();
            ?>
            <table class="table-bordered">
                <tbody>
                    <?php
                    foreach ($results as $result) {
                        $obj = new userMatch;
                        $match = $obj->getUserMatch($result->id);
                        $obj = new userPhoto;
                        $photo = $obj->getLastUploadedPhoto($result->id);
                        $img_url = $photo ? '<img src="' . DIR_WS_SITE_FILE . $photo->file_name . '" class="img img-responsive" style="width: 100%;height: 100px"/>' : '<img src="' . DIR_WS_SITE_IMAGE . 'noimage.jpg" class="img img-responsive" style="width: 100%;height: 100px"/>';
                        ?>
                        <tr>
                            <td valign="top" style="width: 25%">
                                <div style="padding: 10px">
                                    <?php echo $img_url; ?>
                                </div>
                            </td>
                            <td valign="top">
                                <div class="tbl_username">
                                    <a href="<?php echo make_admin_url('account', 'view', 'view', 'id=' . $result->id) ?>">
                                        <?php echo $result->username; ?>
                                    </a>
                                </div>
                                <div class="user_info">
                                    <b>Age: </b> <?php echo $result->age; ?><br/>
                                    <b>Location: </b> <?php echo getLocation($result->zip); ?><br/>
                                    <i><?php echo ucfirst($result->gender); ?> seeking a <?php echo ucfirst($match->gender); ?>, aged <?php echo $match->from_age; ?> to <?php echo $match->to_age; ?>, living within <?php echo $match->distance; ?> miles of his location.</i>
                                </div>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
            <?php
            $message = ob_get_clean();
            SendEmail('Latest Matches', $user->email, 'admin@kissconnection.com', 'KissConnection.Com', $message);
        }
    }
}
?>
<div class="alert alert-danger" style="text-align: center;font-size: 18px">
    Cron Job Done!<br/>
    <a href="<?php echo make_admin_url('home') ?>">Go Back</a>
</div>
<?php
include 'tmp/footer.php';
