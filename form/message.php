<?php

/* sections */
switch ($section):
    case 'list':
        include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/list.php');
        break;
    case 'update':
        include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/edit.php');
        break;
    case 'insert':
        include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/create.php');
        break;
    case 'how':
        include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/how.php');
        break;
    case 'compose':
        include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/compose.php');
        break;
    case 'sent':
        include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/sent.php');
        break;
    case 'blocked':
        include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/blocked.php');
        break;
    case 'view_inbox':
        include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/view_inbox.php');
        break;
    case 'view_sent':
        include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/view_sent.php');
        break;
    default:break;
endswitch;
?>