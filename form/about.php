<?php

/* sections */
switch ($section):
    case 'list':
        include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/list.php');
        break;
    case 'photo':
        include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/photo.php');
        break;
    case 'kissconnection':
        include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/kissconnection.php');
        break;
    case 'help':
        include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/help.php');
        break;
    case 'options':
        include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/options.php');
        break;
    case 'tips':
        include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/tips.php');
        break;
    case 'anonymity':
        include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/anonymity.php');
        break;
    case 'cupid':
        include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/cupid.php');
        break;
    default:break;
endswitch;
?>