<?php

/* sections */
switch ($section):
    case 'list':
        include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/list.php');
        break;
    case 'update':
        include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/edit.php');
        break;
    case 'insert':
        include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/create.php');
        break;
    case 'response':
        include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/response.php');
        break;
    case 'options':
        include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/options.php');
        break;
    case 'tips':
        include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/tips.php');
        break;
    case 'view':
        include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/view.php');
    break;
    case 'profile':
        include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/profile.php');
        break;
	case 'ajaxuername':
	 include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/ajaxuername.php');
        break;
    default:break;
endswitch;
?>
