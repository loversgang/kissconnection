$(document).ready(function () {
    window.base_url = $('#site_url').attr('href');
    window.ajax_url = base_url + 'tmp/ajax.php';
    // Plugins Initialization
    $('.datatable').DataTable();
    // Registration
    $(document).on('click', '.signup_user', function () {
        toastr.remove();
        var el = $(this);
        var html = el.html();
        var main_div = el.closest('.signup_form');
        var data_array = {};
        data_array.username = main_div.find('#username').val();
        data_array.password = main_div.find('#password').val();
        data_array.password2 = main_div.find('#password2').val();
        data_array.email = main_div.find('#email').val();
        data_array.gender = main_div.find('#gender').val();
        data_array.is_auto_login = main_div.find('input[name="auto_login"]:checked').val();
        data_array.is_cupid = main_div.find('#cupid').val();
        if (data_array.username) {
            if (data_array.password) {
                if (data_array.password === data_array.password2) {
                    if (data_array.email && validateEmail(data_array.email)) {
                        if (confirm("By clicking on 'I Agree' below, you acknowledge that you have read and accept KissConnection.com's Terms and Conditions and Privacy Policy.")) {
                            main_div.find('.disagree').hide();
                            el.html('<i class="fa fa-spinner fa-spin"></i> Signing Up');
                            $.post(ajax_url, {action: 'user_signup', data_array: data_array}, function (data) {
                                el.html(html);
                                main_div.find('.disagree').show();
                                if (data === 'usernameExists') {
                                    toastr.error('Username Already Exists!', 'Error');
                                } else if (data === 'emailExists') {
                                    toastr.error('Email Already Exists!', 'Error');
                                } else {
                                    window.location = base_url + 'control.php?Page=account&action=response&section=response';
                                }
                            });
                        }
                    } else {
                        toastr.error('Please Enter Valid Email Address!', 'Error');
                    }
                } else {
                    toastr.error('Password Not Matched!', 'Error');
                }
            } else {
                toastr.error('Please Enter Password!', 'Error');
            }
        } else {
            toastr.error('Please Enter Username!', 'Error');
        }
    });
    // Sign In
    $(document).on('click', '.signin_user', function () {
        toastr.remove();
        var el = $(this);
        var html = el.html();
        var main_div = el.closest('.signin_form');
        var data_array = {};
        data_array.username = main_div.find('.username').val();
        data_array.password = main_div.find('.password').val();
        if (data_array.username) {
            if (data_array.password) {
                el.html('<i class="fa fa-spinner fa-spin"></i> Logging In');
                $.post(ajax_url, {action: 'user_signin', data_array: data_array}, function (data) {
                    el.html(html);
                    data = $.parseJSON(data);
                    if (data.status === 'success') {
                        window.location = base_url + 'control.php';
                    } else {
                        toastr.error('Something went wrong!', 'Error');
                    }
                });
            } else {
                toastr.error('Please Enter Password!', 'Error');
            }
        } else {
            toastr.error('Please Enter Username!', 'Error');
        }
    });
    $(document).on('change', '.options_type', function () {
        var option = $(this).val();
        if (option === 'radio' || option === 'checkbox' || option === 'select') {
            $('.options_data').html('<div class="form-group"><label class="control-label">Options</label> <div class="input-group"><input type="text" name="options[]" class="form-control" required/><div class="input-group-addon add_more_options" style="cursor:pointer""><i class="fa fa-plus"></i></div></div>');
        } else {
            $('.options_data').html('');
        }
    });
    $(document).on('click', '.add_more_options', function () {
        $(this).html('<i class="fa fa-trash"></i>').removeClass('add_more_options').addClass('remove_more_options');
        $('.add_more_options_data').append('<div class="form-group"><div class="input-group"><input type="text" name="options[]" class="form-control" required/><div class="input-group-addon add_more_options" style="cursor:pointer"><i class="fa fa-plus"></i></div></div>');
    });
    $(document).on('click', '.remove_more_options', function () {
        $(this).closest('.form-group').hide(400, function () {
            $(this).remove();
        });
    });
});
function validateEmail(email) {
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    return emailReg.test(email);
}

//$(window).load(function () {
//    var phones = [{"mask": "###-###-####"}, {"mask": "(###) ###-##############"}];
//    $('#phone').inputmask({
//        mask: phones,
//        greedy: false,
//        definitions: {'#': {validator: "[0-9]", cardinality: 1}}});
//});
$(document).on('keydown', '.username, .password', function (event) {
    if (event.keyCode === 13) {
        $('.signin_user').click();
    }
});

$('input[name="signup_user"]').click(function (e) {
    toastr.remove();
    var inputVal = $('#phone').val();
    var characterReg = /\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/;
    if (inputVal) {
        if (!characterReg.test(inputVal)) {
            e.preventDefault();
            toastr.error('Phone number incorrect')
        }
        if (inputVal.length > 15) {
            e.preventDefault();
            toastr.error('Phone number incorrect')
        }
    }
});

$(document).ready(function () {
    $('.tool').tooltip();
});


$(document).ready(function () {
    $("#search-box").keyup(function () {

        var ajaxurl = $(".ajaxurl").val();

        $.ajax({
            type: "POST",
            url: ajaxurl,
            data: 'keyword=' + $(this).val(),
            beforeSend: function () {
                $("#search-box").css("background", "#FFF url(LoaderIcon.gif) no-repeat 165px");
            },
            success: function (data) {
                $("#search-box").css("background", "none");
                $("#suggesstion-box").show();
                $("#suggesstion-box").html(data);

            }
        });
    });
});
//To select country name
function selectCountry(val) {
    $("#search-box").val(val);
    $("#suggesstion-box").hide();
}
