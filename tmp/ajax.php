<?php

$root = dirname(__DIR__);
require_once($root . "/include/config/config.php");
if (isset($_POST['action'])) {
    extract($_POST);
    if ($action == 'user_signin') {
        $arr = array();
        if ($user = validate_user('user', $data_array)) {
            $user->user_type = 'user';
            $admin_user->set_admin_user_from_object($user);
            update_last_access($user->id, 1);
            $_SESSION['last_activity_recorded'] = time();
            if ($user->is_auto_login == 1) {
                setcookie("kissconnection", $user->username, time() + (365 * 24 * 60 * 60), "/");
            }
            $arr['status'] = 'success';
        } else {
            $arr['status'] = 'error';
        }
        echo json_encode($arr);
    }
    if ($action == 'user_signup') {
        $obj = new user;
        $usernameExists = $obj->checkField('username', $data_array['username']);
        if (!$usernameExists) {
            $obj = new user;
            $emailExists = $obj->checkField('email', $data_array['email']);
            if (!$emailExists) {
                $obj = new user;
                $user_id = $obj->saveUser($data_array);
                $object = new user;
                $user = $object->getUser($user_id);
                SendEmail($Subject, $user->email, 'rocky.developer004@gmail.com', $FromName, $Message);
                $user->user_type = 'user';
                $admin_user->set_admin_user_from_object($user);
                update_last_access($user->id, 1);
                if ($user->is_auto_login == 1) {
                    setcookie("kissconnection", $user->username, time() + (365 * 24 * 60 * 60), "/");
                }
            } else {
                echo 'emailExists';
            }
        } else {
            echo 'usernameExists';
        }
    }
}