<div class="row">
    <div class="col-md-12">
        <div id="navbar1" >
            <ul class="nav navbar-nav nq">
                <li><a href="<?php echo make_admin_url('about', 'kissconnection', 'kissconnection'); ?>">About KissConnection.com</a></li>
                <li><a href="<?php echo make_admin_url('about', 'help', 'help'); ?>">Help/FAQs</a></li>
                <li><a href="<?php echo($admin_user->is_logged_in()) ? make_admin_url('account', 'options', 'options') : 'sitemap.php' ?>">Site Map</a></li>
                <li><a href="<?php echo DIR_WS_SITE; ?>privacy_policy.php">Privacy Policy</a></li>
                <li><a href="<?php echo DIR_WS_SITE; ?>contact.php">Contact Us</a></li>
            </ul>
        </div>
    </div>
    <div class="col-md-12">
        <div class="right"> 
            <p class="ag">©2015 Argent Enterprises Inc. All Right Reserved</p>
        </div>
    </div>
</div>
</div>
<script src="<?php echo DIR_WS_SITE_JS ?>bootstrap.min.js" ></script>
<script src="<?php echo DIR_WS_SITE_PLUGINS ?>datatables/jquery.dataTables.min.js" ></script>
<script src="<?php echo DIR_WS_SITE_PLUGINS ?>datatables/dataTables.bootstrap.min.js" ></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.1.62/jquery.inputmask.bundle.js"></script>

<script src="<?php echo DIR_WS_SITE_JS ?>main.js" ></script>
</body>
</html>