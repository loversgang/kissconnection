<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta charset="utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <title>Kiss Connection</title>
        <link rel="stylesheet" href="<?php echo DIR_WS_SITE_CSS ?>bootstrap.min.css"/>
        <link rel="stylesheet" href="<?php echo DIR_WS_SITE_CSS ?>responsive.css" type="text/css"/>
        <link rel="stylesheet" href="<?php echo DIR_WS_SITE_CSS ?>style.css" type="text/css"/>
        <link rel="stylesheet" href="<?php echo DIR_WS_SITE_PLUGINS ?>toastr/toastr.css" type="text/css"/>
        <link rel="stylesheet" href="<?php echo DIR_WS_SITE_PLUGINS ?>fa-fa/font-awesome.min.css" type="text/css"/>
        <link rel="stylesheet" href="<?php echo DIR_WS_SITE_PLUGINS ?>datatables/jquery.dataTables.min.css" type="text/css"/>
        <link href='https://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css' />
        <script src="<?php echo DIR_WS_SITE_JS ?>jquery.min.js" ></script>
        <script src="<?php echo DIR_WS_SITE_PLUGINS ?>toastr/toastr.js" ></script>

    </head>
    
    <body>   
        <div class="container">
            <?php if ($admin_user->is_logged_in()) { ?>
                <nav class="navbar navbar-default nc">
                    <div class="container-fluid a4">
                        <div class="navbar-header">
                            <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".global-navbar-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a id="site_url" class="navbar-brand" href="<?php echo DIR_WS_SITE; ?>"><img class="img" src="<?php echo DIR_WS_SITE_IMAGE ?>logo_sm.png"/></a>
                        </div>
                        <div class="collapse navbar-collapse global-navbar-collapse">
                            <ul class="nav navbar-nav navbar-right">
                                <li>
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Account <b class="caret"></b></a>
                                    <ul class="dropdown-menu multi-level">
                                        <li><a href="<?php echo make_admin_url('home') ?>">Welcome</a></li>
                                        <li><a href="<?php echo make_admin_url('account', 'options', 'options') ?>">Options Overview</a></li>
                                       <!-- <li><a href="<?php echo make_admin_url('logout') ?>">Logout</a></li>-->
                                    </ul>
                                </li>
                                <li>
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Matches <b class="caret"></b></a>
                                    <ul class="dropdown-menu multi-level">
                                        <li><a href="<?php echo make_admin_url('matches') ?>">View my matches</a></li>
                                        <li><a href="<?php echo make_admin_url('account', 'update', 'update', '&type=match') ?>">Change my match criteria</a></li>
                                        <li><a href="<?php echo make_admin_url('search', 'saved', 'saved') ?>">Manage saved searches</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Messaging  (<b><?php echo count($unseen_message_count) ?></b>)<b class="caret"></b></a>
                                    <ul class="dropdown-menu multi-level">
                                        <li><a href="<?php echo make_admin_url('message'); ?>">Inbox</a></li>
                                        <li><a href="<?php echo make_admin_url('message', 'sent', 'sent'); ?>">Sent Messages</a></li>
                                        <li><a href="<?php echo make_admin_url('message', 'compose', 'compose'); ?>">New Message</a></li>
                                        <li><a href="<?php echo make_admin_url('message', 'blocked', 'blocked'); ?>">Manage blocked members</a></li>
                                        <li><a href="<?php echo make_admin_url('message', 'how', 'how'); ?>">How To Send A Message</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Profile <b class="caret"></b></a>
                                    <ul class="dropdown-menu multi-level">
                                        <li><a href="<?php echo make_admin_url('account') ?>">Change my profile</a></li>
                                        <li><a href="<?php echo make_admin_url('account', 'tips', 'tips'); ?>">Profile tips and examples</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="<?php echo make_admin_url('home') ?>">Search</a>
                                    <ul class="dropdown-menu multi-level">
                                        <li><a href="<?php echo make_admin_url('photo') ?>">Manage my photos</a></li>
                                        <li><a href="<?php echo make_admin_url('video') ?>">Manage my videos</a></li>
                                        <li><a href="<?php echo make_admin_url('about', 'photo', 'photo') ?>">Help with photos</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Cupid <b class="caret"></b></a>
                                    <ul class="dropdown-menu multi-level">
                                        <li><a href="<?php echo make_admin_url('cupid') ?>">Turn Cupid on/off</a></li>
                                        <li><a href="<?php echo make_admin_url('account', 'update', 'update', '&type=match') ?>">Change my matching criteria</a></li>
                                        <li><a href="<?php echo make_admin_url('about', 'cupid', 'cupid') ?>">About Cupid</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Membership <b class="caret"></b></a>
                                    <ul class="dropdown-menu multi-level">
                                        <li><a href="<?php echo make_admin_url('membership') ?>">Change my membership information</a></li>
                                    </ul>
                                </li>
                            </ul>

                        </div>
                    </div>
                    <div class="pull-right" style="margin-right: 5px;">
                        <a href="<?php echo make_admin_url('logout', 'list', 'list') ?>" >Logout</a>
                    </div>
                </nav>
            <?php } else { ?>
                <nav class="navbar navbar-default nc">
                    <div class="container-fluid a4">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
                        <div id="navbar" class="navbar-collapse collapse" aria-expanded="false" style="height: 1px;">
                            <ul class="nav navbar-nav ns">
                                <li><a id="site_url" href="<?php echo DIR_WS_SITE; ?>">HOME</a></li>
                                <li><a href="#">MY ACCOUNT</a></li>
                                <?php if ($admin_user->is_logged_in()) { ?>
                                    <li><a href="<?php echo DIR_WS_SITE_CONTROL; ?>control.php?Page=logout">LOGOUT</a></li>
                                <?php } else { ?>
                                    <li><a href="<?php echo DIR_WS_SITE; ?>register.php">SIGN UP</a></li>
                                <?php } ?>
                                <li><a href="#">FULL SEARCH</a></li>
                                <li><a href="#">QUICK SEARCH</a></li>
                            </ul>
                            <div style="margin-left:152px">
                                <img src="<?php echo DIR_WS_SITE . 'assets/logo/logo.png' ?>" class="img img-responsive" />
                            </div>
                        </div>
                    </div>
                </nav>
            <?php } ?>

