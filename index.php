<?php
require_once("include/config/config.php");

if (!$admin_user->is_logged_in()) {
    if (isset($_GET['user_id'])) {
        $query = new user;
        $user_info = $query->getUser($_GET['user_id']);
        $message_login = '"' . $user_info->message_login . '"';
        $message_login = explode(' ', $user_info->message_login);
        if ($message_login[0] !== '') {
            if (in_array($_GET['message_login'], $message_login)) {
                $_SESSION['admin_session_secure']['user_id'] = $user_info->id;
                $_SESSION['admin_session_secure']['logged_in'] = 1;
                $_SESSION['admin_session_secure']['username'] = $user_info->username;
                $_SESSION['admin_session_secure']['user_type'] = 'user';
                $_SESSION['admin_session_secure']['last_access'] = time();
                if ($user_info->is_auto_login == 1) {
                    setcookie("kissconnection", $user->username, time() + (365 * 24 * 60 * 60), "/");
                }
                Redirect(make_admin_url('message', 'view_inbox', 'view_inbox&id=' . $_GET['message_id']));
            } else {
                Redirect(DIR_WS_SITE);
            }
        } else {
            Redirect(DIR_WS_SITE);
        }
    }
}

if ($admin_user->is_logged_in()) {
    if (isset($_GET['user_id'])) {
        if ($_GET['user_id'] == $_SESSION['admin_session_secure']['user_id']) {
            Redirect(make_admin_url('message', 'view_inbox', 'view_inbox&id=' . $_GET['message_id']));
        } else {
            Redirect(DIR_WS_SITE . 'control.php');
        }
    } else {
        Redirect(DIR_WS_SITE . 'control.php');
    }
}



if (isset($_COOKIE['kissconnection'])) {
    $user_detail = user::getUserByUsername($_COOKIE['kissconnection']);
    if ($user_detail) {
        if ($user = validate_user('user', (array) $user_detail, true)) {
            $user->user_type = 'user';
            $admin_user->set_admin_user_from_object($user);
            update_last_access($user->id, 1);
            Redirect(DIR_WS_SITE_CONTROL . "control.php");
        }
    }
}

$Page = isset($_GET['Page']) ? $_GET['Page'] : "home";
require 'tmp/header.php';
if (count($_SESSION['admin_session_secure']['pass_msg']) > 0) {
    ?>
    <script>
        toastr.error('Login in first');
    </script>
    <?php
    unset($_SESSION['admin_session_secure']['pass_msg']);
}
?>
<div class="row-fluid">
    <div class="col-md-6" style="padding: 0px">
        <img class="hidden-xs" src="<?php echo DIR_WS_SITE_IMAGE ?>img1.jpg" style="height:300px;width: 100%;"/>
        <img class="hidden-md hidden-lg" src="<?php echo DIR_WS_SITE_IMAGE ?>img1.jpg" style="width: 100%;"/>
    </div>
    <div class="col-md-6" style="padding: 0px">
        <div class="col-md-5" style="padding: 0px">
            <h4><b>Quick Search</b></h4>
            <form method="POST" action = "search.php">
                <div class="quick_search">
                    <div class="col-md-12">
                        <span class="col-md-4">I am a</span>
                        <select class="col-md-5 s_input">
                            <option value="male">Male</option>
                            <option value="female">Female</option>        
                        </select>
                    </div>
                    <div class="col-md-12">
                        <span class="col-md-4">Seeking a</span>
                        <select name="gender" class="col-md-5 s_input">
                            <option value="female">Female</option>
                            <option value="male">Male</option>        
                        </select>
                    </div>
                    <div class="col-md-12">
                        <span class="col-md-4">Between</span>
                        <select name="from_age" class="s_input">
                            <?php for ($i = 18; $i <= 120; $i++) { ?>
                                <option value="<?php echo $i; ?>"><?php echo $i; ?></option> 
                            <?php } ?>
                        </select>
                    </div>
                    <div class="col-md-12">
                        <span class="col-md-4">And</span>
                        <select name="to_age" class="s_input">
                            <?php for ($i = 18; $i <= 120; $i++) { ?>
                                <option value="<?php echo $i; ?>" <?php echo $i == 35 ? 'selected' : ''; ?>><?php echo $i; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="col-md-12">
                        <span class="col-md-4">Located Within</span>
                        <select name="distance" class="col-md-7s_input">
                            <option value="0">Any</option>
                            <option value="5">5</option>
                            <option value="10">10</option>
                            <option value="25" selected >25</option>
                            <option value="50">50</option>
                            <option value="100">100</option>
                            <option value="300">300</option>
                            <option value="500">500</option>
                            <option value="1000">1000</option>
                        </select>
                    </div>
                    <div class="col-md-12">
                        <span class="col-md-4">Zip Code</span>
                        <input name="zip" class="col-md-5 s_input" type="number" required />
                    </div>
                    <div class="col-md-12">
                        <span class="col-md-4">Photos Only</span>
                        <input name="photos_only" value="1" class="col-md-2 s_input" type="checkbox"/>
                    </div>
                    <div class="col-md-12">
                        <span class="col-md-4 hidden-xs">&nbsp;</span>
                        <input name="quick_search" class="col-md-3 btn btn-success btn-xs" type="submit" value="Submit">
                    </div>
                </div>
            </form>
        </div>
        <div class="col-md-3" style="padding: 0px">
            <h4><b>Login</b></h4>
            <div class="form signin_form">
                <div class="form-group">
                    <label class="control-label">Username:</label>
                    <input type="text" class="form-control input-sm username" name="user">
                </div>
                <div class="form-group">
                    <label class="control-label">Password:</label>
                    <input type="password" class="form-control input-sm password" name="password">
                </div>
                <button type="button" class="btn btn-success btn-xs signin_user">Login</button>
            </div>
            <img src="<?php echo DIR_WS_SITE_IMAGE ?>img2.jpg"/>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-5 xs-6 sc">
        <p class="a3"><a href="<?php echo DIR_WS_SITE; ?>">KissConnection.com</a> is the premier NO COST
            online dating site. Post a proﬁle, browse through
            ads and email other members all for FREE!

            Why pay $100, $200, $300 per year or more?!

            So don't wait start Today</p>
    </div>

    <div class="col-md-4 xs-6 se">
        <img src="<?php echo DIR_WS_SITE_IMAGE ?>img3.jpg"/>
    </div>

</div>

<?php require 'tmp/footer.php'; ?>