<?php
require_once("include/config/config.php");
require_once("include/functionClass/metaDataClass.php");
require_once("include/functionClass/stateClass.php");
if (isset($_COOKIE['kissconnection'])) {
    $user_detail = user::getUserByUsername($_COOKIE['kissconnection']);
    if ($user_detail) {
        if ($user = validate_user('user', (array) $user_detail, true)) {
            $user->user_type = 'user';
            $admin_user->set_admin_user_from_object($user);
            update_last_access($user->id, 1);
            Redirect(DIR_WS_SITE_CONTROL . "control.php");
        }
    }
}
if ($admin_user->is_logged_in()) {
    $admin_user->set_pass_msg('You are already logged In');
    Redirect(DIR_WS_SITE_CONTROL . "control.php");
}
$Page = isset($_GET['Page']) ? $_GET['Page'] : "home";
require 'tmp/header.php';
if (isset($_POST['signup_user'])) {
    extract($_POST);
    $obj = new user;
    $usernameExists = $obj->checkField('username', $username);
    $obj = new user;
    $emailExists = $obj->checkField('email', $email);
    if (!$usernameExists) {
        if (!$emailExists) {
            if ($password == $password2) {
                // User Information
                $user_array['username'] = $username;
                $user_array['password'] = $password;
                $user_array['email'] = $email;
                $user_array['is_cupid'] = $is_cupid;
                $user_array['is_auto_login'] = $is_auto_login;
                $user_array['dob'] = $birth_month . '-' . $birth_day . '-' . $birth_year;
                $user_array['age'] = calculateAge($user_array['dob']);
                $user_array['gender'] = $gender;
                $user_array['foot'] = $foot;
                $user_array['inch'] = $inch;
                $user_array['zip'] = $zip;
                $result = getLnt($zip);
                $user_array['lat'] = $result['lat'];
                $user_array['lng'] = $result['lng'];
                $obj = new user;
                $user_id = $obj->saveUser($user_array);
                // User Match Preference
                $user_match_array['user_id'] = $user_id;
                $user_match_array['gender'] = $gender_pref;
                $user_match_array['from_age'] = $from_age;
                $user_match_array['to_age'] = $to_age;
                $user_match_array['from_foot'] = $from_foot;
                $user_match_array['from_inch'] = $from_inch;
                $user_match_array['to_foot'] = $to_foot;
                $user_match_array['to_inch'] = $to_inch;
                $user_match_array['distance'] = $distance;
                $object = new userMatch;
                $object->saveUserMatch($user_match_array);

                // User Meta Data
                $user_meta_data['First Name'] = $first_name;
                $user_meta_data['Last Name'] = $last_name;
                $user_meta_data['Address 1'] = $address1;
                $user_meta_data['Address 2'] = $address2;
                $user_meta_data['City'] = $city;
                $user_meta_data['State'] = $state;
                $user_meta_data['Profile ZIP'] = $profile_zip;
                $user_meta_data['Phone'] = $phone;
                $user_meta_data['Profile Email'] = $profile_email;
                foreach ($user_meta_data as $meta_title => $value) {
                    $meta_id = meta::getMetaIdByTitle($meta_title);
                    $arr['user_id'] = $user_id;
                    $arr['key_id'] = $meta_id;
                    $arr['value'] = is_array($value) ? implode(',', $value) : $value;
                    $obj = new metaData;
                    $obj->saveMetaData($arr);
                }
                $user_obj = new user;
                $user = $user_obj->getUser($user_id);
                $Subject = "Kiss Connection Registration";
                $FromName = "KissConnection.Com";
                $Message = "Thanku for Signing Up on kissconnection.com";
                SendEmail($Subject, $user->email, 'rocky.developer004@gmail.com', $FromName, $Message);
                $user->user_type = 'user';
                $admin_user->set_admin_user_from_object($user);
                update_last_access($user->id, 1);
                if ($user->is_auto_login == 1) {
                    setcookie("kissconnection", $user->username, time() + (365 * 24 * 60 * 60), "/");
                }
                Redirect(make_admin_url('account', 'response', 'response'));
            } else {
                $admin_user->set_error();
                $admin_user->set_pass_msg('Password Not Matched!');
            }
        } else {
            $admin_user->set_error();
            $admin_user->set_pass_msg('Email Already Exists!');
        }
    } else {
        $admin_user->set_error();
        $admin_user->set_pass_msg('Username Already Exists!');
    }
}
?>
<div class="row-fluid">
    <div class="panel panel-default">
        <div class="panel-heading">
            <ol class="breadcrumb">
                <li><a href="<?php echo make_admin_url('home') ?>">Home</a></li>
                <li class="active">Membership Sign Up: Step 1 of 2</li>
            </ol>
        </div>
        <div class="panel-body">
            <?php display_message(1); ?>
            <form method="post" class="form">
                <p>
                    To become a KissConnection.com member, fill out the registration form below and click on the "I Agree" button. If you click on the "I Disagree" button or any other links your registration will not be submitted.
                    <br/>
                    If you have any questions regarding the use of the information collected on this page, please read our <a href="<?php echo DIR_WS_SITE; ?>privacy_policy.php">privacy policy</a> before continuing.
                    <br/>
                </p>
                <p class="body-reg-sml">All fields are required except "Address 2".</p>
                <hr/>
                <div class="body-reg-bld">User Information:</div>
                <div class="form-comment">Provide your desired username, a password, password confirmation, your email address, and a headline for your profile. Usernames are NOT case-sensitive, but your password IS case-sensitive. Your headline tells people something about you in just a few words—it should be unique and something that grabs a viewers attention. Don't worry, you will be able to change this information later.</div>
                <div class="clearfix"></div>
                <br/>
                <div class="col-md-3" style="padding-left: 0">
                    <div class="form-group">
                        <label class="control-label">Username</label>
                        <input class="form-control" type="text" name="username" required/>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label class="control-label">Password</label>
                        <input class="form-control" type="password" name="password" required/>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label class="control-label">Confirm Password</label>
                        <input class="form-control" type="password" name="password2" required/>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label class="control-label">Profile Email Address</label>
                        <input class="form-control" type="email" name="email" required/>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-12" style="padding-left: 0">
                    <div class="form-group">
                        <label class="control-label">Headline</label>
                        <input class="form-control" type="text" name="headline" required/>
                    </div>
                </div>
                <div class="clearfix"></div>
                <hr/>
                <div class="form-section">Account Settings:</div>
                <div class="form-comment">Please indicate your preferences for the following settings: "My Cupid" will send you an email once a week showing you the most recent matches to your search criteria; "Auto-login" will allow your computer to log into KissConnection.com without prompting you for your username and password. If you are going to be accessing your KissConnection.com account from a shared or public computer, you should NOT enable the "Auto-login" feature. You will be able to change these settings later.</div>
                <div class="col-md-6" style="padding-left: 0">
                    <div class="radio">
                        <span class="form-label">My Cupid</span> 
                        <label class="radio-inline">
                            <input type="radio" name="is_cupid" value="1"> On
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="is_cupid" value="0" checked /> Off
                        </label>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="radio">
                        <span class="form-label">Auto-login?</span> 
                        <label class="radio-inline">
                            <input type="radio" name="is_auto_login" value="1"> Yes
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="is_auto_login" value="0" checked /> No
                        </label>
                    </div>
                </div>
                <div class="clearfix"></div>
                <hr/>
                <div class="form-label">General Information:</div>
                <div class="form-comment">Provide your birthday, gender, height, and zip code. Your zip code is used to find matches within your area. You will be able to change these settings later.
                </div>
                <div class="clearfix"></div>
                <br/>
                <div class="col-md-3" style="padding: 0">
                    <label class="control-label">Birthday</label>
                    <div class="clearfix"></div>
                    <div class="col-md-4" style="padding-left: 0">
                        <select class="form-control" name="birth_month" style="padding: 6px 10px;">
                            <?php for ($month = 1; $month <= 12; $month++) { ?>
                                <option value="<?php echo $month; ?>"><?php echo $month; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="col-md-4" style="padding-left: 0">
                        <select class="form-control" name="birth_day" style="padding: 6px 10px;">
                            <?php for ($day = 1; $day <= 31; $day++) { ?>
                                <option value="<?php echo $day; ?>" <?php echo $day == 5 ? 'selected' : ''; ?>><?php echo $day; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="col-md-4" style="padding-left: 0">
                        <select class="form-control" name="birth_year" style="padding: 6px 2px;">
                            <?php for ($year = 1900; $year <= date('Y'); $year++) { ?>
                                <option value="<?php echo $year; ?>" <?php echo $year == 1995 ? 'selected' : ''; ?>><?php echo $year; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label class="control-label">Gender</label>
                        <select class="form-control" name="gender">
                            <option value="male">Male</option>
                            <option value="female">Female</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-3">
                    <label class="control-label">Height</label>
                    <div class="clearfix"></div>
                    <div class="col-md-6" style="padding-left: 0">
                        <div class="input-group">
                            <select name="foot" class="form-control" style="padding: 6px 6px;">
                                <?php for ($foot = 3; $foot <= 8; $foot++) { ?>
                                    <option value="<?php echo $foot; ?>" <?php echo $foot == 5 ? 'selected' : ''; ?>><?php echo $foot; ?></option>
                                <?php } ?>
                            </select>
                            <div class="input-group-addon">ft.</div>
                        </div>
                    </div>
                    <div class="col-md-6" style="padding-left: 0">
                        <div class="input-group">
                            <select name="inch" class="form-control" style="padding: 6px 6px;">
                                <?php for ($inch = 0; $inch <= 11; $inch++) { ?>
                                    <option value="<?php echo $inch; ?>" <?php echo $inch == 6 ? 'selected' : ''; ?>><?php echo $inch; ?></option>
                                <?php } ?>
                            </select>
                            <div class="input-group-addon">in.</div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label class="control-label">Profile Zip Code</label>
                        <input type="number" name="zip" class="form-control" required/>
                    </div>
                </div>
                <div class="clearfix"></div>
                <hr/>
                <div class="form-label">General Match Information:</div>
                <div class="form-comment">
                    Provide your match preferences for age, gender, height, and distance from your location. You will be able to change these settings later.
                </div>
                <div class="clearfix"></div>
                <br/>
                <div class="col-md-3" style="padding: 0">
                    <label class="control-label">Age Pref.</label>
                    <div class="clearfix"></div>
                    <div class="col-md-6" style="padding-left: 0">
                        <select name="from_age" class="form-control">
                            <?php for ($from_age = 18; $from_age <= 120; $from_age++) { ?>
                                <option value="<?php echo $from_age; ?>" <?php echo $from_age == 18 ? 'selected' : ''; ?>><?php echo $from_age; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="col-md-6" style="padding-left: 0">
                        <select name="to_age" class="form-control">
                            <?php for ($to_age = 18; $to_age <= 120; $to_age++) { ?>
                                <option value="<?php echo $to_age; ?>" <?php echo $to_age == 35 ? 'selected' : ''; ?>><?php echo $to_age; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="col-md-2" style="padding-left: 0">
                    <div class="form-group">
                        <label class="control-label">Gender Pref.</label>
                        <select name="gender_pref" class="form-control">
                            <option value="male">Male</option>
                            <option value="female">Female</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-5" style="padding-left: 0">
                    <label class="control-label">Height Pref.</label>
                    <div class="clearfix"></div>
                    <div class="col-md-3" style="padding-left: 0">
                        <div class="input-group">
                            <select name="from_foot" class="form-control" style="padding: 6px 6px;">
                                <?php for ($foot = 3; $foot <= 8; $foot++) { ?>
                                    <option value="<?php echo $foot; ?>" <?php echo $foot == 5 ? 'selected' : ''; ?>><?php echo $foot; ?></option>
                                <?php } ?>
                            </select>
                            <div class="input-group-addon" style="padding: 6px">ft.</div>
                        </div>
                    </div>
                    <div class="col-md-3" style="padding-left: 0">
                        <div class="input-group">
                            <select name="from_inch" class="form-control" style="padding: 6px 3px;">
                                <?php for ($inch = 0; $inch <= 11; $inch++) { ?>
                                    <option value="<?php echo $inch; ?>" <?php echo $inch == 1 ? 'selected' : ''; ?>><?php echo $inch; ?></option>
                                <?php } ?>
                            </select>
                            <div class="input-group-addon" style="padding: 6px">in.</div>
                        </div>
                    </div>
                    <div class="col-md-3" style="padding-left: 0">
                        <div class="input-group">
                            <select name="to_foot" class="form-control" style="padding: 6px 6px;">
                                <?php for ($foot = 3; $foot <= 8; $foot++) { ?>
                                    <option value="<?php echo $foot; ?>" <?php echo $foot == 5 ? 'selected' : ''; ?>><?php echo $foot; ?></option>
                                <?php } ?>
                            </select>
                            <div class="input-group-addon" style="padding: 6px">ft.</div>
                        </div>
                    </div>
                    <div class="col-md-3" style="padding: 0">
                        <div class="input-group">
                            <select name="to_inch" class="form-control" style="padding: 6px 3px;">
                                <?php for ($inch = 0; $inch <= 11; $inch++) { ?>
                                    <option value="<?php echo $inch; ?>" <?php echo $inch == 11 ? 'selected' : ''; ?>><?php echo $inch; ?></option>
                                <?php } ?>
                            </select>
                            <div class="input-group-addon" style="padding: 6px">in.</div>
                        </div>
                    </div>
                </div>
                <div class="col-md-2" style="padding-left: 0">
                    <div class="form-group">
                        <label class="control-label">Distance Pref.</label>
                        <div class="input-group">
                            <div class="input-group-addon" style="padding: 6px">Within</div>
                            <select name="distance" class="form-control" style="padding: 6px 0px;">
                                <option value="0">Any</option>
                                <option value="5">5</option>
                                <option value="10">10</option>
                                <option value="25" selected="">25</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                                <option value="300">300</option>
                                <option value="500">500</option>
                                <option value="1000">1000</option>
                            </select>
                            <div class="input-group-addon" style="padding: 6px">miles</div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <hr/>
                <div class="form-label">Personal Information:</div>
                <div class="form-comment">
                    This information is for accounting purposes only and will not be visible to other members.
                </div>
                <div class="clearfix"></div>
                <br/>
                <div class="col-md-6" style="padding-left: 0">
                    <div class="form-group">
                        <label class="control-label">First Name</label>
                        <input type="text" name="first_name" class="form-control" required/>
                    </div>
                </div>
                <div class="col-md-6" style="padding-left: 0">
                    <div class="form-group">
                        <label class="control-label">Last Name</label>
                        <input type="text" name="last_name" class="form-control" required/>
                    </div>
                </div>
                <div class="col-md-6" style="padding-left: 0">
                    <div class="form-group">
                        <label class="control-label">Address 1</label>
                        <input type="text" name="address1" class="form-control" required/>
                    </div>
                </div>
                <div class="col-md-6" style="padding-left: 0">
                    <div class="form-group">
                        <label class="control-label">Address 2</label>
                        <input type="text" name="address2" class="form-control"/>
                    </div>
                </div>
                <div class="col-md-6" style="padding-left: 0">
                    <div class="form-group">
                        <label class="control-label">City</label>
                        <input type="text" name="city" class="form-control" required/>
                    </div>
                </div>
                <div class="col-md-6" style="padding-left: 0">
                    <div class="form-group">
                        <label class="control-label">State</label>
                        <?php
                        $obj = new state;
                        $states = $obj->listStates();
                        ?>
                        <select name="state" class="form-control">
                            <?php foreach ($states as $state) { ?>
                                <option value="<?php echo $state->id; ?>"><?php echo strtoupper($state->name); ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="col-md-4" style="padding-left: 0">
                    <div class="form-group">
                        <label class="control-label">Profile Zip Code</label>
                        <input type="number" name="profile_zip" class="form-control" required/>
                    </div>
                </div>
                <div class="col-md-4" style="padding-left: 0">
                    <div class="form-group">
                        <label class="control-label">Phone</label>
                        <input type="text" name="phone" class="form-control" required id="phone"  />
                    </div>
                </div>
                <div class="col-md-4" style="padding-left: 0">
                    <div class="form-group">
                        <label class="control-label">Email Address</label>
                        <input type="email" name="profile_email" class="form-control" required/>
                    </div>
                </div>
                <div class="clearfix"></div>
                <p align="justify">
                    By clicking on "I Agree" below, you acknowledge that you have read and accept KissConnection.com's <a href="<?php echo DIR_WS_SITE; ?>terms_and_conditions.php" target="_blank">Terms and Conditions</a> and <a href="<?php echo DIR_WS_SITE; ?>privacy_policy.php" target="_blank">Privacy Policy</a>.
                </p>
                <br/>
                <div class="col-md-12" style="text-align: center">
                    <input type="submit" class="btn btn-success" value="I Agree" name="signup_user"/>
                    <a href="<?php echo DIR_WS_SITE; ?>" class="btn btn-danger">I Disagree</a>
                </div>
                <br/>
            </form>
        </div>
    </div>
</div>
<?php require 'tmp/footer.php'; ?>