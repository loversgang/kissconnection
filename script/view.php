<?php

include_once(DIR_FS_SITE . 'include/functionClass/saleClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/viewClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/buyerClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/horseClass.php');
isset($_GET['action']) ? $action = $_GET['action'] : $action = 'list';
isset($_GET['section']) ? $section = $_GET['section'] : $section = 'list';
isset($_GET['id']) ? $id = $_GET['id'] : $id = '0';
isset($_GET['horse_id']) ? $horse_id = $_GET['horse_id'] : $horse_id = '0';
isset($_GET['sale_head_id']) ? $sale_head_id = $_GET['sale_head_id'] : $sale_head_id = '0';
$modName = 'view';
#handle actions here.
switch ($action):
    case'list':
        // List Sale Heads
        $obj = new sale_head;
        $sale_heads = $obj->listSaleHeads();
        break;

    case'insert':
        break;
    case'update':
        // View Counts
        $view_count = view::viewCount($horse_id);

        // Get Buyers Id
        $obj = new view;
        $buyer_ids = $obj->getBuyers($horse_id);
        $buyer_id_array = array();
        foreach ($buyer_ids as $buyer_id) {
            $buyer_id_array[] = $buyer_id->buyer_id;
        }
        $buyers_ids = implode(',', $buyer_id_array);

        // Get Buyers detail
        $obj = new buyer;
        $buyers = $obj->getBuyersDetail($buyers_ids);

        // Get Horse Details
        $obj = new horse;
        $horse = $obj->getHorse($horse_id);
        break;
    case'view':
        $obj = new horse;
        $horses = $obj->listHorses($sale_head_id);

        $obj = new view;
        $views = $obj->getViewsByBuyer($_GET['buyer_id'], $horse_id);

        $obj = new buyer;
        $buyer = $obj->getBuyer($_GET['buyer_id']);
        break;
    case'delete':
        break;
    default:break;
endswitch;
?>