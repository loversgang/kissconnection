<?php

include_once(DIR_FS_SITE . 'include/functionClass/userMediaClass.php');
isset($_GET['action']) ? $action = $_GET['action'] : $action = 'list';
isset($_GET['section']) ? $section = $_GET['section'] : $section = 'list';
isset($_GET['id']) ? $id = $_GET['id'] : $id = '0';
isset($_GET['redirect']) ? $redirect = $_GET['redirect'] : $redirect = '';
$user_id = $_SESSION['admin_session_secure']['user_id'];
$modName = 'video';
switch ($action):
    case'list':
        $obj = new userVideo;
        $videos = $obj->getUserVideo();
        if (isset($_POST['upload_video'])) {
            $rx = '~
    ^(?:https?://)?             
     (?:www\.)?                  
     (?:youtube\.com|youtu\.be)  
     /watch\?v=([^&]+)           
     ~x';
            $youtube_share = 'https://youtu.be/';
            $url = $_POST['video_code'];
            $has_match = preg_match($rx, $url, $matches);
            $url_share = substr($url, 0, 17);
            if ($has_match || $url_share == $youtube_share) {
                if ($has_match) {
                    $_POST['video_code'] = substr($_POST['video_code'], 32);
                } else {
                    $_POST['video_code'] = substr($_POST['video_code'], 17);
                }
                $_POST['video_code'] = htmlentities($_POST['video_code']);
                $_POST['user_id'] = $user_id;
                $_POST['is_primary'] = isset($_POST['is_primary']) ? $_POST['is_primary'] : 0;
                $obj = new userVideo;
                $obj->saveUserVideo($_POST);
                $admin_user->set_pass_msg('Video Uploaded Successfully!');
                Redirect(make_admin_url('video'));
            } else {
                $admin_user->set_error();
                $admin_user->set_pass_msg('Please enter correct youtube URL');
                Redirect(make_admin_url('video'));
            }
        }
        break;

    case'insert':
        $obj = new sale_head;
        $sale_heads = $obj->listSaleHeads();

        $obj = new buyer;
        $buyers = $obj->listBuyers();
        break;
    case'update':
        $obj = new sale_head;
        $sale_heads = $obj->listSaleHeads();

        $obj = new buyer;
        $buyers = $obj->listBuyers();

        $obj = new sale;
        $sale = $obj->getSale($id);
        break;
    case'delete':
        $query = new userVideo;
        $query->delete_video($_GET['vid']);
        $admin_user->set_pass_msg('Video Deleted Successfully!');
        if ($redirect) {
            Redirect(make_admin_url($redirect));
        } else {
            Redirect(make_admin_url('video'));
        }
        break;
    case'change':
        $query = new userVideo;
        $query->remove_primary_photo();

        $query = new userVideo();
        $query->setPrimaryPhoto($_GET['vid'], '1');
        $admin_user->set_pass_msg('Primary Video Change Successfully!');
        if ($redirect) {
            Redirect(make_admin_url($redirect));
        } else {
            Redirect(make_admin_url('video'));
        }
        break;
    default:break;
endswitch;
?>
