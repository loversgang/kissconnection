<?php

include_once(DIR_FS_SITE . 'include/functionClass/blockClass.php');
isset($_GET['action']) ? $action = $_GET['action'] : $action = 'list';
isset($_GET['section']) ? $section = $_GET['section'] : $section = 'list';
isset($_GET['type']) ? $type = $_GET['type'] : $type = 'block';
isset($_GET['id']) ? $id = $_GET['id'] : $id = '0';
$modName = 'block';
$user_id = $_SESSION['admin_session_secure']['user_id'];
switch ($action):
    case'list':
        $query = new block;
        $query->saveBlock($user_id, $_GET['block'], $type);
        if ($type == 'block') {
            $admin_user->set_pass_msg('Successfully Blocked');
        } else {
            $admin_user->set_pass_msg('Successfully UnBlocked');
        }
        Redirect(make_admin_url('account', 'view', 'view&id=' . $_GET['block']));
        break;
    default:break;
endswitch;
