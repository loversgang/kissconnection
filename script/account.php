<?php

include_once(DIR_FS_SITE . 'include/functionClass/metaDataClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/userMediaClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/block.php');
isset($_GET['action']) ? $action = $_GET['action'] : $action = 'list';
isset($_GET['section']) ? $section = $_GET['section'] : $section = 'list';
isset($_GET['type']) ? $type = $_GET['type'] : $type = 'about';
isset($_GET['id']) ? $id = $_GET['id'] : $id = '0';
$modName = 'account';
$user_id = $_SESSION['admin_session_secure']['user_id'];
#handle actions here.
switch ($action):
    case'list':
        if (isset($_POST['profile_visible'])) {
            $query = new user;
            $query->is_visible($user_id, $_POST['is_visible']);
            $admin_user->set_pass_msg('Profile Visibility Changed');
            Redirect(make_admin_url('account'));
        }

        // About Me
        $obj = new meta;
        $metaDataAbout = $obj->listMetaByGroupId(1);

        // Narratives
        $obj = new meta;
        $metaDataNarratives = $obj->listMetaByGroupId(3);

        // About My Personality
        $obj = new meta;
        $metaDataPersonality = $obj->listMetaByGroupId(5);

        //About My Match
        $obj = new meta;
        $metaDataMatch = $obj->listMetaByGroupId(4);

        //Pets
        $obj = new meta;
        $metaDataPets = $obj->listMetaByGroupId(7);

        //Turn Ons / Turn Offs
        $obj = new meta;
        $metaDataTurn = $obj->listMetaByGroupId(8);


        $obj = new userPhoto;
        $photos_count = $obj->getPhotoCount($user_id);

//        if ($photos_count == 1) {
//            $query = new userPhoto;
//            $photo = $query->getLastUploadedPhoto();
//        } else {
//            $object = new user;
//            $user = $object->getUser($user_id);
//            $obj = new userPhoto;
//            $photo = $obj->getProfilePhoto();
//        }

        $object = new user;
        $user = $object->getUser($user_id);
        $obj = new userPhoto;
        $photo = $obj->getProfilePhoto();

        $obj = new userVideo;
        $video = $obj->primary_video();
        if (!$video) {
            $obj = new userVideo;
            $video = $obj->getLastUploadedVideo();
        }

        $obj = new userPhoto;
        $photos = $obj->getUserPhoto($user_id);

        $obj = new userVideo;
        $videos = $obj->getUserVideo($user_id);
        break;

    case'profile':

        if (!isset($_GET['profile'])) {
            Redirect(make_admin_url('search'));
        }

        // About Me
        $obj = new meta;
        $metaDataAbout = $obj->listMetaByGroupId(1);

        // Narratives
        $obj = new meta;
        $metaDataNarratives = $obj->listMetaByGroupId(3);

        // About My Personality
        $obj = new meta;
        $metaDataPersonality = $obj->listMetaByGroupId(5);

        //About My Match
        $obj = new meta;
        $metaDataMatch = $obj->listMetaByGroupId(4);

        //Pets
        $obj = new meta;
        $metaDataPets = $obj->listMetaByGroupId(7);

        //Turn Ons / Turn Offs
        $obj = new meta;
        $metaDataTurn = $obj->listMetaByGroupId(8);

        $object = new user;
        $user = $object->getUser($_GET['profile']);


        $obj = new userPhoto;
        $photo = $obj->getLastUploadedPhoto($_GET['profile']);

        $obj = new userPhoto;
        $photos_count = $obj->getPhotoCount($_GET['profile']);

        $obj = new userVideo;
        $video = $obj->getLastUploadedVideo($_GET['profile']);

        $obj = new userPhoto;
        $photos = $obj->getUserPhoto($_GET['profile']);

        $obj = new userVideo;
        $videos = $obj->getUserVideo($_GET['profile']);

        break;

    case'insert':
        break;
    case'update':
        $obj = new user;
        $myself = $obj->getUser();

        // Birthday
        $dob = explode('-', $myself->dob);
        $birthmonth = $dob[0];
        $birthday = $dob[1];
        $birthyear = $dob[2];
        if ($type == 'narratives') {
            $obj = new meta;
            $metaData = $obj->listMetaByGroupId(3);
        } elseif ($type == 'personality') {
            $obj = new meta;
            $metaData = $obj->listMetaByGroupId(5);


            //Pets
            $obj = new meta;
            $metaDataPets = $obj->listMetaByGroupId(7);

            //Turn Ons / Turn Offs
            $obj = new meta;
            $metaDataTurn = $obj->listMetaByGroupId(8);
        } elseif ($type == 'match') {
            $obj = new meta;
            $metaData = $obj->listMetaByGroupId(4);
        } else {
            $obj = new meta;
            $metaData = $obj->listMetaByGroupId(1);
        }
        $obj = new metaData;
        $userMeta = $obj->checkMetaExists();
        $user_meta_array = array();
        foreach ($userMeta as $meta) {
            $user_meta_array[$meta->key_id] = $meta->value;
        }
        if (isset($_POST['submit'])) {
            extract($_POST);
            $user_array['id'] = $user_id;
            if ($type == 'about') {
                $user_array['dob'] = $birth_month . '-' . $birth_day . '-' . $birth_year;
                $user_array['age'] = calculateAge($user_array['dob']);
                $user_array['gender'] = $gender;
                $user_array['foot'] = $foot;
                $user_array['inch'] = $inch;
                $user_array['zip'] = $zip;
                $result = getLnt($zip);
                $user_array['lat'] = $result['lat'];
                $user_array['lng'] = $result['lng'];
            }
            $obj = new user;
            $obj->saveUser($user_array);
            unset($_POST['birth_month']);
            unset($_POST['birth_day']);
            unset($_POST['birth_year']);
            unset($_POST['age']);
            unset($_POST['gender']);
            unset($_POST['foot']);
            unset($_POST['inch']);
            unset($_POST['zip']);
            unset($_POST['submit']);
            foreach ($_POST as $key_id => $value) {
                if (array_key_exists($key_id, $user_meta_array)) {
                    $arr['id'] = metaData::getMetaDataIdByMetaId($key_id);
                }
                $arr['user_id'] = $_SESSION['admin_session_secure']['user_id'];
                $arr['key_id'] = $key_id;
                $arr['value'] = is_array($value) ? implode('||', $value) : $value;
                $obj = new metaData;
                $obj->saveMetaData($arr);
            }
            $admin_user->set_pass_msg('Profile Updated Successfully!');
            Redirect(make_admin_url('account', 'list', 'list'));
        }
        break;
    case'view':
        $query = new block;
        $current_profile_block_him = $query->is_block($id, $user_id);
        if ($current_profile_block_him) {
            $_SESSION['toastr_message'] = 'Page you trying to open is incorrect or you not have permission to access this page';
            Redirect(make_admin_url('home'));
        }

        //if ($user_id == $id) {
        //    Redirect(make_admin_url('account'));
        //}
        // About Me
        $obj = new meta;
        $metaDataAbout = $obj->listMetaByGroupId(1);

        // Narratives
        $obj = new meta;
        $metaDataNarratives = $obj->listMetaByGroupId(3);


        // About My Personality
        $obj = new meta;
        $metaDataPersonality = $obj->listMetaByGroupId(5);

        //About My Match
        $obj = new meta;
        $metaDataMatch = $obj->listMetaByGroupId(4);

        //Pets
        $obj = new meta;
        $metaDataPets = $obj->listMetaByGroupId(7);

        //Turn Ons / Turn Offs
        $obj = new meta;
        $metaDataTurn = $obj->listMetaByGroupId(8);

        $object = new user;
        $user = $object->getUser($id);

        if (!is_object($user)) {
            $_SESSION['toastr_message'] = 'Page you trying to open is incorrect or you not have permission to access this page';
            Redirect(make_admin_url('home'));
        }

        $obj = new userPhoto;
        $photos_count = $obj->getPhotoCount($id);

//        if ($photos_count == 1) {
//            $query = new userPhoto;
//            $photo = $query->getLastUploadedPhoto($id);
//        } else {
//            $object = new user;
//            $user = $object->getUser($id);
//            $obj = new userPhoto;
//            $photo = $obj->getProfilePhoto($id);
//        }

        $object = new user;
        $user = $object->getUser($id);
        $obj = new userPhoto;
        $photo = $obj->getProfilePhoto($id);

        $obj = new userPhoto;
        $photos = $obj->getUserPhoto($id);

        $obj = new userVideo;
        $video = $obj->primary_video($id);
        if (!$video) {
            $obj = new userVideo;
            $video = $obj->getLastUploadedVideo($id);
        }

        $obj = new userVideo;
        $videos = $obj->getUserVideo($id);

        $query = new block;
        $is_block = $query->is_block($user_id, $id);

        break;
    default:break;
endswitch;
