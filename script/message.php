<?php
include_once(DIR_FS_SITE . 'include/functionClass/userMessageClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/userMediaClass.php');
isset($_GET['action']) ? $action = $_GET['action'] : $action = 'list';
isset($_GET['section']) ? $section = $_GET['section'] : $section = 'list';
isset($_GET['id']) ? $id = $_GET['id'] : $id = '0';
isset($_GET['reply_username']) ? $reply_username = $_GET['reply_username'] : $reply_username = '0';

$modName = 'message';

$user_id = $_SESSION['admin_session_secure']['user_id'];



$query = new user;
$user_detail = $query->getUser($user_id);

$user_photos = get_object_by_col2('user_photos', 'user_id', $user_detail->id);

if ($user_photos) {
    $login_photo = $user_photos->file_name;
//    $login_photo_link = 'http://qtx.in/kissconnection/assets/files/' . $login_photo;
    $login_photo_link = DIR_WS_SITE . 'assets/files/' . $login_photo;
} else {
    $login_photo = 'default.png';
    $login_photo_link = DIR_WS_SITE . 'assets/images/' . $login_photo;
}

$obj = new userMessage;
$inbox_messages = $obj->getInboxMessages();

$obj = new userMessage;
$sent_messages = $obj->getSentMessages();


switch ($action):
    case'list':
        if (isset($_POST['submit'])) {
            if ($_POST['d']) {
                foreach ($_POST['d'] as $message_id) {
                    $query = new userMessage;
                    $query->delete_inbox_message($message_id);
                }
                $admin_user->set_pass_msg('Message deleted successfully');
                Redirect(make_admin_url('message', 'list', 'list'));
            } else {
                $admin_user->set_error();
                $admin_user->set_pass_msg('Select Atleast one');
                Redirect(make_admin_url('message', 'list', 'list'));
            }
        }
        break;
    case'compose':
        if (isset($_GET['reply_username'])) {
            $query = new user;
            $user_info = $query->get_email_using_username($_GET['reply_username']);

            $query = new userPhoto;
            $primary_photo = $query->getProfilePhoto($user_info->id);
        }

        $obj = new userMessage;
        $message = $obj->getMessage($id);
        if (isset($_POST['send_message'])) {
            extract($_POST);
            if ($to_username != $logged_user->username) {
                $user = user::getUserByUsername($to_username);
                $email = $user->email;
                if ($user) {
                    $obj = new userBlockList;
                    $blockList = $obj->getBlockedList($user->id);
                    if ($blockList->blocked) {
                        $blockedIds = explode(',', $blockList->blocked);
                        $query = new user;
                        if (!in_array($logged_user->id, $blockedIds)) {
                            $arr['from_id'] = $logged_user->id;
                            $arr['to_id'] = $user->id;
                            $arr['subject'] = $subject;
                            $arr['message'] = $message;
                            $obj = new userMessage;
                            $message_id = $obj->saveMessage($arr);

                            $query = new user;
                            $message_login = $query->update_message_login($user->id);
                            ?>
                            <!DOCTYPE html>
                            <html lang="en-US">
                                <head>
                                    <meta charset="utf-8">
                                </head>
                                <body>
                                    <div>
                                        <?php
                                        echo $new_message = '<div style="background-color:#EAEAEA;;padding: 20px;"><img src="http://qtx.in/kissconnection/assets/logo/logo.png" height="40" /><span style="float:right;color:grey">From: <span style="margin-top: 10px;margin-right: 22px;"><span style="color:#156397;"><a href="http://qtx.in/kissconnection/control.php?Page=account&action=view&section=view&id=' . $user_detail->id . '">' . $user_detail->username . '</a></span><br />Received ' . date('d/m/Y H:i:s A', time()) . '</span></span><br /><br /><div style="font-size:16px;font-size: 16px; border: 1px solid #337ab7; padding: 10px;background-color: #337ab7;"><img src="' . $login_photo_link . '" height="100" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style="position:absolute;color:#FFFFFF;">' . $user_detail->username . '</span><br /><br /><div style="background-color: #fff; padding: 10px;">' . $message . '<br /><br /><center><a href="' . DIR_WS_SITE . '?user_id=' . $user->id . '&id=' . $user_id . '&message_login=' . $message_login . '&message_id=' . $message_id . '" style="color: #fff; background-color: #1B689C; border-color: #2e6da4;display: inline-block; padding: 6px 12px; margin-bottom: 0; font-size: 14px; font-weight: 400; line-height: 1.42857143; text-align: center; white-space: nowrap; vertical-align: middle;background-image: none;border: 1px solid transparent; border-radius: 4px;text-decoration:none">REPLY NOW</a></center></div></div></div><br /><br />';
                                        ?>
                                        <br/>
                                    </div>
                                </body>
                            </html>
                            <?php
                            send_email_by_cron($email, $new_message, $subject, SITE_EMAIL);
                            $admin_user->set_pass_msg('Message Sent Successfully!');
                            Redirect(make_admin_url('message', 'compose', 'compose&reply_username=' . $to_username));
                        } else {
                            $admin_user->set_error();
                            $admin_user->set_pass_msg("You are blocked by " . $to_username . ".");
                            Redirect(make_admin_url('message', 'compose', 'compose'));
                        }
                    } else {
                        $arr['from_id'] = $logged_user->id;
                        $arr['to_id'] = $user->id;
                        $arr['subject'] = $subject;
                        $arr['message'] = $message;
                        $obj = new userMessage;
                        $message_id = $obj->saveMessage($arr);

                        $query = new user;
                        $message_login = $query->update_message_login($user->id);
                        ?>
                        <!DOCTYPE html>
                        <html lang="en-US">
                            <head>
                                <meta charset="utf-8">
                            </head>
                            <body>
                                <div>
                                    <?php
                                    echo $new_message = '<div style="background-color:#EAEAEA;;padding: 20px;"><img src="http://qtx.in/kissconnection/assets/logo/logo.png" height="40" /><span style="float:right;color:grey">From: <span style="margin-top: 10px;margin-right: 22px;"><span style="color:#156397;"><a href="http://qtx.in/kissconnection/control.php?Page=account&action=view&section=view&id=' . $user_detail->id . '">' . $user_detail->username . '</a></span><br />Received ' . date('d/m/Y H:i:s A', time()) . '</span></span><br /><br /><div style="font-size:16px;font-size: 16px; border: 1px solid #337ab7; padding: 10px;background-color: #337ab7;"><img src="' . $login_photo_link . '" height="100" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style="position:absolute;color:#FFFFFF;">' . $user_detail->username . '</span><br /><br /><div style="background-color: #fff; padding: 10px;">' . $message . '<br /><br /><center><a href="' . DIR_WS_SITE . '?user_id=' . $user->id . '&id=' . $user_id . '&message_login=' . $message_login . '&message_id=' . $message_id . '" style="color: #fff; background-color: #1B689C; border-color: #2e6da4;display: inline-block; padding: 6px 12px; margin-bottom: 0; font-size: 14px; font-weight: 400; line-height: 1.42857143; text-align: center; white-space: nowrap; vertical-align: middle;background-image: none;border: 1px solid transparent; border-radius: 4px;text-decoration:none">REPLY NOW</a></center></div></div></div><br /><br />';
                                    ?>
                                    <br/>
                                </div>
                            </body>
                        </html>
                        <?php
                        send_email_by_cron($email, $new_message, $subject, SITE_EMAIL);
                        $admin_user->set_pass_msg('Message Sent Successfully!');
                        Redirect(make_admin_url('message', 'compose', 'compose&reply_username=' . $to_username));
                    }
                } else {
                    $admin_user->set_error();
                    $admin_user->set_pass_msg('Username Not Found!');
                    Redirect(make_admin_url('message', 'compose', 'compose'));
                }
            } else {
                $admin_user->set_error();
                $admin_user->set_pass_msg("You can't send message to yourself!");
                Redirect(make_admin_url('message', 'compose', 'compose'));
            }
        }

        if (isset($_GET['reply_username'])) {
            $query = new userMessage();
            $user = $query->get_object('user', $_GET['reply_username']);
            $query = new userMessage;
            $me_reply_user_messages = $query->my_get_user_messages($user->id, $user_id);
        }
        break;
    case'sent':
        $obj = new userMessage;
        $sent_messages = $obj->getSentMessages();

        if (isset($_POST['submit'])) {
            if ($_POST['d']) {
                foreach ($_POST['d'] as $message_id) {
                    $query = new userMessage;
                    $query->delete_sent_message($message_id);
                }
                $admin_user->set_pass_msg('Message delete successfully');
                Redirect(make_admin_url('message', 'sent', 'sent'));
            } else {
                $admin_user->set_error();
                $admin_user->set_pass_msg('Select Atleast one');
                Redirect(make_admin_url('message', 'sent', 'sent'));
            }
        }
        break;
    case'blocked':

        // Get Blocked List
        $obj = new userBlockList;
        $blockList = $obj->getBlockedList();

        // Block Users
        if (isset($_POST['block_user'])) {
            if ($_POST['username'] != $logged_user->username) {
                $user = user::getUserByUsername($_POST['username']);
                if ($user) {
                    $blockListExists = userBlockList::checkBlockListExists();
                    if ($blockListExists) {
                        $blockedIds = explode(',', $blockListExists->blocked);
                        if (!in_array($user->id, $blockedIds)) {
                            $blockedIds[] = $user->id;
                            $arr['id'] = $blockListExists->id;
                            $arr['blocked'] = $blockListExists->blocked ? implode(',', $blockedIds) : $user->id;
                            $obj = new userBlockList;
                            $obj->saveBlockList($arr);
                            $admin_user->set_pass_msg('User Blocked Successfully!');
                            Redirect(make_admin_url('message', 'blocked', 'blocked'));
                        } else {
                            $admin_user->set_error();
                            $admin_user->set_pass_msg('Username Already Blocked!');
                            Redirect(make_admin_url('message', 'blocked', 'blocked'));
                        }
                    } else {
                        $arr['user_id'] = $logged_user->id;
                        $arr['blocked'] = $user->id;
                        $obj = new userBlockList;
                        $obj->saveBlockList($arr);
                        $admin_user->set_pass_msg('User Blocked Successfully!');
                        Redirect(make_admin_url('message', 'blocked', 'blocked'));
                    }
                } else {
                    $admin_user->set_error();
                    $admin_user->set_pass_msg('Username Not Found!');
                    Redirect(make_admin_url('message', 'blocked', 'blocked'));
                }
            } else {
                $admin_user->set_error();
                $admin_user->set_pass_msg("You cannot block yourself.");
                Redirect(make_admin_url('message', 'blocked', 'blocked'));
            }
        }

        // Unblock Users
        if (isset($_POST['unblock_users'])) {
            extract($_POST);
            if (is_array($unblock)) {
                pr($unblock);
                $ids = explode(',', $blockList->blocked);
                foreach ($unblock as $user_id) {
                    $key = array_search($user_id, $ids);
                    unset($ids[$key]);
                }
                $arr['id'] = $blockList->id;
                $arr['blocked'] = implode(',', $ids);
                $obj = new userBlockList;
                $obj->saveBlockList($arr);
                $admin_user->set_pass_msg('Users Unblocked Successfully!');
                Redirect(make_admin_url('message', 'blocked', 'blocked'));
            } else {
                $admin_user->set_error();
                $admin_user->set_pass_msg("You did not select any members to remove from your blocked list.");
                Redirect(make_admin_url('message', 'blocked', 'blocked'));
            }
        }
        break;
    case'view_inbox':
        $obj = new userMessage;
        $message = $obj->getMessage($id);


        if ($message->seen == 0) {
            $query = new userMessage();
            $query->message_make_seen($id);
        }

        $obj = new user;
        $user = $obj->getUser($message->from_id);

        $query = new userPhoto;
        $query = new userPhoto;
        $primary_photo = $query->getProfilePhoto($user->id);

        break;
    case'view_sent':
        $obj = new userMessage;
        $message = $obj->getMessage($id);


        $obj = new user;
        $user = $obj->getUser($message->to_id);

        $query = new user;
        $user_info = $query->getUser($message->to_id);

        $obj = new userMessage;
        $sent_messages = $obj->getSentMessages($id);

        $query = new userPhoto;
        $primary_photo = $query->getProfilePhoto($sent_messages->to_id);

        break;
    case'delete':
        break;
    default:break;
endswitch;
