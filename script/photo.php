<?php

include_once(DIR_FS_SITE . 'include/functionClass/userMediaClass.php');
isset($_GET['action']) ? $action = $_GET['action'] : $action = 'list';
isset($_GET['section']) ? $section = $_GET['section'] : $section = 'list';
isset($_GET['id']) ? $id = $_GET['id'] : $id = '0';
$type = isset($_GET['type']) ? $_GET['type'] : '';
$user_id = $_SESSION['admin_session_secure']['user_id'];
$modName = 'photo';
switch ($action):
    case'list':
        $obj = new userPhoto;
        $photos = $obj->getUserPhoto();
        if (isset($_POST['upload_photo'])) {
            if (is_array($_FILES['photo']) && $_FILES['photo']['error'] == 0) {
                if (is_uploaded_file($_FILES['photo']['tmp_name'])) {
                    $sourcePath = $_FILES['photo']['tmp_name'];
                    $filename = time() . $_FILES['photo']['name'];
                    $targetPath = DIR_FS_SITE_ASSETS . "files/" . $filename;
                    if (!is_dir(DIR_FS_SITE_ASSETS . 'files/')) {
                        mkdir(DIR_FS_SITE_ASSETS . 'files/', 0777, true);
                    }
                    if (move_uploaded_file($sourcePath, $targetPath)) {
                        $_POST['user_id'] = $user_id;
                        $_POST['file_name'] = $filename;
                        $_POST['is_primary'] = isset($_POST['is_primary']) ? $_POST['is_primary'] : 0;
                        $obj = new userPhoto;
                        $obj->saveUserPhoto($_POST);
                        $admin_user->set_pass_msg('Photo Uploaded Successfully!');
                        Redirect(make_admin_url('photo'));
                    } else {
                        $admin_user->set_error();
                        $admin_user->set_pass_msg('Something Went Wrong!');
                        Redirect(make_admin_url('photo'));
                    }
                }
            }
        }
        break;

    case'insert':
        $obj = new sale_head;
        $sale_heads = $obj->listSaleHeads();

        $obj = new buyer;
        $buyers = $obj->listBuyers();
        break;
    case'update':
        $obj = new sale_head;
        $sale_heads = $obj->listSaleHeads();

        $obj = new buyer;
        $buyers = $obj->listBuyers();

        $obj = new sale;
        $sale = $obj->getSale($id);
        break;
    case'delete':
        $query = new userPhoto;
        $query->delete_photo($_GET['pid']);
        $admin_user->set_pass_msg('Photo Deleted Successfully');
        if ($type) {
            Redirect(make_admin_url($type));
        } else {
            Redirect(make_admin_url('photo'));
        }
        break;
    case'change':
        $query = new userPhoto;
        $query->removeAllProfilePhoto();

        $query = new userPhoto;
        $query->setPrimaryPhoto($_GET['pid'], '1');
        $admin_user->set_pass_msg('Primary Image Change Successfully!');
        if ($type) {
            Redirect(make_admin_url($type));
        } else {
            Redirect(make_admin_url('photo'));
        }
        break;
    default:break;
endswitch;
?>
