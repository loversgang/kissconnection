<?php

include_once(DIR_FS_SITE . 'include/functionClass/userMediaClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/metaDataClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/searchClass.php');

isset($_GET['action']) ? $action = $_GET['action'] : $action = 'list';
isset($_GET['section']) ? $section = $_GET['section'] : $section = 'list';
isset($_GET['id']) ? $id = $_GET['id'] : $id = '0';
$user_id = $_SESSION['admin_session_secure']['user_id'];
$obj = new user;
$logged_user = $obj->getUser();
$modName = 'search';
switch ($action):
    case'list':

        break;
    case'quick':
        if (isset($_POST['quick_search'])) {
            $obj = new user;
            $results = $obj->getQuickSearchResults($_POST);
        }
        break;
    case'username':
        if (isset($_POST['username_search'])) {
            $user = user::getUserByUsername($_POST['username']);
        }
        break;
    case'full':

        $obj = new meta;
        $metaDataMatch = $obj->listMetaByGroupId(1);
        $obj = new search;
        $searches = $obj->listSavedSearch();

        if (isset($_POST['get_saved_search'])) {
            $preValues = array();
            $obj = new searchData;
            $searchData = $obj->listSavedSearchData($_POST['saved_search']);
            foreach ($searchData as $data) {
                $preValues[$data->name] = explode('||', $data->value);
            }
        }

        if (isset($_GET['sid'])) {
            $preValues = array();
            $obj = new searchData;
            $searchData = $obj->listSavedSearchData($_GET['sid']);
            foreach ($searchData as $data) {
                $preValues[$data->name] = explode('||', $data->value);
            }
        }

        break;
    case'results':
        if (isset($_POST['full_search'])) {
            $query = '';
            $values_array = array();
            foreach ($_POST['user'] as $k => $v) {
                $query.='user_metadata.key_id=' . $k . ' AND ';
                if ($k == 14 || $k == 15) {
                    $values_array[] = implode('||', $v);
                }
                foreach ($v as $key => $val) {
                    $values_array[] = $val;
                }
            }
            $unique_values_array = array_unique($values_array);
            $keyID = array_search('Ask me later', $unique_values_array);
            unset($unique_values_array[$keyID]);
            foreach ($unique_values_array as $value) {
                if ($value == end($unique_values_array)) {
                    $query.='user_metadata.value="' . $value . '" AND ';
                } else {
                    $query.='user_metadata.value="' . $value . '" OR ';
                }
            }
            if ($_POST['save_search'] != '') {
                $arr['user_id'] = $user_id;
                $arr['title'] = $_POST['save_search'];
                $obj = new search;
                $search_id = $obj->saveSearch($arr);
                // Save Search Values
                $array['gender'] = $_POST['gender'];
                $array['from_age'] = $_POST['from_age'];
                $array['to_age'] = $_POST['to_age'];
                $array['foot_from'] = $_POST['foot_from'];
                $array['inch_from'] = $_POST['inch_from'];
                $array['foot_to'] = $_POST['foot_to'];
                $array['inch_to'] = $_POST['inch_to'];
                $array['distance'] = $_POST['distance'];
                foreach ($_POST['user'] as $k => $v) {
                    $array[$k] = implode('||', $v);
                }
                $array['activity'] = $_POST['activity'];
                $array['photos_only'] = $_POST['photos_only'];
                foreach ($array as $array_name => $array_value) {
                    $data['search_id'] = $search_id;
                    $data['name'] = $array_name;
                    $data['value'] = $array_value;
                    $obj = new searchData;
                    $obj->saveSearch($data);
                }
            }
            if ($_POST['activity']) {
                extract($_POST);
                if ($activity == 'd2') {
                    $time = strtotime("-2 days");
                } elseif ($activity == 'w1') {
                    $time = strtotime("-1 week");
                } elseif ($activity == 'w2') {
                    $time = strtotime("-2 weeks");
                } elseif ($activity == 'm1') {
                    $time = strtotime("-1 month");
                } elseif ($activity == 'm2') {
                    $time = strtotime("-2 months");
                }
                $query.='user.last_access > "' . $time . '" AND ';
            }
            //echo $query;
            $obj = new user;
            $results = $obj->getFullSearchResults($_POST, $query);

            $obj = new user;
            $currentuser = $obj->getSingelUser($user_id);

            $user_matches = array();
            foreach ($results as $result) {
                $matches = 0;

                $obj = new meta;
                $metaDataAbout = $obj->listMetaByGroupId(1);
                $total_count = count($metaDataAbout);

                foreach ($metaDataAbout as $key => $meta) {
                    $user_key_value = metaData::getMetaValue($meta->id, $result->user_id);
                    $ct_user_key_value = metaData::getMetaValue($meta->id, $user_id);
                    if ($user_key_value == $ct_user_key_value) {
                        $matches ++;
                    }
                }
                $obj = new user;
                $matchUser = $obj->getSingelUser($result->user_id);

                if ($matchUser->gender == $currentuser->gender) {
                    $matches ++;
                }
                if ($matchUser->dob == $currentuser->dob) {
                    $matches ++;
                }
                if ($matchUser->age == "$currentuser->age") {
                    $matches ++;
                }
                if ($matchUser->foot == "$currentuser->foot") {
                    $matches ++;
                }
                if ($matchUser->inch == "$currentuser->inch") {
                    $matches ++;
                }
                if ($matchUser->zip == "$currentuser->zip") {
                    $matches ++;
                }
                if ($matchUser->lat == "$currentuser->lat") {
                    $matches ++;
                }


                $total_count = $total_count + 7;

                // if($user_id!=$result->user_id){
                $profilematch[$result->user_id] = array('total' => $total_count, 'matches' => $matches, 'percentage' => ($matches / $total_count) * 100);

                // }
            }

            // echo '<pre>'; print_r($profilematch); exit;
        }
        break;
    case'saved':
        $obj = new search;
        $searches = $obj->listSavedSearch();
        if (isset($_POST['delete_searches'])) {
            if (is_array($_POST['delete'])) {
                foreach ($_POST['delete'] as $search_id) {
                    $obj = new search;
                    $obj->id = $search_id;
                    $obj->Delete();
                    $admin_user->set_pass_msg("Your list has been updated.");
                    Redirect(make_admin_url('search', 'saved', 'saved'));
                }
            } else {
                $admin_user->set_error();
                $admin_user->set_pass_msg("You did not select any searches to remove from your list.");
                Redirect(make_admin_url('search', 'saved', 'saved'));
            }
        }
        break;
    default:break;
endswitch;
