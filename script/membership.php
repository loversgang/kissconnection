<?php

isset($_GET['action']) ? $action = $_GET['action'] : $action = 'list';
isset($_GET['section']) ? $section = $_GET['section'] : $section = 'list';
isset($_GET['id']) ? $id = $_GET['id'] : $id = '0';

$modName = 'membership';
#handle actions here.
switch ($action):
    case'list':
        $user_id = $_SESSION['admin_session_secure']['user_id'];

        $obj = new user;
        $user = $obj->getUser($user_id);
        
        if (isset($_POST['profile_visible'])) {
            $query = new user;
            $query->is_visible($user_id, $_POST['is_visible']);
            $admin_user->set_pass_msg('Profile Visibility Changed');
            Redirect(make_admin_url('membership'));
        }
        
        if (isset($_POST['change_username'])) {
            if ($user->password == md5($_POST['password'])) {
                if ($_POST['new_username'] == $_POST['confirm_new_username']) {
                    $obj = new user;
                    $usernameExists = $obj->checkField('username', $_POST['new_username']);
                    if (!$usernameExists) {
                        $arr['id'] = $user_id;
                        $arr['username'] = $_POST['new_username'];
                        $obj = new user;
                        $obj->saveUser($arr);
                        $admin_user->set_pass_msg('Username Updated Successfully!');
                        Redirect(make_admin_url('membership'));
                    } else {
                        $admin_user->set_error();
                        $admin_user->set_pass_msg('Username Already Exists!');
                        Redirect(make_admin_url('membership'));
                    }
                } else {
                    $admin_user->set_error();
                    $admin_user->set_pass_msg('Username Not Matched!');
                    Redirect(make_admin_url('membership'));
                }
            } else {
                $admin_user->set_error();
                $admin_user->set_pass_msg('Wrong Password!');
                Redirect(make_admin_url('membership'));
            }
        }
        if (isset($_POST['change_password'])) {
            pr($_POST);
            if ($user->password == md5($_POST['password'])) {
                if ($_POST['new_password'] == $_POST['confirm_new_password']) {
                    $arr['id'] = $user_id;
                    $arr['password'] = md5($_POST['new_password']);
                    $obj = new user;
                    $obj->saveUser($arr);
                    $admin_user->set_pass_msg('Password Updated Successfully!');
                    Redirect(make_admin_url('membership'));
                } else {
                    $admin_user->set_error();
                    $admin_user->set_pass_msg('Password Not Matched!');
                    Redirect(make_admin_url('membership'));
                }
            } else {
                $admin_user->set_error();
                $admin_user->set_pass_msg('Wrong Current Password!');
                Redirect(make_admin_url('membership'));
            }
        }
        if (isset($_POST['change_email'])) {
            if ($user->password == md5($_POST['password'])) {
                if ($_POST['new_email'] == $_POST['confirm_new_email']) {
                    $obj = new user;
                    $emailExists = $obj->checkField('email', $_POST['new_email']);
                    if (!$emailExists) {
                        $arr['id'] = $user_id;
                        $arr['email'] = $_POST['new_email'];
                        $obj = new user;
                        $obj->saveUser($arr);
                        $admin_user->set_pass_msg('Email Updated Successfully!');
                        Redirect(make_admin_url('membership'));
                    } else {
                        $admin_user->set_error();
                        $admin_user->set_pass_msg('Email Already Exists!');
                        Redirect(make_admin_url('membership'));
                    }
                } else {
                    $admin_user->set_error();
                    $admin_user->set_pass_msg('Email Not Matched!');
                    Redirect(make_admin_url('membership'));
                }
            } else {
                $admin_user->set_error();
                $admin_user->set_pass_msg('Wrong Password!');
                Redirect(make_admin_url('membership'));
            }
        }
        if (isset($_POST['update_auto_login'])) {
            if ($_POST['is_auto_login'] == 1) {
                setcookie("kissconnection", $user->username, time() + (365 * 24 * 60 * 60), "/");
            }
            if ($_POST['is_auto_login'] == 0) {
                setcookie('kissconnection', '', time() - 3600, '/');
            }
            $arr['id'] = $user_id;
            $arr['is_auto_login'] = $_POST['is_auto_login'];
            $obj = new user;
            $obj->saveUser($arr);
            $admin_user->set_pass_msg('Auto-login Updated Successfully!');
            Redirect(make_admin_url('membership'));
        }
        break;

    case'insert':
        break;
    case'update':
        break;
    case'delete':
        break;
    default:break;
endswitch;
