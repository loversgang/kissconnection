<?php

# Login messages.
define("MSG_LOGIN_INVALID_USERNAME_PASSWORD", "Invalid <b>Username</b> or <b>Password</b>.", true);
define("MSG_LOGIN_WELCOME_MSG", "<span>Welcome to my account section.</span>", true);
define("MSG_LOGIN_EMAIL_ADDRESS_ALREADY_EXIST", "This email address already exists. Please try again.", true);
define("MSG_LOGIN_USERNAME_ALREADY_EXIST", "This Username already exists. Please try again.", true);
define("MSG_LOGIN_WRONG_PASSWORD", "Password does not match.", true);
define("MSG_LOGIN_EMPTY_PASSWORD", "Password cannot be left empty.", true);
define("MSG_LOGIN_ALREADY_LOGGED_IN", "Sorry! you are already logged in.");
define("MSG_LOGIN_EMAIL_FORMATE", "Please use correct Email format <b>like: username@username.com</b>.<br/>You cannot leave <b>password field</b> blank.");
define("MSG_LOGIN_NOT_BLANK", "You cannot leave <b>email field</b> blank.<br/>You cannot leave <b>password field</b> blank.");
define("MSG_LOGIN_ELSE", "You cannot leave <b>email field</b> blank.<br/> Please use correct Email format <b>like: username@username.com</b>.<br/> You cannot leave <b>password field</b> blank.");
define("MSG_SEC_NOT_VALID", "Security code could not be validated. Please try again.");
# my account messages.
define("MSG_REGISTER_PASSWORDS_NOT_SAME", "Your passwords do not match.", true);
define("MSG_ACCOUNT_PASSWORD_CHANGE_SUCCESS", "<span>Your password has been successfully changed.</span>", true);
define("MSG_ACCOUNT_UPDATE_SUCCESS", "<span>Your account information has been updated successfully.</span>", true);
define("MSG_ACCOUNT_UPDATE_FAILED", "Your account was not updated. Please try again.", true);
define("MSG_ACCOUNT_ALREADY_ACTIVE", "Your account is already active. Please <a href='" . DIR_WS_SITE . "?page=login'>click here</a> to login.");
define("MSG_ACCOUNT_MADE_ACTIVE", "<span>Your account has been activated. An email containing your login details has been sent to your email address. Please <a href='" . DIR_WS_SITE . "?page=login'>click here</a> to login.</span>");
define("MSG_ACCOUNT_NOT_MADE_ACTIVE", "Alas! some techinical malfunction has taken place. Please try again.");
define("MSG_ACCOUNT_WRONG_USER_INFO", "You have used a wrong user information. If you are not a registred user yet. Please <a href='" . DIR_WS_SITE . "?page=register'>click here</a> to register.");
define("MSG_ACCOUNT_WRONG_URL", "You have used a wrong URL. If you are not a registred user yet. Please <a href='" . DIR_WS_SITE . "?page=login'>click here</a> to register.");
define("MSG_ACCOUNT_CONFIRM_EMAIL_RESEND_SUCCESS", "<span>Confirmation email has been successfully resent to your registered email address.</span>");
define("MSG_ACCOUNT_CONFIRM_EMAIL_NEW_RESEND_SUCCESS", "<span>Confirmation email has been successfully sent to your new email address.</span>");


#logout messages.
define("MSG_LOGOUT_SUCCESS", "<span>You have successfully <b>logged out</b>.</span>", true);


#registrations messages.
define("MSG_REGISTR_FAILED", "Sorry! Registration process failed. Please try again.");
define("MSG_REGISTER_PASSWORDS_NOT_SAME", "Password and confirm password must be same.");
define("MSG_REGISTER_EMAIL_ALREADY_EXIST", "Sorry! this email address already exists. If you have forgotten your login details. Please <a href='" . DIR_WS_SITE . "?page=login'>click here</a> to know your login details.");
define("MSG_REGISTER_SUCCESS", "<span>You details have been accepted.</span>");
define("MSG_REGISTER_EMAIL_ALREADY_EXIST1", "Sorry! this email address already exists.Please <a target='_parent' href='" . DIR_WS_SITE . "?page=login'>click here</a> to login .");
define("MSG_REGISTER_EMAIL_ALREADY_EXIST2", "Sorry! this email address already exists.");
define("TERMS_AND_CONDITION", "Please accept the terms of services");
define("MSG_REGISTER_EMAIL_ALREADY_EXIST3", "Sorry! this email address already exists.Please <b>login</b> to continue or use another email address");


# forgot password messages.
define("MSG_FORGOT_PASSWORD_SUCCESS", "<span>A link has been sent to your email address. Please click that link to set new password.</span>");
define("MSG_FORGOT_PASSWORD_FAIL", "Your email address does not exist in our database.<br />If you are a new user. Please <a href='" . DIR_WS_SITE . "?page=register'>click here</a> to register.<br /> You can <a href='" . DIR_WS_SITE . "?page=forgotpassword'>click here</a> to try again.");
define("MSG_FORGOT_PASSWORD_FAIL1", "Your email address does not exist in our database.<br /> You can <a href='" . DIR_WS_SITE . "?page=forgotpassword'>click here</a> to try again.");



# change password messages.
define("MSG_CHANGE_PASSWORD_SUCCESS", "<span>Your password has been changed successfully.<br>Your new login details have been sent to your email address.</span>");
define("MSG_CHANGE_PASSWORD_FAILED", "You have entered a wrong password.");
define("MSG_PASSWORD_RESET", "<span>Your <b>account password</b> has been reset successfully.</span>");
define("MSG_NOT_AUTHORIZED", "You are not <b>authorized</b> to do this.");

#others.
define("MSG_ONLINE_ENQUIRY_SUCCESS", "<span>Your online enquiry has successfully been submitted.</span>");
define("MSG_FREE_SAMPLE_SENT_SUCCESS", "<span>Thank you, Your free sample request has been sent. Our support staff will contact you soon.</span>");
define("MSG_PAYMENT_SUCCESS", "<span>Your order has successfully been placed. You can track your order states by logging into your online account.<br/> Your login details 
have been emailed to you (in case you are a new customer). Please <a href='" . DIR_WS_SITE . "?page=login'><strong><u>click here</u></strong></a> to login.</span>");
define("MSG_CUSTOM_SUCCESS", "<span>Thank you for shopping at " . SITE_NAME . ".Your order will be processed shortly.</span>");

#cart messages.
define("MSG_CART_OUT_OF_STOCK", 'Sorry! This item is currently out of stock.');
define("MSG_CART_ALREADY_IN_CART", 'This item is already in the cart.');

#shopping messages.
define("MSG_SHOP_CATEGORY_NO_PRODUCT_FOUND", "Sorry! There is no product in this category.", true);
define("MSG_SHOP_ORDER_COMPLETE_SUCCESS", "<span>You have successfully completed your order.</span>", true);


# website control panel messages.
define("MSG_ADMIN_UPDATE_SUCCESS", "<span>Record updated successfully</span>", true);
define("MSG_ADMIN_DELETE_SUCCESS", "<span>Record deleted successfully</span>", true);
define("MSG_ADMIN_ADDITION_SUCCESS", "<span>Record added successfully</span>", true);
define("MSG_ADMIN_PERMISSION_DENIED", "You donot have permission to access the page.", true);


define("MSG_CONTACTUS", "<span>Your request have been submitted successfully.</span>");
define("MSG_CONTACTUS_CAP", "The entered code was not correct. Please try again.");
define("MSG_CONTACTUS_COMPLETE_INFO", "Please enter compalete information");

define("MSG_BUSINESS", "<span>Your Business has been added successfully. Please wait for the administrator approvel</span>");
define("MSG_INTERACTIVE_MAP", "Please select a Business Category.");



/* FundUni Messages */

define("MSG_ACCOUNT_MADE_BLOCKED", "Sorry, your account has been deactivated by admin due to some reasons. Please try later");


define("MSG_CAMPAIGN_IS_SUCCESSFULL", "This campaign successfully raised its funding goal.");
define("MSG_CAMPAIGN_IS_REJECTED", "This campaign is rejected by admin.");
define("MSG_CAMPAIGN_IS_EXPIRED", "This campaign reached the deadline without achieving its funding goal.");
define("MSG_CAMPAIGN_IS_SUSPENDED", "This campaign is suspended by admin due to some reasons.");



define("MSG_NO_CAMPAIGN_IN_CATEGORY", "Sorry, No Campaign Found in this Category.");
define("MSG_NO_CAMPAIGN_FOR_PERK_TYPE", "Sorry, No Campaign Found for this Reward type.");
define("MSG_NO_CAMPAIGN_FOR_SEARCHED_KEYWORD", "Sorry, No Campaign Found for this keyword.");
define("MSG_NO_CAMPAIGN_CLOSEBY_YOU", "Sorry, No Campaign found close by your location.");
define("MSG_NO_CAMPAIGN_FOR_LOCATION", "Sorry, No Campaign Found for this location.");


define("MSG_USER_HAVE_NO_CAMPAIGN_PUBLIC_PROFILE", "User have created 0 campaign.");
define("MSG_USER_HAVE_NO_CURRENT_CAMPAIGN", "You do not have any current campaign.");
define("MSG_USER_HAVE_NO_PREVIOUS_CAMPAIGN", "No previous campaign found.");
define("MSG_EDIT_USER_PROFILE_SUCCESS", "<span>Your profile information has been updated successfully.</span>");


define("MSG_EDIT_CAMPAIGN_TITLE_SUCCESS", "<span>Information has been updated successfully.</span>");
define("MSG_CAMPAIGN_DESCRIPTION_CHANGE_SUCCESS", "<span>Information has been updated successfully.</span>");


define("MSG_ADD_PERK_SUCCESS", "<span>Reward has been added successfully.</span>");
define("MSG_EDIT_PERK_SUCCESS", "<span>Reward has been updated successfully.</span>");

define("MSG_NO_UPDATE_FOUND_OF_CAMPAIGN", "Sorry, No Update Found for this campaign.");
define("MSG_ADD_UPDATE_SUCCESS", "<span>Update has been added successfully.Please wait for admin approval.</span>");
define("MSG_ADD_UPDATE_SUCCESS1", "<span>Update has been added successfully.<span>");

define("MSG_CAMPAIGN_NO_IMAGE_IN_EXTRA_STUFF", "No image found for this campaign.");
define("MSG_CAMPAIGN_NO_AUDIO_IN_EXTRA_STUFF", "No audio found for this campaign.");
define("MSG_CAMPAIGN_NO_VIDEO_IN_EXTRA_STUFF", "No video found for this campaign.");
define("MSG_CAMPAIGN_NO_DOCUMENT_IN_EXTRA_STUFF", "No document found for this campaign.");
define("MSG_CAMPAIGN_NO_EXTRA_STUFF", "No extra stuff found for this campaign.");


define("MSG_CAMPAIGN_MEDIA_UPDATE_SUCCESS", "<span>Media Files has been updated successfully.</span>");
define("MSG_CAMPAIGN_VIDEO_UPDATE_SUCCESS", "<span>Campaign has been updated successfully.</span>");
define("MSG_CAMPAIGN_IMAGE_UPDATE_SUCCESS", "<span>Campaign Image has been updated successfully.</span>");


define("MSG_NO_FUNDER_FOUND_FOR_CAMPAIGN", "No funder found for this campaign.");
define("MSG_FUNDING_FAIL", " Sorry , your Funding Process is not success full due to some reasons.!");
define("MSG_NO_PERKS_FOR_ME", "No funder found for your campaigns."); /* App Message */define("MSG_ADD_FOOD_SUCCESS", "Food has been added successfully.");
define("MSG_EDIT_FOOD_SUCCESS", "Food has been updated successfully.");
define("OPERATION_PERFORM_SUCCESS", "Operation performed successfully.");
define("MSG_RESTORE_FOOD_SUCCESS", "Food has been restore  successfully.");
define("MSG_ADD_USER_SUCCESS", "User has been added successfully.");
define("MSG_EDIT_USER_SUCCESS", "User has been updated successfully.");
define("MSG_RESTORE_USER_SUCCESS", "User has been restore  successfully.");
?>