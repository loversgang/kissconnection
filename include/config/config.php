<?php

/* output buffering started */
ob_start();

/* set error level */
error_reporting(0);

/* start session by default */
session_start();

/*
 *   Code Developed By: cWebConsultants Team - India (Chandigarh)
 *   Project Name: <content managment system> - cWebConsultants
 *   Dated: <10 Jan, 2012>
 *   *** Copyrighted by cWebConsultants India - We reserve the right to take legal action against anyone using this software without our permission.  ***
 */

/* set document root */
define("DIR_FS", $_SERVER['DOCUMENT_ROOT'], true);

/* set website filesystem */
define("DIR_FS_SITE", dirname(dirname(dirname(__FILE__))) . '/', true);

/*
 * Be very carefully while setting these variables 
 * These are used in the URL Rewrite
 */
/*
 * set databse details here 
 */
$DBHostName = "localhost";
$DBDataBase = "kissconnection";
$DBUserName = "root";
$DBPassword = "cwebco";

$is_local = false;
$is_qtx = false;
$is_rxy = false;
$is_keithharte = false;
if (strpos($_SERVER['HTTP_HOST'], 'qtx.in') !== false) {
    $is_qtx = true;
} elseif (strpos($_SERVER['HTTP_HOST'], 'rxy.in') !== false) {
    $is_rxy = true;
} elseif (strpos($_SERVER['HTTP_HOST'], 'khbsales.keithharte.com') !== false) {
    $is_keithharte = true;
} else {
    $is_local = true;
}
if ($is_local) {
    define("HTTP_SERVER", "http://localhost/", true);
}
if ($is_qtx) {
    define("HTTP_SERVER", "http://qtx.in/", true);
    $DBPassword = "GtZhBjPDRYA5aDSVm";
}

if ($is_rxy) {
    define("HTTP_SERVER", "http://rxy.in/", true);
    $DBDataBase = "rxyin_khb";
    $DBUserName = "rxyin_khb";
    $DBPassword = "khb@123";
}
if ($is_keithharte) {
    define("DIR_WS_SITE", "http://khbsales.keithharte.com/", true);
    $DBDataBase = "cwebcons_khbsales";
    $DBUserName = "cwebcons_khbsale";
    $DBPassword = "R^4V4Zi4ogTm";
} else {
    define("DIR_WS_SITE", HTTP_SERVER . "kissconnection/", true);
}

/*
 *   --- WARNING ---
 *  All the files below are location sensitive. 
 *  Maintain the sequence of files
 *
 */

# include sub-configuration files here.
require_once(DIR_FS_SITE . "include/config/url.php");

# include the database class files.
require_once(DIR_FS_SITE_INCLUDE_CLASS . "mysql.php");
require_once(DIR_FS_SITE_INCLUDE_CLASS . "query.php");
# include session files here.
# include the utitlity files here
require_once(DIR_FS_SITE_INCLUDE_CLASS . "phpmailer.php");
require_once(DIR_FS_SITE_INCLUDE_CONFIG . "constant.php");
require_once(DIR_FS_SITE_INCLUDE_CONFIG . "message.php");

# custom files
include_once(DIR_FS_SITE_INCLUDE_CLASS . 'admin_session.php');
# include functions here.
include_once(DIR_FS_SITE_INCLUDE_FUNCTION . 'date.php');
include_once(DIR_FS_SITE_INCLUDE_FUNCTION . 'email.php');
include_once(DIR_FS_SITE_INCLUDE_FUNCTION . 'users.php');
//require_once(DIR_FS_SITE_INCLUDE_FUNCTION."dBug.php");
# include function files here.
include_once(DIR_FS_SITE . 'include/function/basic.php');
require_once(DIR_FS_SITE_INCLUDE_FUNCTION_CLASS . "class.php");
require_once(DIR_FS_SITE_INCLUDE_FUNCTION_CLASS . "userClass.php");
require_once(DIR_FS_SITE_INCLUDE_FUNCTION_CLASS . "userMessageClass.php");


if ($admin_user->is_logged_in()) {
    $query = new userMessage;
    $unseen_message_count = $query->unseen_message($_SESSION['admin_session_secure']['user_id']);
}

#date_default_timezone_set('Asia/Calcutta');
?>