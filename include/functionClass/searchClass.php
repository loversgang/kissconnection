<?php

class search extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    /* */

    function __construct($order = 'asc', $orderby = 'id') {
        parent::__construct('user_saved_search');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'user_id', 'title', 'date_add');
    }

    function saveSearch($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            if ($this->Update()) {
                return $this->Data['id'];
            }
        } else {
            $this->Data['date_add'] = time();
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    function getSavedSearch($id) {
        return $this->_getObject('user_saved_search', $id);
    }

    function listSavedSearch($user_id = 0) {
        if ($user_id == 0) {
            $user_id = $_SESSION['admin_session_secure']['user_id'];
        }
        $this->Where = "WHERE user_id='$user_id'";
        return $this->ListOfAllRecords('object');
    }

}

class searchData extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    /* */

    function __construct($order = 'asc', $orderby = 'id') {
        parent::__construct('user_saved_search_metadata');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'search_id', 'name', 'value');
    }

    function saveSearch($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            if ($this->Update()) {
                return $this->Data['id'];
            }
        } else {
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    function getSavedSearch($id) {
        return $this->_getObject('user_saved_search', $id);
    }

    function listSavedSearchData($search_id) {
        $this->Where = "WHERE search_id='$search_id'";
        return $this->ListOfAllRecords('object');
    }

}
