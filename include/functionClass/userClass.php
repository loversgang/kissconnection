<?php

/*
 * food Module Class - 
 * You are not adviced to make edits into this class.
 * Created By :- cWebConsultants India
 * http://www.cwebconsultants.com
 * Package :- cWebconsultants's web content management system
 *   
 */

class user extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    /* */

    function __construct($order = 'asc', $orderby = 'first_name') {
        parent::__construct('user');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'username', 'password', 'email', 'is_active', 'last_access', 'gender', 'date_add', 'ip_address', 'is_loggedin', 'is_auto_login', 'is_cupid', 'dob', 'foot', 'inch', 'zip', 'age', 'lat', 'lng', 'message_login');
    }

    /*
     * Create new or update existing 
     */

    function saveUser($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        $this->Data['last_access'] = time();
        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            if ($this->Update()) {
                return $this->Data['id'];
            }
        } else {
            $this->Data['password'] = md5($POST['password']);
            $this->Data['date_add'] = time();
            $this->Data['ip_address'] = $_SERVER['REMOTE_ADDR'];
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    /*
     * Get User by id
     */

    function getUser($id = 0) {
        if ($id == 0) {
            $id = $_SESSION['admin_session_secure']['user_id'];
        }
        return $this->_getObject('user', $id);
    }

    function getAllUser($id = 0) {
        $this->Where = "where id=$id";
        return $this->ListOfAllRecords('object');
    }

    function getSingelUser($id = 0) {
        $this->Where = "where id=$id";
        return $this->DisplayOne();
    }

    /*
     * Get List of all User in object array
     */

    function listUsers() {
        return $this->ListOfAllRecords('object');
    }

    function limitedUserInfo() {
        $this->Field = ('id,first_name,last_name');
        $this->Where = "where is_deleted='0' AND is_active='1' order by $this->orderby $this->order";
        return $this->ListOfAllRecords('object');
    }

    function checkField($field, $value) {
        $this->Where = "where `" . $field . "`= '" . $value . "'";
        return $this->DisplayOne();
    }

    function listTrashUser() {
        $this->Where = "where is_deleted='1' order by $this->orderby $this->order";
        return $this->ListOfAllRecords('object');
    }

    /*
     * delete a User by id
     */

    function deleteUser($id) {
        $this->id = $id;
        if (SOFT_DELETE) {
            return $this->SoftDelete();
        } else {
            return $this->Delete();
        }
    }

    // Get User Detail by Username

    public static function getUserByUsername($username) {
        $obj = new user;
        $obj->Where = "where username='$username' AND `is_visible` = 1";
        $data = $obj->DisplayOne();
        return is_object($data) ? $data : '';
    }

    function getQuickSearchResults($options = array()) {
        $user_id = $_SESSION['admin_session_secure']['user_id'];
        extract($options);
        $results = getLnt($zip);
        $lat = $results['lat'];
        $lng = $results['lng'];
        if ($distance == '0') {
            $distance = 57470147.9926;
        }
        $this->Field = "*, (3959 * acos(cos(radians($lat)) * cos(radians(lat)) * cos(radians(lng) - radians($lng)) + sin(radians($lat)) * sin(radians(lat)))) AS distance";
        $this->Where = "WHERE id !='$user_id' AND gender='$gender' AND (age BETWEEN $from_age AND $to_age)  AND `is_visible` = 1 HAVING distance < $distance";
        return $this->ListOfAllRecords('object');
    }

    function getFullSearchResults($options = array(), $query = '') {
        $user_id = $_SESSION['admin_session_secure']['user_id'];
        extract($options);
        $results = getLnt($zip);
        $lat = $results['lat'];
        $lng = $results['lng'];
        if ($distance == '0') {
            $distance = 57470147.9926;
        }
        $obj = new query('user,user_metadata');
        $obj->Field = "user.*, user_metadata.user_id, (3959 * acos(cos(radians($lat)) * cos(radians(lat)) * cos(radians(lng) - radians($lng)) + sin(radians($lat)) * sin(radians(lat)))) AS distance";
        $obj->Where = "WHERE $query user.gender='$gender' AND (user.foot BETWEEN $foot_from AND $foot_to) AND (user.inch BETWEEN $inch_from AND $inch_to) AND (user.age BETWEEN $from_age AND $to_age) AND user.id!='$user_id' AND user_metadata.user_id !='$user_id' AND user.id=user_metadata.user_id GROUP BY user_metadata.user_id HAVING distance < $distance";
        return $obj->ListOfAllRecords('object');
    }

    function getMatchResults($match) {
        $user = $this->getUser();
        $results = getLnt($user->zip);
        $lat = $results['lat'];
        $lng = $results['lng'];
        if ($match->distance == '0') {
            $distance = 57470147.9926;
        } else {
            $distance = $match->distance;
        }
        $this->Field = "user.*,(3959 * acos(cos(radians($lat)) * cos(radians(lat)) * cos(radians(lng) - radians($lng)) + sin(radians($lat)) * sin(radians(lat)))) AS distance";
        $this->Where = "WHERE gender='$match->gender' AND (age BETWEEN $match->from_age AND $match->to_age) AND (foot BETWEEN $match->from_foot AND $match->to_foot) AND (inch BETWEEN $match->from_inch AND $match->to_inch) AND id != $user->id GROUP BY id HAVING distance < $distance";
        return $this->ListOfAllRecords('object');
    }

    function getMatchResultsLatest($match) {
        $user = $this->getUser($match->user_id);
        $results = getLnt($user->zip);
        $lat = $results['lat'];
        $lng = $results['lng'];
        if ($match->distance == '0') {
            $distance = 57470147.9926;
        } else {
            $distance = $match->distance;
        }
        $this->Field = "user.*,(3959 * acos(cos(radians($lat)) * cos(radians(lat)) * cos(radians(lng) - radians($lng)) + sin(radians($lat)) * sin(radians(lat)))) AS distance";
        $this->Where = "WHERE gender='$match->gender' AND (age BETWEEN $match->from_age AND $match->to_age) AND (foot BETWEEN $match->from_foot AND $match->to_foot) AND (inch BETWEEN $match->from_inch AND $match->to_inch) AND id != $user->id GROUP BY id HAVING distance < $distance";
        return $this->ListOfAllRecords('object');
    }

    function is_visible($user_id, $is_visible) {
        $this->Data['id'] = $user_id;
        $this->Data['is_visible'] = $is_visible;
        $this->Update();
    }

    function get_messages_using_from_id($from_id) {
        $query = new user;
        $query->Where = "WHERE `id` = '$from_id'";
        return $query->DisplayOne();
    }

    function get_email_using_username($username) {
        $this->Where = "WHERE `username` = '$username'";
        return $this->DisplayOne();
    }

    function update_message_login($to_id) {
        $this->Data['id'] = $to_id;
        $old_message_logins = $this->getUser($to_id)->message_login;
        $time = time();
        if ($old_message_logins) {
            $this->Data['message_login'] = $old_message_logins . ' ' . $time;
        } else {
            $this->Data['message_login'] = $time;
        }
        $this->Update();
        return $time;
    }

}

class userMatch extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    /* */

    function __construct($order = 'asc', $orderby = 'id') {
        parent::__construct('user_match');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'user_id', 'gender', 'from_age', 'to_age', 'from_foot', 'to_foot', 'from_inch', 'to_inch', 'distance');
    }

    function saveUserMatch($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            if ($this->Update()) {
                return $this->Data['id'];
            }
        } else {
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    function getUserMatch($user_id = 0) {
        if ($user_id == 0) {
            $user_id = $_SESSION['admin_session_secure']['user_id'];
        }
        $this->Where = "WHERE user_id='$user_id'";
        return $this->DisplayOne();
    }

}

class userBlockList extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    /* */

    function __construct($order = 'asc', $orderby = 'id') {
        parent::__construct('user_block_list');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'user_id', 'blocked');
    }

    function saveBlockList($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            if ($this->Update()) {
                return $this->Data['id'];
            }
        } else {
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    public static function checkBlockListExists() {
        $user_id = $_SESSION['admin_session_secure']['user_id'];
        $obj = new userBlockList;
        $obj->Where = "WHERE user_id='$user_id'";
        return $obj->DisplayOne();
    }

    function getBlockedList($user_id = 0) {
        if ($user_id == 0) {
            $user_id = $_SESSION['admin_session_secure']['user_id'];
        }
        $this->Where = "WHERE user_id='$user_id'";
        $data = $this->DisplayOne();
        return is_object($data) ? $data : '';
    }

}
