<?php

class meta extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    /* */

    function __construct($order = 'asc', $orderby = 'sequence') {
        parent::__construct('user_meta');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'title', 'default_value', 'type', 'possible_value', 'sequence', 'group_id', 'is_matching', 'is_searchable');
    }

    function saveMeta($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            if ($this->Update()) {
                return $this->Data['id'];
            }
        } else {
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    function getMetaGroup($id) {
        return $this->_getObject('user_meta_groups', $id);
    }

    function listMeta() {
        $this->Where = "ORDER BY sequence asc";
        return $this->ListOfAllRecords('object');
    }

    function listMetaByGroupId($group_id) {
        $this->Where = "WHERE group_id='$group_id' ORDER BY sequence asc";
        return $this->ListOfAllRecords('object');
    }

    public static function getMetaIdByTitle($title) {
        $obj = new meta;
        $obj->Field = "id";
        $obj->Where = "WHERE title='$title'";
        $data = $obj->DisplayOne();
        return $data->id;
    }

}

class metaGroup extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    /* */

    function __construct($order = 'asc', $orderby = 'id') {
        parent::__construct('user_meta_groups');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'name', 'sequence', 'is_active', 'is_deleted');
    }

    function saveMetaGroup($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);

        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            if ($this->Update()) {
                return $this->Data['id'];
            }
        } else {
            $this->Data['add_date'] = time();
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    function getMetaGroup($id) {
        return $this->_getObject('user_meta_groups', $id);
    }

    function ListMetaGroups() {
        $this->Where = "WHERE is_active='1'";
        return $this->ListOfAllRecords('object');
    }

}

class metaData extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    /* */

    function __construct($order = 'asc', $orderby = 'id') {
        parent::__construct('user_metadata');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'user_id', 'key_id', 'value', 'last_update');
    }

    function saveMetaData($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        $this->Data['last_update'] = time();
        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            if ($this->Update()) {
                return $this->Data['id'];
            }
        } else {
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    function getMetaData($id) {
        return $this->_getObject('user_metadata', $id);
    }

    function getMetaDataByMetaId($meta_id, $user_id = 0) {
        if ($user_id == 0) {
            $user_id = $_SESSION['admin_session_secure']['user_id'];
        }
        $this->Where = "WHERE user_id='$user_id' AND key_id='$meta_id'";
        return $this->DisplayOne();
    }

    function checkMetaExists() {
        $user_id = $_SESSION['admin_session_secure']['user_id'];
        $this->Where = "WHERE user_id='$user_id'";
        return $this->ListOfAllRecords('object');
    }

    public static function getMetaDataIdByMetaId($meta_id) {
        $user_id = $_SESSION['admin_session_secure']['user_id'];
        $obj = new metaData;
        $obj->Where = "WHERE key_id='$meta_id' and user_id='$user_id'";
        $data = $obj->DisplayOne();
        return $data->id;
    }

    public static function getMetaValue($meta_id, $user_id = 0) {
        if ($user_id == 0) {
            $user_id = $_SESSION['admin_session_secure']['user_id'];
        }
        $obj = new metaData;
        $obj->Field = 'value';
        $obj->Where = "WHERE key_id='$meta_id' and user_id='$user_id'";
        $data = $obj->DisplayOne();
        return $data->value;
    }
    public static function getMetaValueP($meta_id, $user_id = 0) {
        if ($user_id == 0) {
            $user_id = $_SESSION['admin_session_secure']['user_id'];
        }
        $obj = new metaData;
        $obj->Field = 'value';
        $obj->Field = 'key_id';
        $obj->Where = "WHERE key_id='$meta_id' and user_id='$user_id'";
        $data = $obj->DisplayOne();
        return $data->value;
    }

}

class metaOptions extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    /* */

    function __construct($order = 'asc', $orderby = 'id') {
        parent::__construct('user_meta_options_value');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'meta_id', 'value', 'date_add');
    }

    function saveMetaOption($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            if ($this->Update()) {
                return $this->Data['id'];
            }
        } else {
            $this->Data['date_add'] = time();
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    function getMetaOption($id) {
        return $this->_getObject('user_meta_options_value', $id);
    }

    function getMetaOptionsByMetaId($meta_id) {
        $this->Where = "WHERE meta_id='$meta_id'";
        return $this->ListOfAllRecords('object');
    }

}

class matchMetaData extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    /* */

    function __construct($order = 'asc', $orderby = 'id') {
        parent::__construct('user_match_metadata');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'user_id', 'key_id', 'value', 'last_update');
    }

    function saveMatchData($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        $this->Data['last_update'] = time();
        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            if ($this->Update()) {
                return $this->Data['id'];
            }
        } else {
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    function getMatchData($id) {
        return $this->_getObject('user_match_metadata', $id);
    }

}
