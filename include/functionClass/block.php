<?php

class block extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    /* */

    function __construct($order = 'asc', $orderby = 'id') {
        parent::__construct('block');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'user_id', 'block');
    }

    function saveBlock($user_id, $block, $type) {
        if ($type == 'block') {
            $this->Data['user_id'] = $user_id;
            $this->Data['block'] = $block;
            $this->Insert();
            return $this->GetMaxId();
        } else {
            $this->Where = "WHERE `user_id` = '$user_id' AND `block` = '$block'";
            $this->Delete_where();
        }
    }

    function is_block($user_id, $block) {
        $this->Where = "WHERE `user_id` = '$user_id' AND `block` = '$block'";
        return $this->DisplayOne();
    }

}
