<?php

class state extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    /* */

    function __construct($order = 'asc', $orderby = 'id') {
        parent::__construct('states');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'name', 'abbr', 'zip', 'is_active');
    }

    function saveState($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            if ($this->Update()) {
                return $this->Data['id'];
            }
        } else {
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    function getMetaGroup($id) {
        return $this->_getObject('states', $id);
    }

    function listStates() {
        $this->Where = "WHERE is_active='1'";
        return $this->ListOfAllRecords('object');
    }

}
