<?php

class userPhoto extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    /* */

    function __construct($order = 'asc', $orderby = 'id') {
        parent::__construct('user_photos');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'user_id', 'caption', 'file_name', 'add_date', 'upd_date', 'sequence', 'is_primary');
    }

    function saveUserPhoto($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            $this->Data['upd_date'] = time();
            if ($this->Update()) {
                return $this->Data['id'];
            }
        } else {
            $this->Data['add_date'] = time();
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    function getPhoto($id) {
        return $this->_getObject('user_photos', $id);
    }

    function getUserPhoto($user_id = 0) {
        if ($user_id == 0) {
            $user_id = $_SESSION['admin_session_secure']['user_id'];
        }
        $this->Where = "WHERE user_id='$user_id' order by sequence desc";
        return $this->ListOfAllRecords('object');
    }

    function setPrimaryPhoto($pid, $set) {
        $this->Data['id'] = $pid;
        $this->Data['is_primary'] = $set;
        $this->Update();
    }

    function removeAllProfilePhoto($user_id = 0) {
        if ($user_id == 0) {
            $user_id = $_SESSION['admin_session_secure']['user_id'];
        }
        $this->Data['is_primary'] = '0';
        $this->Where = " WHERE user_id='$user_id' ";

        $records = $this->UpdateCustom();
    }

    function getProfilePhoto($user_id = 0) {
        if ($user_id == 0) {
            $user_id = $_SESSION['admin_session_secure']['user_id'];
        }
        $this->Where = "WHERE user_id='$user_id' AND `is_primary`='1' order by id desc limit 1";
        return $this->DisplayOne();
    }

    function getLastUploadedPhoto($user_id = 0) {
        if ($user_id == 0) {
            $user_id = $_SESSION['admin_session_secure']['user_id'];
        }
        $this->Where = "WHERE user_id='$user_id' order by id desc limit 1";
        return $this->DisplayOne();
    }

    function getPhotoCount($user_id) {
        $this->Field = "COUNT(*) as count";
        $this->Where = "WHERE user_id='$user_id'";
        $data = $this->DisplayOne();
        return $data->count;
    }

    function delete_photo($pid) {
        $this->id = $pid;
        $this->Delete();
    }

    public static function inbox_user_photo($from_id) {
        $query = new userPhoto;
        $query->Where = "WHERE `user_id` = '$from_id' AND `is_primary` = 1 ORDER BY `id` DESC";
        return $query->DisplayOne();
    }

    public static function sent_user_photo($to_id) {
        $query = new userPhoto;
        $query->Where = "WHERE `user_id` = '$to_id' AND `is_primary` = 1 ORDER BY `id` DESC";
        return $query->DisplayOne();
    }

    public static function ajax_primary_photo($user_id) {
        $query = new userPhoto;
        $query->Where = "WHERE `user_id` = '$user_id' AND `is_primary` = 1";
        return $query->DisplayOne();
    }

}

class userVideo extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    /* */

    function __construct($order = 'asc', $orderby = 'id') {
        parent::__construct('user_videos');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'user_id', 'caption', 'video_code', 'add_date', 'upd_date', 'sequence', 'is_primary');
    }

    function saveUserVideo($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            $this->Data['upd_date'] = time();
            if ($this->Update()) {
                return $this->Data['id'];
            }
        } else {
            $this->Data['add_date'] = time();
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    function getVideo($id) {
        return $this->_getObject('user_videos', $id);
    }

    function getUserVideo($user_id = 0) {
        if ($user_id == 0) {
            $user_id = $_SESSION['admin_session_secure']['user_id'];
        }
        $this->Where = "WHERE user_id='$user_id' order by sequence desc";
        return $this->ListOfAllRecords('object');
    }

    function getLastUploadedVideo($user_id = 0) {
        if ($user_id == 0) {
            $user_id = $_SESSION['admin_session_secure']['user_id'];
        }
        $this->Where = "WHERE user_id='$user_id' order by id desc limit 1";
        return $this->DisplayOne();
    }

    function primary_video($user_id = 0) {
        if ($user_id == 0) {
            $user_id = $_SESSION['admin_session_secure']['user_id'];
        }
        $this->Where = "WHERE user_id='$user_id' AND `is_primary` = 1";
        return $this->DisplayOne();
    }

    function getVideoCount($user_id) {
        $this->Field = "COUNT(*) as count";
        $this->Where = "WHERE user_id='$user_id'";
        $data = $this->DisplayOne();
        return $data->count;
    }

    function delete_video($vid) {
        $this->id = $vid;
        $this->Delete();
    }

    function remove_primary_photo($user_id = 0) {
        if ($user_id == 0) {
            $user_id = $_SESSION['admin_session_secure']['user_id'];
        }
        $this->Data['is_primary'] = '0';
        $this->Where = " WHERE user_id='$user_id' ";

        $records = $this->UpdateCustom();
    }

    function setPrimaryPhoto($vid, $set) {
        $this->Data['id'] = $vid;
        $this->Data['is_primary'] = $set;
        $this->Update();
    }

}
