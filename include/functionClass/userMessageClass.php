<?php

class userMessage extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    /* */

    function __construct($order = 'asc', $orderby = 'id') {
        parent::__construct('user_messages');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'from_id', 'to_id', 'subject', 'message', 'is_read', 'from_status', 'to_status', 'add_date', 'seen');
    }

    function saveMessage($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);

        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            if ($this->Update()) {
                return $this->Data['id'];
            }
        } else {
            $this->Data['add_date'] = time();
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    function getMessage($id) {
        return $this->_getObject('user_messages', $id);
    }

    function getInboxMessages() {
        $user_id = $_SESSION['admin_session_secure']['user_id'];
        $this->Where = "WHERE to_id='$user_id' AND `receiver_delete` = 0 ORDER BY id desc";
        return $this->ListOfAllRecords('object');
    }

    function getSentMessages($id = 0) {
        if ($id == 0) {
            $user_id = $_SESSION['admin_session_secure']['user_id'];
            $this->Where = "WHERE from_id='$user_id' AND `sender_delete` = 0 ORDER BY id desc";
            return $this->ListOfAllRecords('object');
        } else {
            $this->Where = "WHERE `id` = $id";
            return $this->DisplayOne();
        }
    }

    function my_get_user_messages($reply_username, $user_id) {
        $this->Where = "WHERE (`from_id` = '$reply_username' OR `to_id` = $reply_username) AND (`from_id` = '$user_id' OR `to_id` = $user_id) ORDER BY `id` DESC";
        return $this->ListOfAllRecords();
    }

    function get_object($tablename, $username) {
        $this->TableName = $tablename;
        $this->Where = "WHERE `username` = '$username'";
        return $this->DisplayOne();
    }

    function message_make_seen($id) {
        $this->Data['id'] = $id;
        $this->Data['seen'] = 1;
        return $this->Update();
    }

    function unseen_message($user_id) {
        $this->Where = "WHERE `to_id` = '$user_id' AND `seen` = 0 AND `receiver_delete` = 0";
        return $this->ListOfAllRecords();
    }

    function delete_inbox_message($id) {
        $this->Data['id'] = $id;
        $this->Data['receiver_delete'] = 1;
        $this->Update();
    }

    function delete_sent_message($id) {
        $this->Data['id'] = $id;
        $this->Data['sender_delete'] = 1;
        $this->Update();
    }

}
